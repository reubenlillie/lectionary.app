/**
 * @file Defines the chained template for the copyright notice
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 */

/**
 * Acts as front matter data in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */
export var data = {
  date: '2024-08-27',
  emojiId: 'copyright',
  templateEngineOverride: '11ty.js,md',
  title: 'Copyright Notice',
  navTitle: 'Copyright',
  weight: 12,
  description: data => {
    var {bcp, copyright} = data
    return `The Daily Office Lectionary is in the public domain from the [_Book of Common Prayer_](${bcp.pdf}) (${bcp.edition}). Unless otherwise noted, content on the Lectionary.app is licensed under the ${copyright.license.name}).`
  }
}

/**
 * The content of the copyright notice
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @since 0.1.0
 * @since 1.0.0 Add link to contact form
 * @since 2.2.0 Add views disclaimer for BibleGateway
 */
export function render(data) {
  /** @type {Object} Destructure a portion of `data` */
  var {bcp, copyright, pkg: {repository}} = data

  return `<!--./pages/copyright.11ty.js-->

Last updated on ${this.pageDate(data)}

## ${this.emojiById('copyright')} Copyrighted Content

The Daily Office Lectionary is in the public domain from the [_Book of Common Prayer_](${bcp.pdf}) (${bcp.edition}).

Links to BibleGateway are for your convenience. We’re not affiliated with BibleGateway, Zondervan, or HarperCollins Christian Publishing. We’re grateful to BibleGateway for making their resources freely available. Views expressed in BibleGateway’s content belong to the content creators and not the Church of the Nazarene in the Loop, our affiliates, or employees.

Unless otherwise noted, content on the Lectionary.app is licensed under the [${copyright.license.name}](${copyright.url}).

You are free to copy and redistribute the material in any medium or format. You are also free to remix, transform, and build upon the material for any purpose, even commercially. We wot revoke these terms as long as you follow the terms of this license.

You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

## ${this.emojiById('free')} Source Code License

The source code for the Lectionary.app is licensed under the [MIT License](${repository.url}/blob/master/LICENSE). A copy of the license is available in the source code repository on [GitLab](${repository.url}).

## ${this.emojiById('contact')} Contact the Lectionary.app team

If you would like to use our copyrighted content beyond the scope of this notice, or if you have any other questions related to copyright, please [contact us][contact] or write:

${this.contactInfo(data)}

For technical support, you may also [visit our GitLab repository](${repository.url}).

[contact]: /contact/`
}
