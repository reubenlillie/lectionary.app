/**
 * @file The chained layout for the app privacy policy
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 */

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  date: '2024-08-14',
  emojiId: 'privacy',
  templateEngineOverride: '11ty.js,md',
  title: 'Privacy Policy',
  navTitle: 'Privacy',
  weight: 13,
  description: 'Our privacy policy is designed to be simple and easy to read. We value your privacy. Use the Lectionary.app in peace.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @since 0.1.0
 * @since 1.0.0 Add link to contact form
 */
export function render(data) {
  /** @type {Object} Destructure a portion of `data` */
  var {pkg: {repository}} = data

  return `<!--./pages/policies/privacy.11ty.js-->

Last updated on ${this.pageDate(data)}

## What do we mean by “personal information?”

For us, “personal information” means information which either directly identifies you (like your name or email address) or can be reasonably linked or combined to identify you (like an account identification number or IP address). We’ll always tell you what personal information we’re collecting from you.

We consider any other information “non-personal information.”

If we store your personal information with information that is non-personal, then we will consider the combination as personal information. If we remove all personal information from a set of data, then the remaining set is non-personal information.

## What information do we collect?

We don’t collect any personal information other than what you explicitly share with us through the Lectionary.app. Unless you contact us to tell us you’ve used the Lectionary.app, we have no way of knowing.

## How do we use information we collect?

When you give us information, we will use it only in the ways for which you've given us permission. Generally, we use your information to communicate directly with you.

## What information do we share?

Whenever you share personal information with us through the Lectionary.app, we will not share that information with anyone apart from your knowledge and without your explicit permission.

## What if we want to share information we collect?

Before sharing any personal information you’ve shared with us through the Lectionary.app, we will seek your permission directly. First, we will contact you by e-mail, phone, or private message through a common social media platform, or some other mutually beneficial communication method. We will ask you if and how we may share specific personal information you have shared with us through the Lectionary.app. Then, only after you have given us your explicit permission, will we share any of that information.

## What if we change this policy?

We may update this privacy policy from time to time. We will not reduce your rights under this privacy policy without your explicit consent. We always indicate the date the last changes were published, and we offer access to archived versions for your review. If we make any significant changes, we will provide a more prominent notice.

## ${this.emojiById('contact')} Contact the Lectionary.app team

If you want to make a correction to your information, or if you have any questions about our privacy policy, please [contact us][contact] or write:

${this.contactInfo(data)}

For technical support, you may also [visit our GitLab repository](${repository.url}).

[contact]: /contact/`
}
