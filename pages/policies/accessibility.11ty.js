/**
 * @file Defines the chained template for the accessibility statement
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter data in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */
export var data = {
  date: '2024-08-14',
  emojiId: 'accessibility',
  templateEngineOverride: '11ty.js,md',
  title: 'Accessiblity Statement',
  navTitle: 'Accessiblity',
  weight: 11,
  description: 'We strive to make the Lectionary.app accessible to anyone with an internet connection.Use the Lectionary.app in peace.'
}

/**
 * The content of the Accessiblity Statement
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @since 0.1.0
 * @since 1.0.0 Add link to contact form
 */
export function render(data) {
  /** @type {Object} Destructure a portion of `data` */
  var {pkg: {repository}} = data

  return `<!--./pages/policies/terms.11ty.js-->

Last updated on ${this.pageDate(data)}

We are committed to making the Lectionary.app accessible for everyone.

## 📏 Minimum standards

The Lectionary.app meets or exceeds [Web Content Accessibility Guidelines 2.1][wcag2aa] Level AA.

## ✔️ Additional considerations

We also take the following measures to ensure the Lectionary.app remains accessible:

* Use clear written lanuage
* Make the user interface easy to understand
* Run automated and manual tests for accessibility
* Honor users’ system preferences for lighter or darker color schemes
* Limit file sizes and device memory loads
* Publish our source code in a [public repository][git], allowing anyone to inspect it and suggest changes
* Document our code, to welcome contributers with little or no programming experience

## ${this.emojiById('contact')} Contact the Lectionary.app team

If you encounter any accessbility issues or have any feedback about this statement, please [contact us][contact] or write:

${this.contactInfo(data)}

You may also submit issues and feature requests through our [GitLab repository][git].

[contact]: /contact/
[git]: ${repository.url}
[wcag2aa]: https://www.w3.org/WAI/standards-guidelines/wcag/`
}
