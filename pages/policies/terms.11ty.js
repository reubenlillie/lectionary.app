/**
 * @file Defines the chained template for the terms of use
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter data in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */
export var data = {
  date: '2024-08-14',
  emojiId: 'policy',
  templateEngineOverride: '11ty.js,md',
  title: 'Terms of Use',
  navTitle: 'Terms',
  weight: 14,
  description: 'Our terms of use are designed to be simple and easy to read. Use the Lectionary.app in peace.'
}

/**
 * The content of the Terms of Use
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @since 0.1.0
 * @since 1.0.0 Add link to contact form
 */
export function render(data) {
  /** @type {Object} Destructure a portion of `data` */
  var {pkg: {repository}} = data

  return `<!--./pages/policies/terms.11ty.js-->

Last updated on ${this.pageDate(data)}

## Privacy and Copyright Protection

Our [privacy policy](/privacy/) explains how we treat personal information and protect your privacy when you use the Lectionary.app.

Our [copyright notice](/copyright/) explains the permissions we give you when using app content or the source code.

## Warranties

The Lectionary.app is provided “as is,” without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and noninfringement.

## Liability

We will not be liable for any loss or damage that is not reasonably foreseeable.

## About these terms and conditions

These terms define the relationship between you as a user and us as the owner and developer of the Lectionary.app. These terms do not create third party beneficiary rights.

We may update these terms or any additional terms that apply to the Lectionary.app. If you do not agree to the modified terms, then you should discontinue your use of the Lectionary.app.

If there is a conflict between these terms and any additional terms, the additional terms will take precedence.

If you do not comply by these terms, and we do not take action right away, then that fact alone does not mean we are giving up any rights we may have (including the right to act in the future).

The laws of the State of Illinois, <abbr title="USA: United States of America">USA</abbr>, will apply to any disputes relating to these terms. All claims relating to these terms will be litigated exclusively in the federal or state courts of the County of Cook, Illinois, USA, and yours and our consent to personal jurisdiction in those courts.

## ${this.emojiById('contact')} Contact the Lectionary.app team

If you have any questions about these terms, please [contact us][contact] or write:

${this.contactInfo(data)}

For technical support, you may also [visit our GitLab repository](${repository.url}).

[contact]: /contact/`
}
