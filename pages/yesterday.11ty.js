/**
 * @file A page for yesterday’s readings
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.5.0
 */

/**
 * Acts as data in JavaScript template files
 * @since 0.5.0
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  layout: 'layouts/entry',
  eleventyComputed: {
    title: 'Yesterday’s Readings',
    today: data => data.days.yesterday.date,
    entry: data => data.days.yesterday.entry
  },
  description: 'Look back to see yesterday’s readings from the Daily Office Lectionary. Readings updated daily at midnight UTC.'
}
