/**
 * @file The chained layout for the app “About” page
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @since 0.10.0 Add `aboutLink()` (depracates `bcpLink()`)
 * @since 0.11.3 Reorder lists
 */

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'information',
  icon: 'ℹ️',
  navTitle: 'More',
  tags: ['footerNav', 'nav'],
  title: 'About the Lectionary.app',
  weight: 3,
  donateButton: true,
  description: 'The Lectionary.app is a ministry of the Church of the Nazarene in the Loop. Thanks to our partners, volunteers, and donors who make it free.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 * @since 0.1.0
 * @since 1.0.0 Deprecate policyLink()
 */
export function render({
  collections: {about, cta, policies},
  contact: {social},
  pkg
}) {
  /**
   * Define markup for social media links on the About page
   * @since 0.6.0
   * @since 1.0.0 Add conditional logic for `attr` and `element` properties
   *              like for interactive buttons instead of page URLs.
   * @param {Object} account Social media account data
   * @return {string} HTML
   */
// export default ({data: {attr, element, emojiId, icon, navTitle, url}, ...rest})
  function socialLink({attr, cta, emojiId, element, name, title, url, ...rest}) {
    return `<!--${name ? name : title} from global socialLink()-->
<li ${emojiId ? `data-emoji="${emojiId}"` : ''}
  class="${name ? name.toLowerCase() : title.toLowerCase()}">
  <${element ? element : 'a'} ${attr ? attr : `href="${url ? url : rest.page.url}"`}>
    ${cta}
  </${element ? element : 'a'}>
</li>`
  }

  /**
   * Define markup for front matter links on the About page
   * @since 0.10.0
   * @param {Object} item Front matter link data
   * @return {string} HTML
   */
  function aboutLink({data: {emojiId, title}, url}) {
    return `<!--${title} from aboutLink()-->
<li ${emojiId ? `data-emoji="${emojiId}"` : ''}>
  <a href="${url}">${title}</a>
</li>`
  }

  /*
   * @since 1.0.0 Add `data` objects for calls to action to the `social` array
   */
  social = cta.map(({data}) => data).concat(social)

  return `<!--pages/about.11ty.js-->
<p style="margin-inline:auto;">The Lectionary.app is a ministry of the <a href="https://loopnaz.org/">Church of the Nazarene in the Loop</a>. We want to thank our partners, volunteers, and donors who make the Lectionary.app available for free.</p>
<nav aria-label="about">
  <ul class="emoji-list no-padding border-block-end padding-block-end">
    ${about.sort((a, b) => a.data.weight - b.data.weight)
    .map(item => aboutLink(item)).join('\n')}
  </ul>
  <ul class="emoji-list no-padding border-block-end padding-block-end">
    ${social.map(account => socialLink(account)).join('\n')}
  </ul>
  <ul class="emoji-list no-padding">
    ${policies.sort((a, b) => a.data.weight - b.data.weight)
    .map(policy => aboutLink(policy)).join('\n')}
  </ul>
</nav>
<footer class="text-center">
  <p>Version <a href="${pkg.repository.releases}">${pkg.version}</a></p>
</footer>`
}
