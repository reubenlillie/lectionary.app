# `./pages/`

This directory contains [JavaScript template files](https://www.11ty.dev/docs/languages/javascript/) (files ending in `*.11ty.js`) for pages other than the app index and 404 page (which remain in the root directory). 

`pages.11tydata.js` is a [directory data file](https://www.11ty.dev/docs/data-template-dir/) Eleventy uses to store data common to all pages. For example, Eleventy treats the template files here as [chained layouts](https://www.11ty.dev/docs/layout-chaining/) wrapped by [`./_includes/layouts/page.11ty.s`](https://gitlab.com/reubenlillie/lectionary.app/-/tree/master/_includes/layouts/page.11ty.js). You can override this behavior by setting the `layout` property in the front matter `data` object for a given template.
