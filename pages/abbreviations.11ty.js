/**
 * @file The chained layout for the app “Abbreviations” page
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.10.0
 */

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'key',
  hasTable: true,
  tags: 'about',
  title: 'Abbreviations',
  templateEngineOverride: '11ty.js,md',
  weight: 2,
  description: 'This page explains the meanings of abbreviations in the Lectionary.app, including the recommended abbreviations for books of the Bible from the Society of Biblical Literature.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render({bibleBooks, emoji}) {
  /**
   * Define a helper functions for sorting items by an `order` property
   * @since 0.10.0
   * @param {Object} items Items to sort
   * @return {string} HTML
   */
  function sortByOrder(items) {
    /** @type {Object[]|null} Placeholder for ordered items */
    var result

    /** @type {Object[]} Temporary array for each item’s index and number */
    var indexed = items.map((item, index) => {
      return {
        index: index,
        order: item.order
      }
    })

    // Sort the temporary array
    indexed.sort((a, b) =>
      a.order > b.order
        ? 1
        : a.order < b.order
          ? -1
          : 0
    )

    // Reorder the original array based on each item’s index
    result = [...indexed.map(item => items[item.index])]
    return result
  }

  /** @type {Object[]} Books of the Bible in canonical order */
  var sources = sortByOrder(Object.values(bibleBooks))

  return `<!--./pages/abbreviations.11ty.js-->
<details>
  <summary>
    <h2 class="inline">Finding your way around the Bible</h2>
    <p class="indent prompt show-more" aria-hidden="true">&hellip; Show more</p>
    <p class="indent prompt show-less" aria-hidden="true">&hellip; Show less</p>
  </summary>

### 📐 Structure

The Bible contains two main parts:

* Hebrew Bible (or Old Testament, see <a href="#table1">table&nbsp;1</a>)
* New Testament (see <a href="#table3">table&nbsp;3</a>).

Occasionally, the [Daily Office Lectionary](/what-is-the-lectionary/) also includes readings from extra sources called “deuterocanonical works” (or the Apocrypha, see <a href="#table2">table&nbsp;2</a>).

Each part contains several books. The books further divide into chapters, and chapters into verses.

### 🤔 How biblical references work

The biblical authors didn’t write with chapters and verses in mind.

In fact, the Bible has existed without these divisions longer than with them. The chapters we use today are attributed to Stephen Langton, Archbishop of Canterbury in 1205. And the French publisher Robert Etienne came up with the verse numbers from 1551 to 1553.

The numbers are tools that help refer to specific passages of the Bible.

#### 📚 Books

When referring to an entire book of the Bible, the book name is usually spelled out.

When referring to part of a book, the book name is usually abbreviated. Not only does abbreviating book names save space, but it also makes the names similar in length. These abbreviations are often a better fit for lists and tables than full book names.

The Lectionary.app uses the abbreviations developed by the Society of Biblical Literature (<abbr>SBL</abbr>) rather than the system that’s more unique to the _Book of Common Prayer_.

#### 📑 Chapters

When referring to a large section of a book, the abbreviated book name appears first, followed by a chapter number.

#### 🔖 Verses

When referring to verses within a chapter, the chapter number is followed by a **colon and a space (:&nbsp;)**, then one or more verse numbers.

Consecutive verses are connected with an **en dash (–)**.

Noncosecutive verses are separated by a **comma followed by a space (,&nbsp;)**.

Sometimes a reference includes verses from more than one chapter. When the verses from multiple chapters aren’t consecutive, the chapter break is shown by a **semicolon followed by a space (;&nbsp;)**.

#### 🛠️ Putting it all together

Most references are simple.

_For your convenience, we’ve added links to BibleGateway so you can read all of them on a single page._

##### 😊 Example 1

For example, _<abbr>Matt</abbr> 6:7–15_ refers to the gospel of <em>Matthew</em> (from the <a href="#table3">New Testament</a>)<em>, chapter 6, verses 7 through 15</em>, when Jesus is teaching on prayer.

<blockquote class="bible-passage">
  <p>
    <span><sup class="verse-number">7</sup>“When you pray, don’t pour out a flood of empty words, as the Gentiles do. They think that by saying many words they’ll be heard.</span>
    <span><sup class="verse-number">8</sup>Don’t be like them, because your Father knows what you need before you ask.</span>
    <span><sup class="verse-number">9</sup>Pray like this:</span>
  </p>
  <p class="poetry hanging-indent">
    <span>Our Father in heaven,<br>
    may your name be revered as holy.</span><br>
    <span><sup class="verse-number">10</sup>Bring in your kingdom<br>
    so that your will is done on earth as it’s done in heaven.</span><br>
    <span><sup class="verse-number">11</sup>Give us the bread we need for today.</span><br>
    <span><sup class="verse-number">12</sup>Forgive us for the ways we have wronged you,<br>
    just as we also forgive those who have wronged us.</span><br>
    <span><sup class="verse-number">13</sup>And don’t lead us into temptation,<br>
    but rescue us from the evil one.</span>
  </p>
  <p>
    <span><sup class="verse-number">14</sup>“If you forgive others their sins, your heavenly Father will also forgive you.</span>
    <span><sup class="verse-number">15</sup>But if you don’t forgive others, neither will your Father forgive your sins.</span>
  </p>
  <p class="text-inline-end">
    <cite><abbr>Matt</abbr> 6:7–15 <abbr title="Common English Bible">CEB</abbr></cite>
  </p>
</blockquote>

##### 😃 Example 2

For a more complex example, _<abbr>Rev</abbr> 21:1–4, 22–22:5_ refers to the book of <em>Revelation</em> (also from the New Testament)<em>, chapter 21, verses 1 through 4, then verses 22 of the same chapter through chapter 22, verse 5</em>, when John of Patmos describes the final images of his vision.

<blockquote class="bible-passage">
  <p>
    <span><span class="chapter-number">21</span>Then I saw a new heaven and a new earth, for the first heaven and the first earth had passed away, and the sea was no more.</span>
    <span><sup class="verse-number">2</sup>And I saw the holy city, the new Jerusalem, coming down out of heaven from God, prepared as a bride adorned for her husband.</span>
    <span><sup class="verse-number">3</sup>And I heard a loud voice from the throne saying,</span>
  </p>
  <p class="poetry">
    <span class="hanging-indent">“See, the home of God is among mortals.</span><br>
    <span class="hanging-indent">He will dwell with them;</span><br>
    <span class="hanging-indent">they will be his peoples,</span><br>
    <span class="hanging-indent">and God himself will be with them and be their God;</span><br>
    <span><span class="hanging-indent"><sup class="verse-number">4</sup>he will wipe every tear from their eyes.</span><br>
    <span class="hanging-indent">Death will be no more;</span><br>
    <span class="hanging-indent">mourning and crying and pain will be no more,</span><br>
    <span class="hanging-indent">for the first things have passed away.”</span></span>
  </p>
  <hr>
  <p>
    <span><sup class="verse-number">22</sup>I saw no temple in the city, for its temple is the Lord God the Almighty and the Lamb.</span>
  </p>
  <p>
    <span><span class="chapter-number">22</span>Then the angel showed me the river of the water of life, bright as crystal, flowing from the throne of God and of the Lamb</span>
    <span><sup class="verse-number">2</sup>through the middle of the street of the city. On either side of the river is the tree of life with its twelve kinds of fruit, producing its fruit each month, and the leaves of the tree are for the healing of the nations.</span>
    <span><sup class="verse-number">3</sup>Nothing accursed will be found there any more. But the throne of God and of the Lamb will be in it, and his servants will worship him;</span>
    <span><sup class="verse-number">4</sup>they will see his face, and his name will be on their foreheads.</span>
    <span><sup class="verse-number">5</sup>And there will be no more night; they need no light of lamp or sun, for the Lord God will be their light, and they will reign forever and ever.</span>
  </p>
  <p class="text-inline-end">
    <cite><abbr>Rev</abbr> 21:1–4, 22–22:5 <abbr title="New Revised Standard Version, Updated Edition">NRSVue</abbr></cite>
  </p>
</blockquote>

Again, most references are straightforward like Example 1.

### ⁉️ Other punctuation

Any reading may be lenthened. Suggested lengthenings are shown in **parentheses ()**.

**Brackets ([])** show psalms which may be omitted. In general, the psalms are arranged so you can read all 150 of them every seven weeks (averaging three psalms per day). If you want to follow that pattern, then don’t skip the bracketed psalms.

### 🥰 Emoji

The Lectionary.app also uses several emoji. They’re fun. Like letter abbreviations, they save space. We made a key (see <a href="#table4">table&nbsp;4</a>) to help show the symbolism we have in mind. We want their function to add to the fun instead of taking away from it.

</details>

${sources.map(source => {
    var books = sortByOrder(source.books)
    return `<!--${source.name}-->
<div role="region" aria-labelledby="seasonTable" tabindex="0">
  <table id="table${source.order}" data-source-order="${source.order}">
    <caption class="bold">${source.name}</caption>
    <thead>
      <tr>
        <th scope="col" class="numero padding-inline-end">№</th>
        <th scope="col" class="padding-inline-start slim-column">Abbreviation</th>
        <th scope="col">Book</th>
      </tr>
    </thead>
    <tbody>
      ${books.map(book => `<!--${book.name}-->
  <tr data-book-order="${book.order}">
    <td class="numero padding-inline-end text-inline-end">${book.order}</td>
    <td class="padding-inline-start">${book.abbr}${book.altAbbr ? ` <em>or</em>&nbsp;${book.altAbbr}` : ''}</td>
    <td>${book.name}${book.altName ? ` <em>or</em>&nbsp;${book.altName}` : ''}</td>
  </tr>`).join('\n')}
    </tbody>
    <tfoot class="smaller">
      <tr>
        <td colspan=3><i>Source:</i> <i>SBL Handbook of Style</i> ${source.sbl}</td>
      </tr>
    </tfoot>
  </table>
</div>`
  }).join('\n')}
  <!--${emoji.name}-->
<div role="region" aria-labelledby="seasonTable" tabindex="0">
  <table id="table4" data-source-order="4">
    <caption class="bold">${emoji.name}</caption>
    <thead>
      <tr>
        <th scope="col" class="numero padding-inline-end">${this.emojiById('key')}</th>
        <th scope="col">Symbolizes</th>
      </tr>
    </thead>
    <tbody>
      ${emoji.symbols.filter(({exclude}) => !exclude)
    .map(({id, emoji, symbolizes}) => `<!--${id}-->
  <tr data-emoji="${emoji}">
    <td class="numero padding-inline-end">${emoji}</td>
    <td>${symbolizes}</td>
  </tr>`).join('\n')}
    </tbody>
  </table>
</div>
<footer class="hanging-indent smaller">
  <h2>${this.emojiById('references')} References</h2>
  <p><a href="https://www.sbl-site.org/">Society of Biblical Literature</a>. 2014. “Abbreviations: Primary Sources: Ancient Texts.” In <i>SBL Handbook of Style: For Biblical Studies and Related Disciplines</i>, 2nd ed., 8.3.1–3. Atlanta: SBL Press.</p>
</footer>`
}
