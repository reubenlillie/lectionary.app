/**
 * @file Defines the chained template for the main contact form
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 1.0.0
 */

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  title: 'Contact us',
  cta: 'Contact us',
  emojiId: 'contact',
  tags: 'cta',
  weight: 2,
  templateEngineOverride: '11ty.js,md',
  description: 'Find a typo? Have an idea to make the Lectionary.app even better? We’d love to hear it. Thanks for your help.'
}

/**
 * The content of the template
 * @return {string} The rendered template
 */
export function render() {
  return `<!--./pages/contact.11ty.js-->
Find a typo?

Have an idea to make the Lectionary.app better?

We’d love to hear from you!

<form id="contact"
  method="POST"
  action="/thanks/"
  autocomplete="on"
  netlify-honeypot="bot_field"
  class="flex flex-column"
  netlify>
  <label class="screen-reader">Don’t fill this out if you’re human: <input name="bot_field"></label>
  <label for="name">Name</label>
  <input id="name"
   name="name"
   type="text" name="name"
   required="true">
  <label for="email">Email</label>
  <input id="email"
   name="email"
   type="email"
   required="true">
  <label for="message">Message</label>
  <textarea id="message"
    name="message"
    rows="13"
    cols="47"></textarea>
  <button id="submit"
    name="submit"
    type="submit">Send</button>
</form>`
}
