/**
 * @file Defines the chained template for a landing page to display when the user is offline
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 */

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  title: 'Your device is offline',
  description: 'This page appears when your device is offline. Use these links to revisit pages you’ve browsed until  you get back online.'
}

/**
 * The content of the template
 * @return {string} The rendered template
 */
export async function render() {
  return `<!-- ./pages/offline.11ty.js -->
<div data-offline></div>
<script>
  ${await this.minifyJs(this.fileToString('js/offline.js'))}
</script>`
}
