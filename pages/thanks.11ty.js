/**
 * @file Defines the thank you page layout for contact form submissions
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @since 1.0.0
 */

/**
 * Acts as front matter data in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in JavaScript templates in Eleventy}
 */
export var data = {
  title: 'Thank you for contacting us!',
  eleventyExcludeFromCollections: true,
  emojiId: 'donate',
  layout: 'layouts/page',
  templateEngineOverride: '11ty.js,md'
}

/**
 * Defines markup for a link to a social media account
 * @return {string} HTML
 */
export function render() {
  return `<!--./content/pages/thanks.11ty.js-->

We got your message, and we’ll respond soon.

Feel free to keep looking around:

* Go back to the [homepage](/)
* [Send us another message](/contact/)`
}
