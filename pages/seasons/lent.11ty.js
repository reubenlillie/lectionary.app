/**
 * @file The chained layout for the Lent page.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 */

// Local functions
import addDays from '../../_includes/functions/add-days.js'

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'purple-heart',
  templateEngineOverride: '11ty.js,md',
  title: 'Lent',
  weight: 4,
  eleventyComputed: {
    startDate: data =>
      // Is today between Advent and Holy Name (Jan 1) or after Pentecost?
      data.isNewLiturgicalYear || data.afterPentecost
        // Use the date for Ash Wednesday in the next calendar year
        ? data.days.nextAshWednesday
        // Use the date from Ash Wednesday for the current calendar year
        : data.days.ashWednesday,
    // Is it a new Liturgical Year?
    endDate: data =>
      // Is today between Advent and Holy Name (Jan 1) or after Pentecost?
      data.isNewLiturgicalYear || data.afterPentecost
        // Use the date for Easter in the next calendar year
        ? addDays(-1, data.days.nextEaster)
        // Use the date from Easter for the current calendar year
        : addDays(-1, data.days.easter)
  },
  description: 'Lent is a 40-day fast before Easter. It begins Ash Wednesday and ends at the Easter Vigil (on Holy Saturday). The season commemorates the 40 days Jesus fasted and was tempted in the desert before beginning his public ministry as well as the final days before his death.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render(data) {
  return `Lent is a 40-day fast before [Easter](/easter/). It begins Ash Wednesday (no earlier than Febuary 3, no later than March 10) and ends at the Easter Vigil on Holy Saturday. Counting Sundays, Lent lasts a total of 46 days.

The [season](/seasons/) commemorates the 40 days Jesus fasted and was tempted in the desert before beginning his public ministry.

The last week of Lent, called Holy Week, commemorates the final days before Jesus’s death. It begins with Palm Sunday, recalling Jesus’s arrival at Jerusalem. Holy Week also includes Maudy Thursday, dedicated to the Last Supper Jesus shared with the disciples, followed by Good Friday, recalling Jesus’s crucixion by Rome, and ending at the Easter Vigil.`
}
