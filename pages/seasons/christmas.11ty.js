/**
 * @file The chained layout for the Christmas page.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 */

// Local functions
import addDays from '../../_includes/functions/add-days.js'

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'white-heart',
  templateEngineOverride: '11ty.js,md',
  title: 'Christmas',
  layout: 'layouts/season',
  weight: 2,
  eleventyComputed: {
    startDate: data => data.days.holyName < data.days.today && data.days.today < data.days.epiphany
      ? data.days.lastChristmas
      : data.days.christmas,
    endDate: data => data.days.holyName < data.days.today && data.days.today < data.days.epiphany
      ? addDays(-1, data.days.epiphany)
      : addDays(-1, data.days.nextEpiphany)
  },
  description: 'Christmas celebrates the birth of Jesus of Nazareth. It begins December 25 and lasts twelve days, culminating at the Eve of Epiphany (or Twelfth Night) on January 5. The season is a time to celebrate the gift of God’s presence and to commemorate early events in Jesus’s life, including Holy Innocents’ Day.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render(data) {
  return `Christmas celebrates the birth of Jesus of Nazareth. It begins December 25 and lasts twelve days, culminating at the Eve of [Epiphany](/epiphany/) (or Twelfth Night) on January 5.

Christmastide is a [season](/seasons/) to celebrate the gift of God’s presence and to commemorate early events in Jesus’s life.

Holy Innocents’ Day (December 28), which recognizes the children executed by Herod the Great at Bethlehem according to the Gospel of Matthew.

The feast of the Holy Name of Jesus recalls when Jesus’s parents named him at Jerusalem, according to the Gospel of Luke.`
}
