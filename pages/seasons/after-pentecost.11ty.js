/**
 * @file The chained layout for the Season after Pentecost page.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 */

// Local functions
import addDays from '../../_includes/functions/add-days.js'

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'green-heart',
  templateEngineOverride: '11ty.js,md',
  title: 'The Season after Pentecost',
  layout: 'layouts/season',
  weight: 6,
  eleventyComputed: {
    // Is today between Advenent and Holy Name (Jan 1)?
    startDate: data => data.isNewLiturgicalYear
      // Use the date for Whit Monday in the next calendar year
      ? data.days.nextWhitMonday
      // Use the date from Whit Monday for the current calendar year
      : data.days.whitMonday,
    // Is it a new Liturgical Year?
    endDate: data => data.isNewLiturgicalYear
      // Use the date for Advent in the next calendar year
      ? addDays(-1, data.days.nextAdvent)
      // Use the date from Advent for the current calendar year
      : addDays(-1, data.days.advent)
  },
  description: 'The Season after Pentecost (or Ordinary Time) begins the day after Pentecost and ends the day before the First Sunday of Advent, lasting between 23 and 28 weeks. The first Sunday celebrates the Trinity, and the last Sunday celebrates the Reign of Christ. The season also includes the Transfiguration (August 6), Holy Cross Day (September 14), and All Saints’ Day (November 1), among other major feasts.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render(data) {
  return `Pentecost is the final day of the [Easter](/easter/) season. It commemorates the gift of the Holy Spirit at Jerusalem, according to Acts 2.

The season that follows (also known as Ordinary Time) covers the remainder of the [Church Year](/seasons/), ending the day before the First Sunday of [Advent](/advent/). In all, the Season after Pentecost lasts 23–29 weeks (no earlier than May 9, no later than December 2).

The first Sunday after Pentecost celebrates the Trinity, and the last Sunday after Pentecost celebrates the Reign of Christ.

The season also includes the Feast of the Transfiguration (August 6), Holy Cross (September 14), and All Saints’ Day (November 1), among other major feasts.`
}
