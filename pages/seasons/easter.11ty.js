/**
 * @file The chained layout for the Easter page.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 */

// Local functions
import addDays from '../../_includes/functions/add-days.js'

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'white-heart',
  templateEngineOverride: '11ty.js,md',
  title: 'Easter',
  weight: 5,
  eleventyComputed: {
    // Is today between Advenent and Holy Name (Jan 1)?
    startDate: data => data.isNewLiturgicalYear
      // Use the date for Easter in the next calendar year
      ? data.days.nextEaster
      // Use the date from Easter for the current calendar year
      : data.days.easter,
    // Is it a new Liturgical Year?
    endDate: data => data.isNewLiturgicalYear
      // Use the date for Whit Monday in the next calendar year
      ? addDays(-1, data.days.nextWhitMonday)
      // Use the date from Whit Monday for the current calendar year
      : addDays(-1, data.days.whitMonday)
  },
  description: 'Easter celebrates the resurrection of Jesus Christ. It begins the Sunday after the full moon that falls on or after March 21. The season lasts 50 days, culminating at Pentecost. Eastertide commemorates Jesus’s appearances after the resurrection, the Ascension, and the gift of the Holy Spirit.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render(data) {
  return `Easter celebrates the resurrection of Jesus Christ. It begins the Sunday after the full moon that falls on or after March 21 (no later than April 25). The [season](/seasons/) lasts 50 days, culminating at [Pentecost](/after-pentecost/).

Eastertide commemorates Jesus’s appearances after the resurrection as well as the ascension and the gift of the Holy Spirit.`
}
