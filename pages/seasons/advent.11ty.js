/**
 * @file The chained layout for the Advent page.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 */

// Local functions
import addDays from '../../_includes/functions/add-days.js'

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'purple-heart',
  templateEngineOverride: '11ty.js,md',
  title: 'Advent',
  layout: 'layouts/season',
  weight: 1,
  eleventyComputed: {
    startDate: data => data.days.advent,
    endDate: data => addDays(-1, data.days.christmas)
  },
  description: 'Advent is the first season of each new church year. It begins four Sundays before Christmas and ends at Christmas Eve on December 24. The season is a time to celebrate and anticipate the first and final comings of Christ.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render(data) {
  return `Advent is the first [season](/seasons/) of each new church year. It begins four Sundays before [Christmas](/christmas/) (no earlier than November 27, no later than December 3) and ends at Christmas Eve on December 24.

The season is a time to celebrate and anticipate the first and final comings of Christ.`
}
