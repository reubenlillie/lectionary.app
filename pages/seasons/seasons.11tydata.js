/**
 * @file Contains metadata common to all season pages, to reduce repetition
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Directory data module
 * @module pages/seasons
 * @since 0.11.0
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in Eleventy}
 */
export default {
  hasTable: true,
  layout: 'layouts/season',
  tags: 'seasons',
  eleventyComputed: {
    today: data => data.days.today.date,
    year: data => data.days.year,
    lectionary: data => data.dailyOffice,
    // Is today after Pentecost?
    afterPentecost: data => data.days.whitMonday < data.days.today.date,
    // Is today between Advent and Holy Name (Jan 1)?
    isNewLiturgicalYear: data => data.days.advent < data.today &&
      data.today < data.days.nextHolyName
  }
}
