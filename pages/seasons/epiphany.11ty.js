/**
 * @file The chained layout for the Epiphany page.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 */

// Local functions
import addDays from '../../_includes/functions/add-days.js'

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'green-heart',
  templateEngineOverride: '11ty.js,md',
  title: 'Epiphany',
  weight: 3,
  eleventyComputed: {
    startDate: data =>
      // Is today between Advent and Holy Name (Jan 1) or after Pentecost?
      data.isNewLiturgicalYear || data.afterPentecost
        // Use the date for Epiphany in the next calendar year
        ? data.days.nextEpiphany
        // Use the date from Epiphany for the current calendar year
        : data.days.epiphany,
    endDate: data =>
      // Is today between Advent and Holy Name (Jan 1) or after Pentecost?
      data.isNewLiturgicalYear || data.afterPentecost
        // Use the date for Ash Wednesday in the next calendar year
        ? addDays(-1, data.days.nextAshWednesday)
        // Use the date from Ash Wednesday for the current calendar year
        : addDays(-1, data.days.ashWednesday)
  },
  description: 'Epiphany celebrates the revelation of God becoming human as Jesus of Nazareth. It begins January 6, followed by the 40–63 days leading up to Ash Wednesday. The season is a time to celebrate the arrival of the Magi, Jesus’s baptism, presentation at the Temple, transfiguration, and other events in Jesus’s life before beginning his public ministry.'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render(data) {
  return `Epiphany celebrates the revelation of God becoming human as Jesus of Nazareth.

It begins January 6, followed by the 40–63 days leading up to Ash Wednesday (the first day of [Lent](/lent/)), depending on the date of [Easter](/easter/).

The [season](/seasons/) is a time to celebrate major events in Jesus’s life before beginning his public ministry, including:

* The arrival of the magi (or _the_ Epiphany January 6)
* Jesus’s baptism (the next Sunday) and
* Presentation at the Temple (February 2)

The Last Sunday after the Epiphany (before Ash Wednesday) commemorates Jesus’s Transfiguration. It also marks the transition from meeting God in Jesus to entering the Lenten fast.`
}
