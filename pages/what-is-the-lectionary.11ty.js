/**
 * @file The chained layout for info about the Daily Office Lectionary
 *
 * Replaces the “Concerning the Daily Office Lectionary” page facsimile
 * from the _Book of Common Prayer_.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.10.0
 */

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'thought-balloon',
  tags: 'about',
  templateEngineOverride: '11ty.js,md',
  title: 'What is the Lectionary?',
  weight: 1,
  description: 'The Daily Office Lectionary is a two-year cycle of Bible readings that dates back to 1549, now enjoyed by millions globally.'
}

/**
 * The content of the template
 * @return {string} The rendered template
 */
export function render({app: {locale}, bcp, days: {advent, nextAdvent, today}}) {
  return `<!--./pages/what-is-the-lectionary.11ty.js-->
A **lectionary** is a schedule of Bible readings.

Some lectionaries are designed for public church services. Others are more suited for private devotions. The Daily Office Lectionary works for both.

## 📜 Brief History

Lectionaries have been used for centuries. Judaism traces the origin of the weekly Torah portion (or _parsha_) to the time of Ezra and Nehemiah (around 445 BCE). Nowadays, the variety of Bible reading plans is practically limitless.

The Daily Office Lectionary dates back to 1549 CE, when the Church of England published the first edition of the _Book of Common Prayer_ (BCP). Its current form comes from the 1789 BCP, published by the Episcopal Church.

Millions of people worldwide have followed this simple, yet profound system for generations. Whenever you read from it, you can be sure you’re not alone. Someone around the world is reading those same passages with you at the same time.

## 📐 Form

The Daily Office Lectionary completes a cycle every two years. Rather than following days of the month, it follows the [seasons][seasons] of the church year.

Year One begins the First Sunday of [Advent][advent] (four weeks before [Christmas][christmas]) before odd-numbered years. Year Two begins the First Sunday of Advent before even-numbered years.

So, ${this.getYear(this.addDays(-365, today.date))} starts on ${this.formattedDate(advent, locale)} and ${this.getYear(today.date, 365)} starts on ${this.formattedDate(nextAdvent, locale)}.

## 📚 Readings

Each day features three types of readings.

The first reading usually comes from the Hebrew Bible (sometimes called the Old Testament). Occasionally, it’s from one of the deuterocanonical works (or Apocrypha).

The second reading usually comes from a New Testament letter. Sometimes it’s from Acts or Revelation.

The third reading is called the “gospel” because it almost always comes from Matthew, Mark, Luke, or John’s account of Jesus’s life and ministry.

Each reading is usually short, often less than a chapter. Then, over the two-year cycle, you would read most of the Bible.

Traditionally, two readings are for the morning and one for the evening. Some folks prefer to read them all at once. If you divide them, the BCP suggests the gospel for evening during Year One and for morning during Year Two.

## 🎶 Psalms

Each day also features morning and evenings psalms.

Most of the time, the order of the psalms follows a seven-week pattern. Some psalms are specially selected for the Christmas, Lent, and Easter seasons.

## ⁉️ Punctuation

The Daily Office Lectionary uses brackets to show when psalms may be skipped. Sometimes alternative psalms appear alongside them. If you want to read the book of Psalms in its entirety, then use the bracketed psalms, not the alternatives.

Any reading may be lengthened. Suggested lengthenings appear in parentheses.

> We made an [${this.emojiById('key')} Abbreviations][abbreviations] page to help explain how Bible references are formatted.

## 🎉 Feasts

Throughout the year, some major church holidays (or feasts) will replace the ordinary readings for that day. These Holy Days often commemorate significant events in the life of Jesus as well as some saints.

<h2>${this.svgIcon('logo-color')} The Lectionary.app</h2>

The Lectionary.app is designed to help you follow the Daily Office without having to remember what day it is in the church year.

The app automatically updates everyday at midnight UTC. The homepage gives you quick access to today’s readings as well as yesterday’s and tomorrow’s.

The app also has tables containing all the readings for each [season][seasons]. So whether you want to plan ahead or catch up, you don’t have to wonder what the readings for a particular day are.

You can discover [more features][about] and [request new ones][contact].

Whether you’re new to the Daily Office Lectionary, already old friends, or getting reacquainted, we hope the Lectionary.app enhances your Bible reading and your life.

<footer class="smaller hanging-indent">
  <h2>${this.emojiById('references')} References</h2>
  <p>Episcopal Church. ${bcp.edition}. “Concerning the Daily Office Lectionary.” In <a href="${bcp.pdf}#page=934"><cite>The Book of Common Prayer</cite></a>, 934–935</a>. New York: Church Publishing.</p>
</footer>

[abbreviations]: /abbreviations/
[about]: /about/
[advent]: /advent/
[christmas]: /christmas/
[contact]: /contact/
[seasons]: /seasons/`
}
