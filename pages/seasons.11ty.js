/**
 * @file The chained layout for the app “Seasons of the Church Year” page
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 */

// Local shortcodes
import formattedDate from '../_includes/shortcodes/formatted-date.js'

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  emojiId: 'calendar',
  layout: 'layouts/page',
  tags: 'about',
  templateEngineOverride: '11ty.js,md',
  title: 'Seasons of the Church Year',
  weight: 3,
  description: 'The church year is divided into six seasons: Advent, Christmas, Epiphany, Lent, Easter, and the Season after Pentecost (or Ordinary Time).'
}

/**
 * The content of the template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render({app: {locale}, collections: {seasons}}) {
  /**
   * Define markup for items in the list of seasons
   * @param {Object} season Item in the seasons collection
   * @return {string} HTML
   * @see {@link https://www.11ty.dev/docs/collections/ “Collections (Using Tags)” in Eleventy Docs}
   */
  function seasonListItem({
    url,
    data: {emojiId, title, weight, startDate, endDate}
  }) {
    return `<!--seasonListItem() in pages/seasons.11ty.js--><li ${emojiId ? `data-emoji="${emojiId}"` : ''} data-weight="${weight}"><a href="${url}">${title}</a> <span class="smaller">(${formattedDate(startDate, locale)}–${formattedDate(endDate, locale)})</span></li>`
  }

  return `<!--pages/seasons.11ty.js-->
The church year centers around two major holidays:

* [Christmas](/christmas/)
* [Easter](/easter/)

Both holidays have their own seasons. Both seasons are also preceded by a season of fasting and followed by a season of feasts.

Together, these six seasons run in two cycles:

* [Advent](/advent/) leads to Christmas, followed by [Epiphany](/epiphany/), then
* [Lent](/lent/) leads to Easter, followed by the [Season after Pentecost](/after-pentecost/)

Christmas is always December 25. But the date for Easter changes every year. On the Gregorian calendar, it’s the Sunday after the full moon that falls on or after March 21 (a fixed date close to the northward equinox).

Each new church year begins with Advent (four Sundays before Christmas). The next four seasons cover major events in Jesus’s life.

Christmas celebrates Jesus’s birth.

Epiphany celebrates the world receiving Jesus as Messiah, or the Christ (symbolized by the visit from the magi in <abbr>Matt</abbr> 3).

Lent recalls Jesus’s 40-day fast in the wilderness and the final days before his crucifixion.

Easter celebrates Jesus’s resurrection from the dead and his ascension to heaven. The Easter season culminates at Pentecost, when the disciples received the Holy Spirit. In that sense, Pentecost marks the birth of the church.

These first five seasons cover roughly half of the year.

The Season after Pentecost (or Ordinary Time) covers the remaining half. Instead of tracking a theme or event, the Season after Pentecost is a time for spiritual growth and maturation.

Throughout the year, several feasts mark other events in Jesus’s life as well as the lives of saints throughout church history.

Each day’s readings follow this basic rhythm. They call the church habitually to repent, celebrate, and proclaim the good news of Jesus Christ.

You can learn more about each season from the following list. Each page includes a table of readings for the entire season. So whether you want to plan ahead or catch up, you don’t have to wonder what any day’s readings are.

<ul id="seasonsList" class="emoji-list hanging-indent">
  ${seasons.sort((a, b) => a.data.weight - b.data.weight)
    .map(season => seasonListItem(season), this).join('\n')}
</ul>`
}
