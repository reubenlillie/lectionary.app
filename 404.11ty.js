/**
 * @file Defines the chained template for the 404 page
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */
export var data ={
  title: 'Page not found',
  layout: 'layouts/page',
  permalink: '404.html',
  templateEngineOverride: '11ty.js,md',
  eleventyExcludeFromCollections: true
}

/**
 * The content of the 404 page template
 * @param {Object} data Eleventy’s `data` object
 * @return {String} The rendered template
 * @see {@link https://www.11ty.dev/docs/quicktips/not-found/ 404 pages in Eleventy}
 */
export function render({pkg: {homepage, bugs}}) {
  return `<!-- ./404.11ty.js -->

Looks like you've followed a broken link or entered a URL that doesn't exist.

Try one of these options:

* [Return to the homepage](${homepage})
* [File an issue in Git](${bugs.url})`
}
