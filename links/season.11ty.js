/**
 * @file Defines a link to the page for the current season
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */
export var data = {
  emojiId: 'calendar',
  navTitle: 'Season',
  tags: ['nav'],
  weight: 2,
  eleventyComputed: {
    url: data => {
      /** @type {Object[]} The name of the current season */
      var arr = data.days.season.split(' ')

      /** @type {string} Is the season name longer than one word? */
      var str = arr.length === 1
        // Advent, Christmas, Epiphany, Lent, or Easter
        ? arr[0]
        // after-Pentecost
        : arr.slice(-2).join('-')

      // Format the string as a relative URL
      return `/${str.toLowerCase()}`
    }
  }
}
