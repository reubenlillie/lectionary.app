/**
 * @file Defines a link for the dontations page
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */
export var data = {
  emojiId: 'donate',
  cta: 'Donate',
  name: 'Donate',
  element: 'button',
  attr: 'zeffy-form-link="https://www.zeffy.com/embed/donation-form/86f4012a-1cb7-4b9a-aae8-e22e854c2a06?modal=true"',
  tags: 'cta',
}
