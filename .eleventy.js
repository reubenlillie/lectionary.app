/**
 * @file Configures preferences for Eleventy
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @see {@link https://www.11ty.dev/docs/config/ Eleventy Documentation}
 * @since 0.1.0
 * @since 1.2.0 Add `toCamelCase()` and `importFromDir()`
 */

/*
 * Import native Node.js modules
 */
import fs from 'node:fs/promises'
import {parse} from 'path'

/**
 * Convert a hyphenated file path to camel case
 * @since 1.2.0
 * @param {string} fileName A hyphenated file name (e.g., file-name.js)
 * @return {string} camelCase (e.g. fileName)
 * @see {@link https://nodejs.org/api/path.html#pathparsepath Node.js `path.parse(path)`}
 */
function toCamelCase(fileName) {
  /** @type {string} Convert the file name to camel case */
  var camelCase = fileName.replace(/-([a-z0-9])/g, str => str[1].toUpperCase())

  // Remove the file extension
  return parse(camelCase).name
}

/**
 * Eleventy’s configuration module
 * @module .eleventy
 * @param {Object} eleventyConfig Eleventy’s Config API
 * @return {Object} Eleventy’s Config object optional
 * @see {@link https://www.11ty.dev/docs/config/ Configuring Eleventy}
 */
export default async function (eleventyConfig) {

  /**
   * Import default modules from a directory and configure them with Eleventy
   * @since 1.2.0
   * @param {string} dir Directory path from which to import files
   * @param {function} [callback=addJavaScriptFunction] `eleventyConfig` method
   * @return {Promise<function>} Passes each default export to the `callback`
   * @see toCamelCase
   * @see {@link https://discord.com/channels/741017160297611315/1276983631184662663 Eleventy Discord}
   * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/import Dynamic `import()` on MDN}
   */
  async function importFromDir(dir, callback = 'addJavaScriptFunction') {
    /** @type {string[]} List the files in a directory */
    var files = await fs.readdir(dir)

    // Run on each of the files
    return await Promise.all(files.map(async fileName => {
      /** @type {string} Format fileName to pass to eleventyConfig[callback] */
      var name = toCamelCase(fileName)

      /** @type {Promise<function>} Import the module dynamically */
      var module = await import(`${dir}/${fileName}`)

      eleventyConfig[callback](name, module.default)
    }))
  }

  /*
   * Configure Filters, functions, shortcodes, and transforms
   */
  await importFromDir('./_includes/filters')
  await importFromDir('./_includes/functions')
  await importFromDir('./_includes/shortcodes')
  await importFromDir('./_includes/transforms', 'addTransform')

  /**
   * Combine data in the Eleventy data cascade, rather than overwriting it
   * @see {@link https://www.11ty.dev/docs/data-deep-merge/ Data deep merge in Eleventy}
   */
  eleventyConfig.setDataDeepMerge(true)

  /**
   * Copie static assets to the output directory
   * @see {@link https://www.11ty.dev/docs/copy/ Passthrough copy in Eleventy}
   */
  eleventyConfig.addPassthroughCopy('favicons')
  eleventyConfig.addPassthroughCopy('fonts')
  eleventyConfig.addPassthroughCopy('img')

  /**
   * Have Eleventy watch the following additional files for live browsersync
   * @see @{@link https://www.11ty.dev/docs/config/#add-your-own-watch-targets Add your own watch targets in Eleventy}
   */
  eleventyConfig.addWatchTarget('./**/*.css')

  /**
   * Serve the rendered 404 page when using `eleventy --serve` locally
   * @see {@link https://www.11ty.dev/docs/quicktips/not-found/#with-serve 404 pages in Eleventy}
   */
  eleventyConfig.setBrowserSyncConfig({
    callbacks: {
      ready: (err, bs) => {
        bs.addMiddleware('*', (req, res) => {
          const content_404 = fs.readFileSync('_site/404.html');
          // Provides the 404 content without redirect
          res.write(content_404);
          // Add 404 http status code in request header
          // res.writeHead(404, { "Content-Type": "text/html" })
          res.writeHead(404);
          res.end()
        })
      }
    }
  })

}
