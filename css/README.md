# `./css/`

This directory contains stylesheets (CSS) for the app.

These stylesheets are concatenated and loaded internally via calls to [`this.minifyCSS()`](https://gitlab.com/reubenlillie/lectionary.app/-/blob/master/_includes/filters/minify-css.js) and [`this.internalCSS()`](https://gitlab.com/reubenlillie/lectionary.app/-/blob/master/_includes/shortcodes/internal-css.js) in [`_includes/layouts/base.11ty.js`](https://gitlab.com/reubenlillie/lectionary.app/-/blob/master/_includes/layouts/base.11ty.js).
