# `./img/`

This directory contains image files for the app.

Load <abbr title="SVG: Scalable Vector Graphics">SVGs</abbr> inline with [`this.fileToString()`](https://gitlab.com/reubenlillie/lectionary.app/-/blob/master/_includes/filters/file-to-string.js).

For example:

```js
/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 */
exports.render = async data => `
  <!-- An inline SVG -->
  <figure>${this.fileToString('img/logo-wordmark.svg')}</figure>
`
```
