/**
 * @file Contains global metadata for colors
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global color data module
 * @module _data/colors
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 * @see {@link https://nazarene.org/logo Church of the Nazarene Brand Guide }
 * @see {@link https://coolors.co/f4d40f-53819a-ef4237-be606e-57446c-40996d-7d7e81 Pink or “Rose” supplement using Coolers.co} 
 */
export default {
  red: {
    hex: '#ef4237',
    rgb: '239, 66, 55'
  },
  yellow: {
    hex: '#f89626',
    rgb: '244, 212, 15'
  },
  green: {
    hex: '#40996d',
    rgb: '64, 153, 109'
  },
  blue: {
    hex: '#53819a',
    rgb: '83, 129, 154'
  },
  pink: {
    hex: 'be606e',
    rgb: '190, 96, 110'
  },
  purple: {
    hex: '#57446c',
    rgb: '87, 68, 108'
  },
  white: {
    hex: '#fff',
    rgb: '255, 255, 255'
  },
  lightGray: {
    hex: '#dedfe0',
    rgb: '222, 223, 224'
  },
  lighterGray: {
    hex: '#abadb0',
    rgb: '171, 173, 176'
  },
  mediumGray: {
    hex: '#7d7e81',
    rgb: '125, 126, 129'
  },
  darkerGray: {
    hex: '#4a4b4c',
    rgb: '74, 75, 76'
  },
  darkGray: {
    hex: '#414042',
    rgb: '65, 64, 66'
  },
  black: {
    hex: '#231f20',
    rgb: '35, 31, 32'
  },
  facebook: {
    hex: '#1877f2',
    rgb: '24, 119, 242'
  },
  instagram: {
    hex: '#e1306c',
    rgb: '225, 48, 108'
  },
  linkedin: {
    hex: '#1666C5',
    rgb: '22, 102, 197'
  },
  messenger: {
    hex: '#0084ff',
    rgb: '0, 132, 255'
  },
  twitter: {
    hex: '#1da1f2',
    rgb: '29, 161, 242'
  },
  git: {
    hex: '#f43c00',
    rgb: '244, 60, 0'
  }
}
