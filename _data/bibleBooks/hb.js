/**
 * @file Contains global data for the books of the Hebrew Bible
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global books of the Hebrew Bible data module
 * @module _data/books/hb
 * @since 0.10.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  order: 1,
  name: 'Books of the Hebrew Bible',
  abbr: 'HB',
  sbl: '8.3.1',
  books: [
    {
      order: 1,
      name: 'Genesis',
      abbr: 'Gen'
    },
    {
      order: 2,
      name: 'Exodus',
      abbr: 'Exod'
    },
    {
      order: 3,
      name: 'Leviticus',
      abbr: 'Lev'
    },
    {
      order: 4,
      name: 'Numbers',
      abbr: 'Num'
    },
    {
      order: 5,
      name: 'Deuteronomy',
      abbr: 'Deut'
    },
    {
      order: 6,
      name: 'Joshua',
      abbr: 'Josh'
    },
    {
      order: 7,
      name: 'Judges',
      abbr: 'Judg'
    },
    {
      order: 8,
      name: 'Ruth',
      abbr: 'Ruth'
    },
    {
      order: 9,
      name: '1 Samuel',
      abbr: '1 Sam',
      altName: '1 Kingdoms',
      altAbbr: '1 Kgdms'
    },
    {
      order: 10,
      name: '2 Samuel',
      abbr: '2 Sam',
      altName: '2 Kingdoms',
      altAbbr: '2 Kgdms'
    },
    {
      order: 11,
      name: '1 Kings',
      abbr: '1 Kgs',
      altName: '3 Kingdoms',
      altAbbr: '3 Kgdms'
    },
    {
      order: 12,
      name: '2 Kings',
      abbr: '2 Kgs',
      altName: '4 Kingdoms',
      altAbbr: '4 Kgdms'
    },
    {
      order: 13,
      name: '1 Chronicles',
      abbr: '1 Chr'
    },
    {
      order: 14,
      name: '2 Chronicles',
      abbr: '2 Chr'
    },
    {
      order: 15,
      name: 'Ezra',
      abbr: 'Ezra'
    },
    {
      order: 16,
      name: 'Nehemiah',
      abbr: 'Neh'
    },
    {
      order: 17,
      name: 'Esther',
      abbr: 'Esth'
    },
    {
      order: 18,
      name: 'Job',
      abbr: 'Job'
    },
    {
      order: 19,
      name: 'Psalms',
      abbr: 'Ps'
    },
    {
      order: 20,
      name: 'Proverbs',
      abbr: 'Prov'
    },
    {
      order: 21,
      name: 'Ecclesiates',
      abbr: 'Eccl',
      altName: 'Qoheleth',
      altAbbr: 'Qoh',
    },
    {
      order: 22,
      name: 'Song of Songs',
      abbr: 'Song',
      altName: 'Canticles',
      altAbbr: 'Can',
    },
    {
      order: 23,
      name: 'Isaiah',
      abbr: 'Isa'
    },
    {
      order: 24,
      name: 'Jeremiah',
      abbr: 'Jer'
    },
    {
      order: 25,
      name: 'Lamentations',
      abbr: 'Lam'
    },
    {
      order: 26,
      name: 'Ezekiel',
      abbr: 'Ezek'
    },
    {
      order: 27,
      name: 'Daniel',
      abbr: 'Dan'
    },
    {
      order: 28,
      name: 'Hosea',
      abbr: 'Hos'
    },
    {
      order: 29,
      name: 'Amos',
      abbr: 'Amos'
    },
    {
      order: 30,
      name: 'Obadiah',
      abbr: 'Obad'
    },
    {
      order: 31,
      name: 'Jonah',
      abbr: 'Jonah'
    },
    {
      order: 32,
      name: 'Micah',
      abbr: 'Mic'
    },
    {
      order: 32,
      name: 'Nahum',
      abbr: 'Nah'
    },
    {
      order: 33,
      name: 'Habakkuk',
      abbr: 'Hab'
    },
    {
      order: 34,
      name: 'Zephaniah',
      abbr: 'Zeph'
    },
    {
      order: 35,
      name: 'Haggai',
      abbr: 'Hag'
    },
    {
      order: 36,
      name: 'Zechariah',
      abbr: 'Zech'
    },
    {
      order: 37,
      name: 'Malachi',
      abbr: 'Mal'
    }
  ]
}
