/**
 * @file Contains global data for the deuterocanonical works of the Bible
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global books of the New Testament data module
 * @module _data/books/deutercanonical
 * @since 0.10.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  order: 2,
  name: 'Deuterocanonical Works',
  sbl: '8.3.3',
  books: [
    {
      order: 1,
      name: 'Tobit',
      abbr: 'Tob'
    },
    {
      order: 2,
      name: 'Judith',
      abbr: 'Jdt'
    },
    {
      order: 3,
      name: 'Additions to Esther',
      abbr: 'Add Esth'
    },
    {
      order: 4,
      name: 'Wisdom of Solomon',
      abbr: 'Wis'
    },
    {
      order: 5,
      name: 'Sirach',
      abbr: 'Sir',
      altName: 'Ecclesiasticus'
    },
    {
      order: 6,
      name: 'Baruch',
      abbr: 'Bar'
    },
    {
      order: 7,
      name: 'Epistile of Jeremiah',
      abbr: 'Ep Jer'
    },
    {
      order: 8,
      name: 'Additions to Daniel',
      abbr: 'Add Dan'
    },
    {
      order: 9,
      name: 'Susanna',
      abbr: 'Sus'
    },
    {
      order: 10,
      name: 'Bel and the Dragon',
      abbr: 'Bel'
    },
    {
      order: 11,
      name: '1 Maccabees',
      abbr: '1 Macc'
    },
    {
      order: 12,
      name: '2 Maccabees',
      abbr: '2 Macc'
    },
    {
      order: 13,
      name: '1 Esdras',
      abbr: '1 Esd'
    },
    {
      order: 14,
      name: 'Prayer of Manasseh',
      abbr: 'Pr Man'
    },
    {
      order: 15,
      name: 'Psalm 151',
      abbr: 'Ps 151'
    },
    {
      order: 16,
      name: '3 Maccabees',
      abbr: '3 Macc'
    },
    {
      order: 17,
      name: '2 Esdras',
      abbr: '2 Esd'
    },
    {
      order: 18,
      name: '4 Maccabees',
      abbr: '4 Macc'
    }
  ]
}
