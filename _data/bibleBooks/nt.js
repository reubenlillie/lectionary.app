/**
 * @file Contains global data for the books of the New Testament
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global books of the New Testament data module
 * @module _data/books/nt
 * @since 0.10.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  order: 3,
  name: 'Books of the New Testament',
  abbr: 'NT',
  sbl: '8.3.2',
  books: [
    {
      order: 1,
      name: 'Matthew',
      abbr: 'Matt'
    },
    {
      order: 2,
      name: 'Mark',
      abbr: 'Mark'
    },
    {
      order: 3,
      name: 'Luke',
      abbr: 'Luke'
    },
    {
      order: 4,
      name: 'John',
      abbr: 'John'
    },
    {
      order: 5,
      name: 'Acts',
      abbr: 'Acts'
    },
    {
      order: 6,
      name: 'Romans',
      abbr: 'Rom'
    },
    {
      order: 7,
      name: '1 Corinthians',
      abbr: '1 Cor'
    },
    {
      order: 8,
      name: '2 Corinthians',
      abbr: '2 Cor'
    },
    {
      order: 9,
      name: 'Galatians',
      abbr: 'Gal'
    },
    {
      order: 10,
      name: 'Ephesians',
      abbr: 'Eph'
    },
    {
      order: 11,
      name: 'Philippians',
      abbr: 'Phil'
    },
    {
      order: 12,
      name: 'Colossians',
      abbr: 'Col'
    },
    {
      order: 13,
      name: '1 Thessalonians',
      abbr: '1 Thess'
    },
    {
      order: 14,
      name: '2 Thessalonians',
      abbr: '2 Thess'
    },
    {
      order: 15,
      name: '1 Timothy',
      abbr: '1 Tim'
    },
    {
      order: 16,
      name: '2 Timothy',
      abbr: '2 Tim'
    },
    {
      order: 17,
      name: 'Titus',
      abbr: 'Titus'
    },
    {
      order: 18,
      name: 'Philemon',
      abbr: 'Phlm'
    },
    {
      order: 19,
      name: 'Hebrews',
      abbr: 'Heb'
    },
    {
      order: 20,
      name: 'James',
      abbr: 'Jas'
    },
    {
      order: 21,
      name: '1 Peter',
      abbr: '1 Pet'
    },
    {
      order: 22,
      name: '2 Peter',
      abbr: '2 Pet'
    },
    {
      order: 23,
      name: '1 John',
      abbr: '1 John'
    },
    {
      order: 24,
      name: '2 John',
      abbr: '2 John'
    },
    {
      order: 25,
      name: '3 John',
      abbr: '3 John'
    },
    {
      order: 26,
      name: 'Jude',
      abbr: 'Jude'
    },
    {
      order: 27,
      name: 'Revelation',
      abbr: 'Rev'
    }
  ]
}
