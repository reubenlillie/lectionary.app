# `./_data/bibleBooks`

This directory contains [global data files](https://www.11ty.dev/docs/data-global/) for the books of the Bible and their abbreviations. Books a separated the following arrays of primary sources:

* Hebrew Bible (Old Testament) in `hb.js`
* Deuterocanonical Works (Apocrypha) in `deutercanonical.js`
* New Testament in `nt.js`

## References

[Society of Biblical Literature](https://www.sbl-site.org/). 2014. “Abbreviations: Primary Sources: Ancient Texts.” In _SBL Handbook of Style: For Biblical Studies and Related Disciplines_, 2nd ed., 8.3.1–3. Atlanta: SBL Press.
