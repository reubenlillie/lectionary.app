/**
 * @file Contains global metadata for configuring the app
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global app data module
 * @module _data/app
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  name: 'The Lectionary.app',
  author: {
    'name': 'Reuben L. Lillie'
  },
  locale: {
    code: 'en',
    dateOptions: {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    }
  },
  baseUrl: 'https://lectionary.app'
}
