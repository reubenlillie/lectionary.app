/**
 * @file Contains global data for searching BibleGateway.com
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for searching BibleGateway.com
 * @module _data/bibleGateway
 * @since 2.2.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 * @see {@link https://www.biblegateway.com/ BibleGateway.com}
 */
export default {
  // BibleGateway’s search URL before the Bible reference query string
  queryStringPefix: 'https://www.biblegateway.com/passage/?search=',
  // Query parameters to follow the Bible reference query string
  queryParameters: {
    // Default Bible translation (BibleGateway’s abbreviation)
    version: 'NRSVUE' // &version=NRSVUE
  }
}
