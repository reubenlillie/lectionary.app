/**
 * @file Contains global data for contact information
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global contact data module
 * @module _data/contact
 * @since 0.1.0
 * @since 1.0.0 Remove `email`
 * @since 0.11.0 Add `emojiId` properties
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data Eleventy}
 */
export default {
  address: {
    street: '633 S Plymouth Ct, #703',
    city: 'Chicago',
    state: 'IL',
    zip: '60605'
  },
  social: [
    {
      name: 'Git',
      cta: 'Check out the source code',
      emojiId: 'free',
      url: 'https://gitlab.com/reubenlillie/lectionary.app'
    }
  ]
}
