/**
 * @file Contains global data for emoji 
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global emoji data module
 * @module _data/emoji
 * @since 0.11.0
 * @since 1.0.0 Add toggle-on and toggle-off
 * @since 2.1.0 Add prayer-hands-open and prayer-hands-folded
 * @since 2.2.0 Add link and open-book
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  name: 'Emoji Key',
  symbols: [
    /*
     * Readings
     */
    {
      id: 'morning',
      emoji: '🌞',
      symbolizes: 'Morning reading'
    },
    {
      id: 'evening',
      emoji: '🌛',
      symbolizes: 'Evening reading'
    },
    {
      id: 'psalm',
      emoji: '🎶',
      symbolizes: 'Psalm'
    },
    {
      id: 'first',
      emoji: '1️⃣',
      symbolizes: 'First reading'
    },
    {
      id: 'second',
      emoji: '2️⃣',
      symbolizes: 'Second reading'
    },
    {
      id: 'gospel',
      emoji: '🗞️',
      symbolizes: 'Gospel reading'
    },
    {
      id: 'alt',
      emoji: '🔀',
      symbolizes: 'Alternate reading'
    },
    /*
     * Liturgical seasons
     */
    {
      id: 'purple-heart',
      emoji: '💜',
      symbolizes: '<a href="/advent/">Advent</a> and <a href="/lent/">Lent</a>'
    },
    {
      id: 'pink-heart',
      emoji: '💗',
      symbolizes: 'The Third Sunday of Advent and the Fourth Sunday in Lent'
    },
    {
      id: 'white-heart',
      emoji: '🤍',
      symbolizes: '<a href="/christmas/">Christmas time</a> and <a href="/easter/">Easter time</a>; Trinity Sunday; Holy days of Jesus (except the Passion), his mother Mary, and saints who were not martyrs; St. John the Evangelist (December 27); the Conversion of St. Paul (January 25); the Nativity of St. John the Baptist (June 24); and All Saints’ Day (November 1)'
    },
    {
      id: 'green-heart',
      emoji: '💚',
      symbolizes: 'Ordinary Time (after <a href="/epiphany/">Epiphany</a> and after <a href="/after-pentecost/">Pentecost</a>)'
    },
    {
      id: 'red-heart',
      emoji: '❤️',
      symbolizes: 'Palm Sunday, Good Friday, Christ’s Passion, Pentecost Sunday, apostles, evangelists, and saints who were martyrs.'
    },
    /*
     * Icons
     */
    {
      id: 'key',
      emoji: '🔑',
      exclude: true
    },
    {
      id: 'references',
      emoji: '📓',
      exclude: true
    },
    {
      id: 'donate',
      emoji: '🫶',
      exclude: true
    },
    {
      id: 'contact',
      emoji: '💌',
      exclude: true
    },
    {
      id: 'follow',
      emoji: '👣',
      exclude: true
    },
    {
      id: 'free',
      emoji: '🆓',
      exclude: true
    },
    {
      id: 'information',
      emoji: 'ℹ️',
      exclude: true
    },
    {
      id: 'thought-balloon',
      emoji: '💭',
      exclude: true
    },
    {
      id: 'calendar',
      emoji: '🗓️',
      exclude: true
    },
    {
      id: 'copyright',
      emoji: '©️',
      exclude: true
    },
    {
      id: 'privacy',
      emoji: '🔒',
      exclude: true
    },
    {
      id: 'policy',
      emoji: '📄',
      exclude: true
    },
    {
      id: 'accessibility',
      emoji: '♿',
      exclude: true
    },
    {
      id: 'toggle-on',
      emoji: '🌂',
      exclude: true
    },
    {
      id: 'toggle-off',
      emoji: '☂️',
      exclude: true
    },
    {
      id: 'prayer-hands-folded',
      emoji: '🙏',
      exclude: true
    },
    {
      id: 'prayer-hands-open',
      emoji: '🤲',
      exclude: true
    },
    {
      id: 'link',
      emoji: '🔗',
      symbolizes: 'External link (See our <a href="/copyright/">copyright notice</a> for more information)'
    },
    {
      id: 'open-book',
      emoji: '📖',
      exclude: true
    }
  ]
}
