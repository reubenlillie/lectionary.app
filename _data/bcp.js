/**
 * @file Contains global data for the Book of Common Prayer (BCP)
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global BCP data module
 * @module _data/bcp
 * @since 0.1.0
 * @since 1.0.0 Upgrade edition to 2016
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  pdf: 'https://episcopalchurch.org/files/book_of_common_prayer.pdf',
  edition: '1979[2016]'
}
