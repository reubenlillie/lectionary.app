/**
 * @file Exposes environment variables to Eleventy
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Uses Node.js’s `process.env` property to run different code 
 * based on the environment, such as development and production
 * @module _data/project
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/data-js/#example-exposing-environment-variables Environment variables in Eleventy}
 */
export default {
  environment: process.env.ELEVENTY_ENV
}
