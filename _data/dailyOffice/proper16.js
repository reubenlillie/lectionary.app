/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 16
 * @module _data/dailyOffice/proper16
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 16',
    collects: {
      contemporary: [
        {
          text: 'Grant, O merciful God, that your Church, being gathered together in unity by your Holy Spirit, may show forth your power among all peoples, to the glory of your Name; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 232
        }
      ],
      traditional: [
        {
          text: 'Grant, we beseech thee, merciful God, that thy Church, being gathered together in unity by thy Holy Spirit, may manifest thy power among all peoples, to the glory of thy Name; through Jesus Christ our Lord, who liveth and reigneth with thee and the same Spirit, one God, world without end.',
          bcpPage: 181
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 980
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: '2 Sam 24:1–2, 10–15',
            second: 'Gal 3:23–4:7',
            gospel: 'John 8:12–20'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: '1 Kgs 1:5–31',
            second: 'Acts 25:1–23',
            gospel: 'Mark 13:14–27'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: '1 Kgs 1:38–2:4',
            second: 'Acts 26:24–27:8',
            gospel: 'Mark 13:28–37'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: '1 Kgs 3:1–15',
            second: 'Acts 27:9–26',
            gospel: 'Mark 14:1–11'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: '1 Kgs 3:16–28',
            second: 'Acts 27:27–44',
            gospel: 'Mark 14:12–26'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: '1 Kgs 5:1–6:1,7',
            second: 'Acts 28:1–16',
            gospel: 'Mark 14:27–42'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: '1 Kgs 7:51–8:21',
            second: 'Acts 28:17–31',
            gospel: 'Mark 14:43–52'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 981
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Job 4:1–6, 12–21',
            second: 'Rev 4:1–11',
            gospel: 'Mark 6:1–6a'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Job 4:1; 5:1–11, 17–21, 26–27',
            second: 'Acts 9:19b–31',
            gospel: 'John 6:52–59'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Job 6:1–4',
            second: 'Acts 9:32–43',
            gospel: 'John 6:60–71'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Job 6:1, 7:1–21',
            second: 'Acts 10:1–16',
            gospel: 'John 7:1–13'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Job 8:1–10, 20–22',
            second: 'Acts 10:17–33',
            gospel: 'John 7:14–36'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Job 9:1–15, 32–35',
            second: 'Acts 10:34–48',
            gospel: 'John 7:37–52'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Job 9:1; 10:1–9, 16–22',
            second: 'Acts 11:1–18',
            gospel: 'John 8:12–20'
          }
        }
      ]
    }
  ]
}
