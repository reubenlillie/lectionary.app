/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 11
 * @module _data/dailyOffice/proper11
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 11',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, the fountain of all wisdom, you know our necessities before we ask and our ignorance in asking: Have compassion on our weakness, and mercifully give us those things which for our unworthiness we dare not, and for our blindness we cannot ask; through the worthiness of your Son Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 231
        }
      ],
      traditional: [
        {
          text: 'Almighty God, the fountain of all wisdom, who knowest our necessities before we ask and our ignorance in asking: Have compassion, we beseech thee, upon our infirmities, and those things which for our unworthiness we dare not, and for our blindness we cannot ask, mercifully give us for the worthiness of thy Son Jesus Christ our Lord; who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 179
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 976
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: '1 Sam 23:7–18',
            second: 'Rom 11:33–12:2',
            gospel: 'Matt 25:14–30'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: '1 Sam 24:1–22',
            second: 'Acts 13:44–52',
            gospel: 'Mark 4:1–20'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '42'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: '1 Sam 25:1–22',
            second: 'Acts 14:1–18',
            gospel: 'Mark 4:21–34'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: '1 Sam 25:23–44',
            second: 'Acts 14:19–28',
            gospel: 'Mark 4:35–41'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '66',
              '67'
            ]
          },
          lessons: {
            first: '1 Sam 28:3–20',
            second: 'Acts 15:1–11',
            gospel: 'Mark 5:1–20'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: '1 Sam 31:1–13',
            second: 'Acts 15:12–21',
            gospel: 'Mark 5:21–43'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: '2 Sam 1:1–16',
            second: 'Acts 15:22–35',
            gospel: 'Mark 6:1–13'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 977
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Josh 6:15–27',
            second: 'Acts 22:30–23:11',
            gospel: 'Mark 2:1–12'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Josh 7:1–13',
            second: 'Rom 13:8–14',
            gospel: 'Matt 26:36–46'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '42'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Josh 8:1–22',
            second: 'Rom 14:1–12',
            gospel: 'Matt 26:47–56'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Josh 8:30–35',
            second: 'Rom 14:13–23',
            gospel: 'Matt 26:57–68'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '66',
              '67'
            ]
          },
          lessons: {
            first: 'Josh 9:3–21',
            second: 'Rom 15:1–13',
            gospel: 'Matt 26:69–75'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Josh 9:22–10:15',
            second: 'Rom 15:14–24',
            gospel: 'Matt 27:1–10'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Josh 23:1–16',
            second: 'Rom 15:25–33',
            gospel: 'Matt 27:11–23'
          }
        }
      ]
    }
  ]
}
