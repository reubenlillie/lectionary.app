/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Epiphany and Following
 * @module _data/dailyOffice/epiphany0
 * @since 0.1.0
 * @since 0.11.0 Add `color` properties
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'The Epiphany and Following',
    collects: {
      contemporary: [
        {
          text: 'O God, by the leading of a star you manifested your only Son to the peoples of the earth: Lead us, who know you now by faith, to your presence, where we may see your glory face to face; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 214
        }
      ],
      traditional: [
        {
          text: 'O God, who by the leading of a star didst manifest thy onlybegotten Son to the peoples of the earth: Lead us, who know thee now by faith, to thy presence, where we may behold thy glory face to face; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 162
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        season: 'Epiphany',
        week: 'The Epiphany and Following',
        bcpPage: 942
      },
      days: [
        {
          day: 'Jan 6',
          title: {
            full: 'The Epiphany, or the Manifestation of Christ to the Gentiles',
            main: 'Epiphany',
            sub: 'The Manifestation of Christ to the Gentiles'
          },
          color: 'white',
          psalms: {
            morning: [
              '46',
              '97'
            ],
            evening: [
              '96',
              '100'
            ]
          },
          lessons: {
            first: 'Isa 52:7–10',
            second: 'Rev 21:22–27',
            gospel: 'Matt 12:14–21'
          }
        },
        {
          day: 'Jan 7',
          psalms: {
            morning: [
              '103'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Isa 52:3–6',
            second: 'Rev 2:1–7',
            gospel: 'John 2:1–11'
          }
        },
        {
          day: 'Jan 8',
          psalms: {
            morning: [
              '117',
              '118'
            ],
            evening: [
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Isa 59:15–21',
            second: 'Rev 2:8–17',
            gospel: 'John 4:46–54'
          }
        },
        {
          day: 'Jan 9',
          psalms: {
            morning: [
              '121',
              '122',
              '123'
            ],
            evening: [
              '131',
              '132'
            ]
          },
          lessons: {
            first: 'Isa 63:1–5',
            second: 'Rev 2:18–29',
            gospel: 'John 5:1–15'
          }
        },
        {
          day: 'Jan 10',
          psalms: {
            morning: [
              '138',
              '139:1–17(18–23)'
            ],
            evening: [
              '147'
            ]
          },
          lessons: {
            first: 'Isa 65:1–9',
            second: 'Rev 3:1–6',
            gospel: 'John 6:1–14'
          }
        },
        {
          day: 'Jan 11',
          psalms: {
            morning: [
              '148',
              '150'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Isa 65:13–16',
            second: 'Rev 3:7–13',
            gospel: 'John 6:15–27'
          }
        },
        {
          day: 'Jan 12',
          psalms: {
            morning: [
              '98',
              '99',
              '[100]'
            ]
          },
          lessons: {
            first: 'Isa 66:1–2, 22–23',
            second: 'Rev 3:14–22',
            gospel: 'John 9:1–12, 35–38'
          }
        },
        {
          title: 'Eve of the Baptism',
          psalms: {
            evening: [
              '104'
            ]
          },
          lessons: {
            evening: {
              first: 'Isa 61:1–9',
              second: 'Gal 3:23–29; 4:4–7'
            }
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 943
      },
      days: [
        {
          day: 'Jan 6',
          title: {
            full: 'The Epiphany, or the Manifestation of Christ to the Gentiles',
            main: 'Epiphany',
            sub: 'The Manifestation of Christ to the Gentiles'
          },
          color: 'white',
          psalms: {
            morning: [
              '46',
              '97'
            ],
            evening: [
              '96',
              '100'
            ]
          },
          lessons: {
            first: 'Isa 49:1–7',
            second: 'Rev 21:22–27',
            gospel: 'Matt 12:14–21'
          }
        },
        {
          day: 'Jan 7',
          psalms: {
            morning: [
              '103'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Deut 8:1–3',
            second: 'Col 1:1–14',
            gospel: 'John 6:30–33, 48–51'
          }
        },
        {
          day: 'Jan 8',
          psalms: {
            morning: [
              '117',
              '118'
            ],
            evening: [
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Exod 17:1–7',
            second: 'Col 1:15–23',
            gospel: 'John 7:37–52'
          }
        },
        {
          day: 'Jan 9',
          psalms: {
            morning: [
              '121',
              '122',
              '123'
            ],
            evening: [
              '131',
              '132'
            ]
          },
          lessons: {
            first: 'Isa 45:14–19',
            second: 'Col 1:24–2:17',
            gospel: 'John 8:12–19'
          }
        },
        {
          day: 'Jan 10',
          psalms: {
            morning: [
              '138',
              '139:1–17(18–23)'
            ],
            evening: [
              '147'
            ]
          },
          lessons: {
            first: 'Jer 23:1–8',
            second: 'Col 2:8–23',
            gospel: 'John 10:7–17'
          }
        },
        {
          day: 'Jan 11',
          psalms: {
            morning: [
              '148',
              '150'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Isa 55:3–9',
            second: 'Col 3:1–17',
            gospel: 'John 14:6–14'
          }
        },
        {
          day: 'Jan 12',
          psalms: {
            morning: [
              '98',
              '99',
              '[100]'
            ]
          },
          lessons: {
            first: 'Gen 49:1–2, 8–12',
            second: 'Col 3:–18–4:6',
            gospel: 'John 15:1–16'
          }
        },
        {
          title: 'Eve of the Baptism',
          psalms: {
            evening: [
              '104'
            ]
          },
          lessons: {
            evening: {
              first: 'Isa 61:1–9',
              second: 'Gal 3:23–29; 4:4–7'
            }
          }
        }
      ]
    }
  ]
}
