/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 4
 * @module _data/dailyOffice/proper4
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 4',
    collects: {
      contemporary: [
        {
          text: 'O God, your never-failing providence sets in order all things both in heaven and earth: Put away from us, we entreat you, all hurtful things, and give us those things which are profitable for us; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 229
        }
      ],
      traditional: [
        {
          text: 'O God, whose never-failing providence ordereth all things both in heaven and earth: We humbly beseech thee to put away from us all hurtful things, and to give us those things which are profitable for us; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 177
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 968
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Deut 11:1–12',
            second: 'Rev 10:1–11',
            gospel: 'Matt 13:44–58'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Deut 11:13–19',
            second: '2 Cor 5:11–6:2',
            gospel: 'Luke 17:1–10'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Deut 12:1–12',
            second: '2 Cor 6:2–13(14–7:1)',
            gospel: 'Luke 17:11–19'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Deut 13:1–11',
            second: '2 Cor 7:2–16',
            gospel: 'Luke 17:20–37'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Deut 16:18–20; 17:14–20',
            second: '2 Cor 8:1–16',
            gospel: 'Luke 18:1–8'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Deut 26:1–11',
            second: '2 Cor 8:16–24',
            gospel: 'Luke 18:9–14'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Deut 29:2–15',
            second: '2 Cor 9:1–15',
            gospel: 'Luke 18:15–30'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 969
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Eccl 1:1–11',
            second: 'Acts 8:26–40',
            gospel: 'Luke 11:1–13'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Eccl 2:1–15',
            second: 'Gal 1:1–17',
            gospel: 'Matt 13:44–52'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Eccl 2:16–26',
            second: 'Gal 1:18–2:10',
            gospel: 'Matt 13:53–58'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Eccl 3:1–15',
            second: 'Gal 2:11–21',
            gospel: 'Matt 14:1–12'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Eccl 3:16–4:3',
            second: 'Gal 3:1–14',
            gospel: 'Matt 14:13–21'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Eccl 5:1–7',
            second: 'Gal 3:15–22',
            gospel: 'Matt 14:22–36'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Eccl 5:8–20',
            second: 'Gal 3:23–4:11',
            gospel: 'Matt 15:1–20'
          }
        }
      ]
    }
  ]
}
