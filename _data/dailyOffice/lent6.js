/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Holy Week
 * @module _data/dailyOffice/lent6
 * @since 0.1.0
 * @since 0.11.0 Specify morning and evening lessons for Holy Saturday
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Lent',
    week: 'Holy Week',
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 956
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'The Sunday of the Passion: Palm Sunday',
            main: 'Palm Sunday',
            sub: 'Sunday of the Passion'
          },
          color: 'red',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            morning: {
              first: 'Zech 9:9–12',
              second: '1 Tim 6:12–16'
            },
            evening: {
              first: 'Zech 12:9–11; 13:1, 7–9',
              gospel: 'Matt 21:12–17'
            }
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty and everliving God, in your tender love for the human race you sent your Son our Savior Jesus Christ to take upon him our nature, and to suffer death upon the cross, giving us the example of his great humility: Mercifully grant that we may walk in the way of his suffering, and also share in his resurrection; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 219
              }
            ],
            traditional: [
              {
                text: 'Almighty and everlasting God, who, of thy tender love towards mankind, hast sent thy Son our Savior Jesus Christ to take upon him our flesh, and to suffer death upon the cross, that all mankind should follow the example of his great humility: Mercifully grant that we may both follow the example of his patience, and also be made partakers of his resurrection; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 168
              }
            ]
          }
        },
        {
          day: 'Monday',
          title: 'Monday in Holy Week',
          color: 'red',
          psalms: {
            morning: [
              '51:1–18(19–20)'
            ],
            evening: [
              '69:1–23'
            ]
          },
          lessons: {
            first: 'Jer 12:1–16',
            second: 'Phil 3:1–14',
            gospel: 'John 12:9–19'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty God, whose most dear Son went not up to joy but first he suffered pain, and entered not into glory before he was crucified: Mercifully grant that we, walking in the way of the cross, may find it none other than the way of life and peace; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 220
              }
            ],
            traditional: [
              {
                text: 'Almighty God, whose most dear Son went not up to joy but first he suffered pain, and entered not into glory before he was crucified: Mercifully grant that we, walking in the way of the cross, may find it none other than the way of life and peace; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 168
              }
            ]
          }
        },
        {
          day: 'Tuesday',
          title: 'Tuesday in Holy Week',
          color: 'red',
          psalms: {
            morning: [
              '6',
              '12'
            ],
            evening: [
              '94'
            ]
          },
          lessons: {
            first: 'Jer 15:10–21',
            second: 'Phil 3:15–21',
            gospel: 'John 12:20–26'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, by the passion of your blessed Son you made an instrument of shameful death to be for us the means of life: Grant us so to glory in the cross of Christ, that we may gladly suffer shame and loss for the sake of your Son our Savior Jesus Christ; who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 220
              }
            ],
            traditional: [
              {
                text: 'O God, who by the passion of thy blessed Son didst make an instrument of shameful death to be unto us the means of life: Grant us so to glory in the cross of Christ, that we may gladly suffer shame and loss for the sake of thy Son our Savior Jesus Christ; who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 168
              }
            ]
          }
        },
        {
          day: 'Wednesday',
          title: 'Wednesday in Holy Week',
          color: 'red',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Jer 17:5–10, 14–17',
            second: 'Phil 4:1–13',
            gospel: 'John 12:27–36'
          },
          collects: {
            contemporary: [
              {
                text: 'Lord God, whose blessed Son our Savior gave his body to be whipped and his face to be spit upon: Give us grace to accept joyfully the sufferings of the present time, confident of the glory that shall be revealed; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 220
              }
            ],
            traditional: [
              {
                text: 'O Lord God, whose blessed Son our Savior gave his back to the smiters and hid not his face from shame: Grant us grace to take joyfully the sufferings of the present time, in full assurance of the glory that shall be revealed; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 169
              }
            ]
          }
        },
        {
          day: 'Thursday',
          title: 'Maundy Thursday',
          color: 'white',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '142',
              '143'
            ]
          },
          lessons: {
            first: 'Jer 20:7–11',
            second: '1 Cor 10:14–17; 11:27–32',
            gospel: 'John 17:1–11(12–26)'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty Father, whose dear Son, on the night before he suffered, instituted the Sacrament of his Body and Blood: Mercifully grant that we may receive it thankfully in remembrance of Jesus Christ our Lord, who in these holy mysteries gives us a pledge of eternal life; and who now lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 221
              }
            ],
            traditional: [
              {
                text: 'Almighty Father, whose dear Son, on the night before he suffered, did institute the Sacrament of his Body and Blood: Mercifully grant that we may thankfully receive the same in remembrance of him who in these holy mysteries giveth us a pledge of life eternal, the same thy Son Jesus Christ our Lord; who now liveth and reigneth with thee and the Holy Spirit ever, one God, world without end.',
                bcpPage: 169
              }
            ]
          }
        },
        {
          day: 'Friday',
          title: 'Good Friday',
          color: 'red',
          psalms: {
            morning: [
              '95',
              '22'
            ],
            evening: [
              '40:1–14(15–19)'
            ]
          },
          lessons: {
            first: 'Wis 1:16–2:1, 12–22',
            altFirst: 'Gen 22:1–14',
            second: '1 Pet 1:10–20',
            gospel: 'John 13:36–38',
            altGospel: 'John 19:38–42'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty God, we pray you graciously to behold this your family, for whom our Lord Jesus Christ was willing to be betrayed, and given into the hands of sinners, and to suffer death upon the cross; who now lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 221
              }
            ],
            traditional: [
              {
                text: 'Almighty God, we beseech thee graciously to behold this thy family, for which our Lord Jesus Christ was contented to be betrayed, and given up into the hands of sinners, and to suffer death upon the cross; who now liveth and reigneth with thee and the Holy Ghost ever, one God, world without end.',
                bcpPage: 169
              }
            ]
          }
        },
        {
          day: 'Saturday',
          title: 'Holy Saturday',
          color: 'white',
          psalms: {
            morning: [
              '95',
              '88'
            ],
            evening: [
              '27'
            ]
          },
          lessons: {
            morning: {
              first: 'Job 19:21–27a',
              second: 'Heb 4:1–16'
            },
            evening: {
              second: 'Rom 8:1–11'
            }
          },
          collects: {
            contemporary: [
              {
                text: 'O God, Creator of heaven and earth: Grant that, as the crucified body of your dear Son was laid in the tomb and rested on this holy Sabbath, so we may await with him the coming of the third day, and rise with him to newness of life; who now lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 221
              }
            ],
            traditional: [
              {
                text: 'O God, Creator of heaven and earth: Grant that, as the crucified body of thy dear Son was laid in the tomb and rested on this holy Sabbath, so we may await with him the coming of the third day, and rise with him to newness of life; who now liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 170
              }
            ]
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 957
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'The Sunday of the Passion: Palm Sunday',
            main: 'Palm Sunday',
            sub: 'Sunday of the Passion'
          },
          color: 'red',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            morning: {
              first: 'Zech 9:9–12',
              second: '1 Tim 6:12–16'
            },
            evening: {
              first: 'Zech 12:9–11; 13:1, 7–9',
              gospel: 'Luke 19:41–48'
            }
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty and everliving God, in your tender love for the human race you sent your Son our Savior Jesus Christ to take upon him our nature, and to suffer death upon the cross, giving us the example of his great humility: Mercifully grant that we may walk in the way of his suffering, and also share in his resurrection; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 219
              }
            ],
            traditional: [
              {
                text: 'Almighty and everlasting God, who, of thy tender love towards mankind, hast sent thy Son our Savior Jesus Christ to take upon him our flesh, and to suffer death upon the cross, that all mankind should follow the example of his great humility: Mercifully grant that we may both follow the example of his patience, and also be made partakers of his resurrection; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 168
              }
            ]
          }
        },
        {
          day: 'Monday',
          title: 'Monday in Holy Week',
          color: 'red',
          psalms: {
            morning: [
              '51:1–18(19–20)'
            ],
            evening: [
              '69:1–23'
            ]
          },
          lessons: {
            first: 'Lam 1:1–2, 6–12',
            second: '2 Cor 1:1–7',
            gospel: 'Mark 11:12–25'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty God, whose most dear Son went not up to joy but first he suffered pain, and entered not into glory before he was crucified: Mercifully grant that we, walking in the way of the cross, may find it none other than the way of life and peace; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 220
              }
            ],
            traditional: [
              {
                text: 'Almighty God, whose most dear Son went not up to joy but first he suffered pain, and entered not into glory before he was crucified: Mercifully grant that we, walking in the way of the cross, may find it none other than the way of life and peace; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 168
              }
            ]
          }
        },
        {
          day: 'Tuesday',
          title: 'Tuesday in Holy Week',
          color: 'red',
          psalms: {
            morning: [
              '6',
              '12'
            ],
            evening: [
              '94'
            ]
          },
          lessons: {
            first: 'Lam 1:17–22',
            second: '2 Cor 1:8–22',
            gospel: 'Mark 11:27–33'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, by the passion of your blessed Son you made an instrument of shameful death to be for us the means of life: Grant us so to glory in the cross of Christ, that we may gladly suffer shame and loss for the sake of your Son our Savior Jesus Christ; who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 220
              }
            ],
            traditional: [
              {
                text: 'O God, who by the passion of thy blessed Son didst make an instrument of shameful death to be unto us the means of life: Grant us so to glory in the cross of Christ, that we may gladly suffer shame and loss for the sake of thy Son our Savior Jesus Christ; who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 168
              }
            ]
          }
        },
        {
          day: 'Wednesday',
          title: 'Wednesday in Holy Week',
          color: 'red',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Lam 2:1–9',
            second: '2 Cor 1:23–2:11',
            gospel: 'Mark 12:1–11'
          },
          collects: {
            contemporary: [
              {
                text: 'Lord God, whose blessed Son our Savior gave his body to be whipped and his face to be spit upon: Give us grace to accept joyfully the sufferings of the present time, confident of the glory that shall be revealed; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 220
              }
            ],
            traditional: [
              {
                text: 'O Lord God, whose blessed Son our Savior gave his back to the smiters and hid not his face from shame: Grant us grace to take joyfully the sufferings of the present time, in full assurance of the glory that shall be revealed; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 169
              }
            ]
          }
        },
        {
          day: 'Thursday',
          title: 'Maundy Thursday',
          color: 'white',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '142',
              '143'
            ]
          },
          lessons: {
            first: 'Lam 2:10–18',
            second: '1 Cor 10:14–17; 11:27–32',
            gospel: 'Mark 14:12–25'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty Father, whose dear Son, on the night before he suffered, instituted the Sacrament of his Body and Blood: Mercifully grant that we may receive it thankfully in remembrance of Jesus Christ our Lord, who in these holy mysteries gives us a pledge of eternal life; and who now lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 221
              }
            ],
            traditional: [
              {
                text: 'Almighty Father, whose dear Son, on the night before he suffered, did institute the Sacrament of his Body and Blood: Mercifully grant that we may thankfully receive the same in remembrance of him who in these holy mysteries giveth us a pledge of life eternal, the same thy Son Jesus Christ our Lord; who now liveth and reigneth with thee and the Holy Spirit ever, one God, world without end.',
                bcpPage: 169
              }
            ]
          }
        },
        {
          day: 'Friday',
          title: 'Good Friday',
          color: 'red',
          psalms: {
            morning: [
              '95',
              '22'
            ],
            evening: [
              '40:1–14(15–19)'
            ]
          },
          lessons: {
            first: 'Lam 3:1–9, 19–33',
            second: '1 Pet 1:10–20',
            gospel: 'John 13:36–38',
            altGospel: 'John 19:38–42'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty God, we pray you graciously to behold this your family, for whom our Lord Jesus Christ was willing to be betrayed, and given into the hands of sinners, and to suffer death upon the cross; who now lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 221
              }
            ],
            traditional: [
              {
                text: 'Almighty God, we beseech thee graciously to behold this thy family, for which our Lord Jesus Christ was contented to be betrayed, and given up into the hands of sinners, and to suffer death upon the cross; who now liveth and reigneth with thee and the Holy Ghost ever, one God, world without end.',
                bcpPage: 169
              }
            ]
          }
        },
        {
          day: 'Saturday',
          title: 'Holy Saturday',
          color: 'white',
          psalms: {
            morning: [
              '95',
              '88'
            ],
            evening: [
              '27'
            ]
          },
          lessons: {
            morning: {
              first: 'Lam 3:37–58',
              second: 'Heb 4:1–16'
            },
            evening: {
              second: 'Rom 8:1–11'
            }
          },
          collects: {
            contemporary: [
              {
                text: 'O God, Creator of heaven and earth: Grant that, as the crucified body of your dear Son was laid in the tomb and rested on this holy Sabbath, so we may await with him the coming of the third day, and rise with him to newness of life; who now lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 221
              }
            ],
            traditional: [
              {
                text: 'O God, Creator of heaven and earth: Grant that, as the crucified body of thy dear Son was laid in the tomb and rested on this holy Sabbath, so we may await with him the coming of the third day, and rise with him to newness of life; who now liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 170
              }
            ]
          }
        }
      ]
    }
  ]
}
