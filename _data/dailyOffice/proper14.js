/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 14
 * @module _data/dailyOffice/proper14
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 14',
    collects: {
      contemporary: [
        {
          text: 'Grant to us, Lord, we pray, the spirit to think and do always those things that are right, that we, who cannot exist without you, may by you be enabled to live according to your will; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 232
        }
      ],
      traditional: [
        {
          text: 'Grant to us, Lord, we beseech thee, the spirit to think and do always such things as are right, that we, who cannot exist without thee, may by thee be enabled to live according to thy will; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 180
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 978
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: '2 Sam 13:1–22',
            second: 'Rom 15:1–13',
            gospel: 'John 3:22–36'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: '2 Sam 13:23–39',
            second: 'Acts 20:17–38',
            gospel: 'Mark 9:42–50'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: '2 Sam 14:1–20',
            second: 'Acts 21:1–14',
            gospel: 'Mark 10:1–16'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: '2 Sam 14:21–33',
            second: 'Acts 21:15–26',
            gospel: 'Mark 10:17–31'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: '2 Sam 15:1–18',
            second: 'Acts 21:27–36',
            gospel: 'Mark 10:32–45'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: '2 Sam 15:19–37',
            second: 'Acts 21:37–22:16',
            gospel: 'Mark 10:46–52'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: '2 Sam 16:1–23',
            second: 'Acts 22:17–29',
            gospel: 'Mark 11:1–11'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 979
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Judg 11:1–11, 29–40',
            second: '2 Cor 11:21b–31',
            gospel: 'Mark 4:35–41'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Judg 12:1–7',
            second: 'Acts 5:12–26',
            gospel: 'John 3:1–21'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Judg 13:1–15',
            second: 'Acts 5:27–42',
            gospel: 'John 3:22–36'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: 'Judg 13:15–24',
            second: 'Acts 6:1–15',
            gospel: 'John 4:1–26'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Judg 14:1–19',
            second: 'Acts 6:15–7:16',
            gospel: 'John 4:27–42'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Judg 14:20–15:20',
            second: 'Acts 7:17–29',
            gospel: 'John 4:43–54'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Judg 16:1–14',
            second: 'Acts 7:30–43',
            gospel: 'John 5:1–18'
          }
        }
      ]
    }
  ]
}
