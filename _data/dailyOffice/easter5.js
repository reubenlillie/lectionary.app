/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 5 Easter
 * @module _data/dailyOffice/easter5
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Easter',
    week: 'Week of the Fifth Sunday of Easter',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, whom truly to know is everlasting life: Grant us so perfectly to know your Son Jesus Christ to be the way, the truth, and the life, that we may steadfastly follow his steps in the way that leads to eternal life; through Jesus Christ your Son our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, for ever and ever.',
         bcpPage: 225
        }
      ],
      traditional: [
        {
          text: 'O Almighty God, whom truly to know is everlasting life: Grant us so perfectly to know thy Son Jesus Christ to be the way, the truth, and the life, that we may steadfastly follow his steps in the way that leadeth to eternal life; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, for ever and ever.',
          bcpPage: 173
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 962
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fifth Sunday of Easter',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Wis 7:22–8:1',
            second: '2 Thess 2:13–17',
            gospel: 'Matt 7:7–14'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Wis 9:1, 7–18',
            second: 'Col (3:18–4:1)2–18',
            gospel: 'Luke 7:36–50'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Wis 10:1–4(5–12)13–21',
            second: 'Rom 12:1–21',
            gospel: 'Luke 8:1–15'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Wis 13:1–9',
            second: 'Rom 13:1–14',
            gospel: 'Luke 8:16–25'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Wis 14:27–15:3',
            second: 'Rom 14:1–12',
            gospel: 'Luke 8:26–39'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Wis 16:15–17:1',
            second: 'Rom 14:13–23',
            gospel: 'Luke 8:40–56'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Wis 19:1–8, 18–22',
            second: 'Rom 15:1–13',
            gospel: 'Luke 9:1–17'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 963
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fifth Sunday of Easter',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Lev 8:1–13, 30–36',
            second: 'Heb 12:1–14',
            gospel: 'Luke 4:16–30'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Lev 16:1–19',
            second: '1 Thess 4:13–18',
            gospel: 'Matt 6:1–6, 16–18'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Lev 16:20–34',
            second: '1 Thess 5:1–11',
            gospel: 'Matt 6:7–15'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Lev 19:1–18',
            second: '1 Thess 5:12–28',
            gospel: 'Matt 6:19–24'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Lev 23:1–22',
            second: '2 Thess 1:1–12',
            gospel: 'Matt 6:25–34'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Lev 23:1–22',
            second: '2 Thess 2:1–17',
            gospel: 'Matt 7:1–12'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Lev 23:23–44',
            second: '2 Thess 3:1–18',
            gospel: 'Matt 7:13–21'
          }
        }
      ]
    }
  ]
}
