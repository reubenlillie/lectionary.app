/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 5
 * @module _data/dailyOffice/proper5
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 5',
    collects: {
      contemporary: [
        {
          text: 'O God, from whom all good proceeds: Grant that by your inspiration we may think those things that are right, and by your merciful guiding may do them; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 229
        }
      ],
      traditional: [
        {
          text: 'O God, from whom all good doth come: Grant that by thy inspiration we may think those things that are right, and by thy merciful guiding may perform the same; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 178
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 970
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Deut 29:16–29',
            second: 'Rev 12:1–12',
            gospel: 'Matt 15:29–39'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Deut 30:1–10',
            second: '2 Cor 10:1–18',
            gospel: 'Luke 18:31–43'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Deut 30:11–20',
            second: '2 Cor 11:1–21a',
            gospel: 'Luke 19:1–10'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Deut 31:30–32:14',
            second: '2 Cor 11:21b–33',
            gospel: 'Luke 19:11–27'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Sir 44:19–45:5',
            second: '2 Cor 12:1–10',
            gospel: 'Luke 19:28–40'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Sir 45:6–16',
            second: '2 Cor 12:11–21',
            gospel: 'Luke 19:41–48'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Sir 46:1–10',
            second: '2 Cor 13:1–14',
            gospel: 'Luke 20:1–8'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 971
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Eccl 6:1–12',
            second: 'Acts 10:9–23',
            gospel: 'Luke 12:32–40'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Eccl 7:1–14',
            second: 'Gal 4:12–20',
            gospel: 'Matt 15:21–28'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Eccl 8:14–9:10',
            second: 'Gal 4:21–31',
            gospel: 'Matt 15:29–39'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Eccl 9:11–18',
            second: 'Gal 5:1–15',
            gospel: 'Matt 16:1–12'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Eccl 11:1–8',
            second: 'Gal 5:16–24',
            gospel: 'Matt 16:13–20'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Eccl 11:9–12:14',
            second: 'Gal 5:25–6:10',
            gospel: 'Matt 16:21–28'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Num 3:1–13',
            second: 'Gal 6:11–18',
            gospel: 'Matt 17:1–13'
          }
        }
      ]
    }
  ]
}
