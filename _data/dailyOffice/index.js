/**
 * @file Loader file for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @since 1.2.0 Export defaults directly
 * @since 2.0.0 Merge Years One and Two
 */

// Seasons
export {default as advent1} from './advent1.js'
export {default as advent2} from './advent2.js'
export {default as advent3} from './advent3.js'
export {default as advent4} from './advent4.js'

export {default as christmas} from './christmas.js'

export {default as epiphany0} from './epiphany0.js'
export {default as epiphany1} from './epiphany1.js'
export {default as epiphany2} from './epiphany2.js'
export {default as epiphany3} from './epiphany3.js'
export {default as epiphany4} from './epiphany4.js'
export {default as epiphany5} from './epiphany5.js'
export {default as epiphany6} from './epiphany6.js'
export {default as epiphany7} from './epiphany7.js'
export {default as epiphany8} from './epiphany8.js'
export {default as epiphanyLast} from './epiphanyLast.js'

export {default as lent0} from './lent0.js'
export {default as lent1} from './lent1.js'
export {default as lent2} from './lent2.js'
export {default as lent3} from './lent3.js'
export {default as lent4} from './lent4.js'
export {default as lent5} from './lent5.js'
export {default as lent6} from './lent6.js'

export {default as easter1} from './easter1.js'
export {default as easter2} from './easter2.js'
export {default as easter3} from './easter3.js'
export {default as easter4} from './easter4.js'
export {default as easter5} from './easter5.js'
export {default as easter6} from './easter6.js'
export {default as easter7} from './easter7.js'
export {default as pentecost} from './pentecost.js'
export {default as trinity} from './trinity.js'

export {default as proper1} from './proper1.js'
export {default as proper2} from './proper2.js'
export {default as proper3} from './proper3.js'
export {default as proper4} from './proper4.js'
export {default as proper5} from './proper5.js'
export {default as proper6} from './proper6.js'
export {default as proper7} from './proper7.js'
export {default as proper8} from './proper8.js'
export {default as proper9} from './proper9.js'
export {default as proper10} from './proper10.js'
export {default as proper11} from './proper11.js'
export {default as proper12} from './proper12.js'
export {default as proper13} from './proper13.js'
export {default as proper14} from './proper14.js'
export {default as proper15} from './proper15.js'
export {default as proper16} from './proper16.js'
export {default as proper17} from './proper17.js'
export {default as proper18} from './proper18.js'
export {default as proper19} from './proper19.js'
export {default as proper20} from './proper20.js'
export {default as proper21} from './proper21.js'
export {default as proper22} from './proper22.js'
export {default as proper23} from './proper23.js'
export {default as proper24} from './proper24.js'
export {default as proper25} from './proper25.js'
export {default as proper26} from './proper26.js'
export {default as proper27} from './proper27.js'
export {default as proper28} from './proper28.js'
export {default as proper29} from './proper29.js'

// Holy Days
export {default as holyDays} from './holyDays.js'

// Special Occasions
export {default as specialOccasions} from './specialOccasions.js'
