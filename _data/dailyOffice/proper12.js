/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 12
 * @module _data/dailyOffice/proper12
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 12',
    collects: {
      contemporary: [
        {
          text: 'O God, the protector of all who trust in you, without whom nothing is strong, nothing is holy: Increase and multiply upon us your mercy; that, with you as our ruler and guide, we may so pass through things temporal, that we lose not the things eternal; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 231
        }
      ],
      traditional: [
        {
          text: 'O God, the protector of all that trust in thee, without whom nothing is strong, nothing is holy: Increase and multiply upon us thy mercy, that, thou being our ruler and guide, we may so pass through things temporal, that we finally lose not the things eternal; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 180
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 976
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: '2 Sam 1:17–27',
            second: 'Rom 12:9–21',
            gospel: 'Matt 25:31–46'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: '2 Sam 2:1–11',
            second: 'Acts 15:36–16:5',
            gospel: 'Mark 6:14–29'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: '2 Sam 3:6–21',
            second: 'Acts 16:6–15',
            gospel: 'Mark 6:30–46'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: '2 Sam 3:22–39',
            second: 'Acts 16:16–24',
            gospel: 'Mark 6:47–56'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: '2 Sam 4:1–12',
            second: 'Acts 16:25–40',
            gospel: 'Mark 7:1–23'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: '2 Sam 5:1–12',
            second: 'Acts 17:1–15',
            gospel: 'Mark 7:24–37'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: '2 Sam 5:22–6:11',
            second: 'Acts 17:16–34',
            gospel: 'Mark 8:1–10'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 977
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Josh 24:1–15',
            second: 'Acts 28:23–31',
            gospel: 'Mark 2:23–28'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Josh 24:16–33',
            second: 'Rom 16:1–16',
            gospel: 'Matt 27:24–31'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Judg 2:1–5, 11–23',
            second: 'Rom 16:17–27',
            gospel: 'Matt 27:32–44'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Judg 3:12–30',
            second: 'Acts 1:1–14',
            gospel: 'Matt 27:45–54'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Judg 4:4–23',
            second: 'Acts 1:15–26',
            gospel: 'Matt 27:55–66'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Judg 5:1–18',
            second: 'Acts 2:1–21',
            gospel: 'Matt 28:1–10'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Judg 5:19–31',
            second: 'Acts 2:22–36',
            gospel: 'Matt 28:11–20'
          }
        }
      ]
    }
  ]
}
