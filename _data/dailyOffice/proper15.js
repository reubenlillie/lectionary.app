/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 15
 * @module _data/dailyOffice/proper15
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 15',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, you have given your only Son to be for us a sacrifice for sin, and also an example of godly life: Give us grace to receive thankfully the fruits of his redeeming work, and to follow daily in the blessed steps of his most holy life; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 232
        }
      ],
      traditional: [
        {
          text: 'Almighty God, who hast given thy only Son to be unto us both a sacrifice for sin and also an example of godly life: Give us grace that we may always most thankfully receive that his inestimable benefit, and also daily endeavor ourselves to follow the blessed steps of his most holy life; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 180
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 980
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: '2 Sam 17:1–23',
            second: 'Gal 3:6–14',
            gospel: 'John 5:30–47'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: '2 Sam 17:24–18:8',
            second: 'Acts 22:30–23:11',
            gospel: 'Mark 11:12–26'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: '2 Sam 18:9–18',
            second: 'Acts 23:12–24',
            gospel: 'Mark 11:27–12:12'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: '2 Sam 18:19–33',
            second: 'Acts 23:23–35',
            gospel: 'Mark 12:13–27'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: '2 Sam 19:1–23',
            second: 'Acts 24:1–23',
            gospel: 'Mark 12:28–34'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: '2 Sam 19:24–43',
            second: 'Acts 24:24–25:12',
            gospel: 'Mark 12:35–44'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: '2 Sam 23:1–7, 13–17',
            second: 'Acts 25:13–27',
            gospel: 'Mark 13:1–13'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 981
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Judg 16:15–31',
            second: '2 Cor 13:1–11',
            gospel: 'Mark 5:25–34'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Judg 17:1–13',
            second: 'Acts 7:44–8:1a',
            gospel: 'John 5:19–29'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Judg 18:1–15',
            second: 'Acts 8:1–13',
            gospel: 'John 5:30–47'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Judg 18:16–31',
            second: 'Acts 8:14–25',
            gospel: 'John 6:1–15'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Job 1:1–22',
            second: 'Acts 8:26–40',
            gospel: 'John 6:16–27'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Job 2:1–13',
            second: 'Acts 9:1–9',
            gospel: 'John 6:27–40'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Job 3:1–26',
            second: 'Acts 9:10–19a',
            gospel: 'John 6:41–51'
          }
        }
      ]
    }
  ]
}
