/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Ash Wednesday and Following
 * @module _data/dailyOffice/lent0
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Lent',
    week: 'Ash Wednesday and Following',
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, you hate nothing you have made and forgive the sins of all who are penitent: Create and make in us new and contrite hearts, that we, worthily lamenting our sins and acknowledging our wretchedness, may obtain of you, the God of all mercy, perfect remission and forgiveness; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 217
        }
      ],
      traditional: [
        {
          text: 'Almighty and everlasting God, who hatest nothing that thou hast made and dost forgive the sins of all those who are penitent: Create and make in us new and contrite hearts, that we, worthily lamenting our sins and acknowledging our wretchedness, may obtain of thee, the God of all mercy, perfect remission and forgiveness; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 166
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 950
      },
      days: [
        {
          day: 'Wednesday',
          title: {
            full: 'The First Day of Lent, or Ash Wednesday',
            main: 'Ash Wednesday',
            sub: 'First Day of Lent'
          },
          psalms: {
            morning: [
              '95',
              '32',
              '143'
            ],
            evening: [
              '102',
              '130'
            ]
          },
          lessons: {
            first: 'Jonah 3:1–4:11',
            second: 'Heb 12:1–14',
            gospel: 'Luke 18:9–14'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Deut 7:6–11',
            second: 'Titus 1:1–16',
            gospel: 'John 1:29–34'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Deut 7:12–16',
            second: 'Titus 2:1–15',
            gospel: 'John 1:35–42'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Deut 7:17–26',
            second: 'Titus 3:1–15',
            gospel: 'John 1:43–51'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 951
      },
      days: [
        {
          day: 'Wednesday',
          title: {
            full: 'The First Day of Lent, or Ash Wednesday',
            main: 'Ash Wednesday',
            sub: 'First Day of Lent'
          },
          psalms: {
            morning: [
              '95',
              '32',
              '143'
            ],
            evening: [
              '102',
              '130'
            ]
          },
          lessons: {
            first: 'Amos 5:6–15',
            second: 'Heb 12:1–14',
            gospel: 'Luke 18:9–14'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Hab 3:1–10(11–15)16–18',
            second: 'Phil 3:12–21',
            gospel: 'John 17:1–8'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Ezek 18:1–4, 25–32',
            second: 'Phil 4:1–9',
            gospel: 'John 17:9–19'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Ezek 39:21–29',
            second: 'Phil 4:10–20',
            gospel: 'John 17:20–26'
          }
        }
      ]
    }
  ]
}
