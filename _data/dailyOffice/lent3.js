/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 3 Lent
 * @module _data/dailyOffice/lent3
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Lent',
    week: 'Week of the Third Sunday in Lent',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, you know that we have no power in ourselves to help ourselves: Keep us both outwardly in our bodies and inwardly in our souls, that we may be defended from all adversities which may happen to the body, and from all evil thoughts which may assault and hurt the soul; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 218
        }
      ],
      traditional: [
        {
          text: 'Almighty God, who seest that we have no power of ourselves to help ourselves: Keep us both outwardly in our bodies and inwardly in our souls, that we may be defended from all adversities which may happen to the body, and from all evil thoughts which may assault and hurt the soul; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 167
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 954
      },
      days: [
        {
          day: 'Sunday',
          title: 'Third Sunday in Lent',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Jer 6:9–15',
            second: '1 Cor 6:12–20',
            gospel: 'Mark 5:1–20'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Jer 7:1–15',
            second: 'Rom 4:1–12',
            gospel: 'John 7:14–36'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Jer 7:21–34',
            second: 'Rom 4:13–25',
            gospel: 'John 7:37–52'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Jer 8:18–9:6',
            second: 'Rom 5:1–11',
            gospel: 'John 8:12–20'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '42',
              '43'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: 'Jer 10:11–24',
            second: 'Rom 5:12–21',
            gospel: 'John 8:21–32'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Jer 11:1–8, 14–20',
            second: 'Rom 6:1–11',
            gospel: 'John 8:33–47'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Jer 13:1–11',
            second: 'Rom 6:12–23',
            gospel: 'John 8:47–59'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 955
      },
      days: [
        {
          day: 'Sunday',
          title: 'Third Sunday in Lent',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Gen 44:1–7',
            second: 'Rom 8:1–10',
            gospel: 'John 5:25–29'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Gen 44:18–34',
            second: '1 Cor 7:25–31',
            gospel: 'Mark 5:21–43'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Gen 45:1–15',
            second: '1 Cor 7:32–40',
            gospel: 'Mark 6:1–13'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Gen 45:16–28',
            second: '1 Cor 8:1–13',
            gospel: 'Mark 6:13–29'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '42',
              '43'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: 'Gen 46:1–7, 28–34',
            second: '1 Cor 9:1–15',
            gospel: 'Mark 6:30–46'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Gen 47:1–26',
            second: '1 Cor 9:16–27',
            gospel: 'Mark 6:47–56'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Gen 47:27–48:7',
            second: '1 Cor 10:1–13',
            gospel: 'Mark 7:1–23'
          }
        }
      ]
    }
  ]
}
