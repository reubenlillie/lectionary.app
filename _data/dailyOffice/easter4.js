/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 4 Easter
 * @module _data/dailyOffice/easter4
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Easter',
    week: 'Week of the Fourth Sunday of Easter',
    collects: {
      contemporary: [
        {
          text: 'O God, whose Son Jesus is the good shepherd of your people: Grant that when we hear his voice we may know him who calls us each by name, and follow where he leads; who, with you and the Holy Spirit, lives and reigns, one God, for ever and ever.',
         bcpPage: 225
        }
      ],
      traditional: [
        {
          text: 'O God, whose Son Jesus is the good shepherd of thy people: Grant that when we hear his voice we may know him who calleth us each by name, and follow where he doth lead; who, with thee and the Holy Spirit, liveth and reigneth, one God, for ever and ever.',
          bcpPage: 173
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 960
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fourth Sunday of Easter',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Wis 1:1–15',
            second: '1 Pet 5:1–11',
            gospel: 'Matt 7:15–29'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Wis 1:16–2:11, 21–24',
            second: 'Col 1:1–14',
            gospel: 'Luke 6:1–11'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Wis 3:1–9',
            second: 'Col 1:15–23',
            gospel: 'Luke 6:12–26'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Wis 4:16–5:8',
            second: 'Col 1:24–2:7',
            gospel: 'Luke 6:27–38'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Wis 5:9–23',
            second: 'Col 2:8–23',
            gospel: 'Luke 6:39–49'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Wis 6:12–23',
            second: 'Col 3:1–11',
            gospel: 'Luke 7:1–17'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Wis 7:1–14',
            second: 'Col 3:12–17',
            gospel: 'Luke 7:18–28(29–30)31–35'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 961
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fourth Sunday of Easter',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Exod 28:1–4, 30–38',
            second: '1 John 2:18–29',
            gospel: 'Mark 6:30–44'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Exod 32:1–20',
            second: 'Col 3:18–4:6(7–18)',
            gospel: 'Matt 5:1–10'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Exod 32:21–34',
            second: '1 Thess 1:1–10',
            gospel: 'Matt 5:11–16'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Exod 33:1–23',
            second: '1 Thess 2:1–12',
            gospel: 'Matt 5:17–20'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Exod 34:1–17',
            second: '1 Thess 2:13–20',
            gospel: 'Matt 5:21–26'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Exod 34:18–35',
            second: '1 Thess 3:1–13',
            gospel: 'Matt 5:27–37'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Exod 40:18–38',
            second: '1 Thess 4:1–12',
            gospel: 'Matt 5:38–48'
          }
        }
      ]
    }
  ]
}
