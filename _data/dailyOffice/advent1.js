/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 1 Advent
 * @module _data/dailyOffice/advent1
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Advent',
    week: 'Week of the First Sunday of Advent',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, give us grace to cast away the works of darkness, and put on the armor of light, now in the time of this mortal life in which your Son Jesus Christ came to visit us in great humility; that in the last day, when he shall come again in his glorious majesty to judge both the living and the dead, we may rise to the life immortal; through him who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 211
        }
      ],
      traditional: [
        {
          text: 'Almighty God, give us grace that we may cast away the works of darkness, and put upon us the armor of light, now in the time of this mortal life in which thy Son Jesus Christ came to visit us in great humility; that in the last day, when he shall come again in his glorious majesty to judge both the quick and the dead, we may rise to the life immortal; through him who liveth and reigneth with thee and the Holy Ghost, one God, now and for ever.',
          bcpPage: 159
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 936
      },
      days: [
        {
          day: 'Sunday',
          title: 'First Sunday of Advent',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Isa 1:1–9',
            second: '2 Pet 3:1–10',
            gospel: 'Matt 25:1–13'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Isa 1:10–20',
            second: '1 Thess 1:1–10',
            gospel: 'Luke 20:1–8'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Isa 1:21–31',
            second: '1 Thess 2:1–12',
            gospel: 'Luke 20:9–18'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Isa 2:1–11',
            second: '1 Thess 2:13–20',
            gospel: 'Luke 20:19–26'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Isa 2:12–22',
            second: '1 Thess 3:1–13',
            gospel: 'Luke 20:27–40'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Isa 3:8–15',
            second: '1 Thess 4:1–12',
            gospel: 'Luke 20:41–21:4'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Isa 4:2–6',
            second: '1 Thess 4:13–18',
            gospel: 'Luke 21:5–19'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 937
      },
      days: [
        {
          day: 'Sunday',
          title: 'First Sunday of Advent',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Amos 1:1-5, 13–2:8',
            second: '1 Thess 5:1-11',
            gospel: 'Luke 21:5–19'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Amos 2:6–16 ',
            second: '2 Pet 1:1–11',
            gospel: 'Matt 21:1–11'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Amos 3:1–11',
            second: '2 Pet 1:12-21',
            gospel: 'Matt 21:12-22'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Amos 3:12–4:5',
            second: '2 Pet 3:1-10',
            gospel: 'Matt 21:23-32'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Amos 4:6-13',
            second: '2 Pet 3:11-18',
            gospel: 'Matt 21:33-46'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Amos 5:1-17',
            second: 'Jude 1-16',
            gospel: 'Matt 22:1-14'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Amos 5:18-27',
            second: 'Jude 17-25',
            gospel: 'Matt 22:15-22'
          }
        }
      ]
    }
  ]
}
