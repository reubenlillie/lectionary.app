/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 18
 * @module _data/dailyOffice/proper18
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 18',
    collects: {
      contemporary: [
        {
          text: 'Grant us, O Lord, to trust in you with all our hearts; for, as you always resist the proud who confide in their own strength, so you never forsake those who make their boast of your mercy; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 233
        }
      ],
      traditional: [
        {
          text: 'Grant us, O Lord, we pray thee, to trust in thee with all our heart; seeing that, as thou dost alway resist the proud who confide in their own strength, so thou dost not forsake those who make their boast of thy mercy; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 181
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 982
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: '1 Kgs 12:21–33',
            second: 'Acts 4:18–31',
            gospel: 'John 10:31–42'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: '1 Kgs 13:1–10',
            second: 'Phil 1:1–11',
            gospel: 'Mark 15:40–47'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: '1 Kgs 16:23–34',
            second: 'Phil 1:23–30',
            gospel: 'Mark 16:1–8(9–20)'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: '1 Kgs 17:1–24',
            second: 'Phil 2:1–11',
            gospel: 'Matt 2:1–12'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '93',
              '96'
            ]
          },
          lessons: {
            first: '1 Kgs 18:1–19',
            second: 'Phil 2:12–30',
            gospel: 'Matt 2:13–23'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: '1 Kgs 18:20–40',
            second: 'Phil 3:1–16',
            gospel: 'Matt 3:1–12'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: '1 Kgs 18:41–19:8',
            second: 'Phil 3:17–4:7',
            gospel: 'Matt 3:13–17'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 983
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Job 25:1–6; 27:1–6',
            second: 'Rev 14:1–7, 13',
            gospel: 'Matt 5:13–20'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Job 32:1–10, 19–33:1, 19–28',
            second: 'Acts 13:44–52',
            gospel: 'John 10:19–30'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Job 29:1–20',
            second: 'Acts 14:1–18',
            gospel: 'John 10:31–42'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Job 29:1; 30:1–2, 16–31',
            second: 'Acts 14:19–28',
            gospel: 'John 11:1–16'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '93',
              '96'
            ]
          },
          lessons: {
            first: 'Job 29:1; 31:1–23',
            second: 'Acts 15:1–11',
            gospel: 'John 11:17–29'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Job 29:1; 31:24–40',
            second: 'Acts 15:12–21',
            gospel: 'John 11:30–44'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Job 38:1–17',
            second: 'Acts 15:22–35',
            gospel: 'John 11:45–54'
          }
        },
      ]
    }
  ]
}
