/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 28
 * @module _data/dailyOffice/proper28
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 28',
    collects: {
      contemporary: [
        {
          text: 'Blessed Lord, who caused all holy Scriptures to be written for our learning: Grant us so to hear them, read, mark, learn, and inwardly digest them, that we may embrace and ever hold fast the blessed hope of everlasting life, which you have given us in our Savior Jesus Christ; who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 236
        }
      ],
      traditional: [
        {
          text: 'Blessed Lord, who hast caused all holy Scriptures to be written for our learning: Grant that we may in such wise hear them, read, mark, learn, and inwardly digest them; that, by patience and comfort of thy holy Word, we may embrace and ever hold fast the blessed hope of everlasting life, which thou hast given us in our Savior Jesus Christ; who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 184
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 992
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: '1 Macc 2:29–43, 49–50',
            second: 'Acts 28:14b–23',
            gospel: 'Luke 16:1–13'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: '1 Macc 3:1–24',
            second: 'Rev 20:7–15',
            gospel: 'Matt 17:1–13'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: '1 Macc 3:25–41',
            second: 'Rev 21:1–8',
            gospel: 'Matt 17:14–21'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: '1 Macc 3:42–60',
            second: 'Rev 21:9–21',
            gospel: 'Matt 17:22–27'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: '1 Macc 4:1–25',
            second: 'Rev 21:22–22:5',
            gospel: 'Matt 18:1–9'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: '1 Macc 4:36–59',
            second: 'Rev 22:6–13',
            gospel: 'Matt 18:10–20'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Isa 65:17–25',
            second: 'Rev 22:14–21',
            gospel: 'Matt 18:21–35'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 993
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Hab 1:1–4(5–11)12–2:1',
            second: 'Phil 3:13–4:1',
            gospel: 'Matt 23:13–24'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Hab 2:1–4, 9–20',
            second: 'Jas 2:14–26',
            gospel: 'Luke 16:19–31'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Hab 3:1–10(11–15)16–18',
            second: 'Jas 3:1–12',
            gospel: 'Luke 17:1–10'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: 'Mal 1:1, 6–14',
            second: 'Jas 3:13–4:12',
            gospel: 'Luke 17:11–19'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Mal 2:1–16',
            second: 'Jas 4:13–5:6',
            gospel: 'Luke 17:20–37'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Mal 3:1–12',
            second: 'Jas 5:7–12',
            gospel: 'Luke 18:1–8'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Mal 3:13–4:6',
            second: 'Jas 5:13–20',
            gospel: 'Luke 18:9–14'
          }
        }
      ]
    }
  ]
}
