/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, 
 * Year One, Eve of Trinity Sunday and Trinity Sunday
 * @module _data/dailyOffice/oneTrinity
 * @since 0.1.0
 * @since 0.11.0 Add `color` properties
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, you have given to us your servants grace, by the confession of a true faith, to acknowledge the glory of the eternal Trinity, and in the power of your divine Majesty to worship the Unity: Keep us steadfast in this faith and worship, and bring us at last to see you in your one and eternal glory, O Father; who with the Son and the Holy Spirit live and reign, one God, for ever and ever.',
          bcpPage: 228
        }
      ],
      traditional: [
        {
          text: 'Almighty and everlasting God, who hast given unto us thy servants grace, by the confession of a true faith, to acknowledge the glory of the eternal Trinity, and in the power of the Divine Majesty to worship the Unity: We beseech thee that thou wouldest keep us steadfast in this faith and worship, and bring us at last to see thee in thy one and eternal glory, O Father; who with the Son and the Holy Spirit livest and reignest, one God, for ever and ever.',
          bcpPage: 176
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 964
      },
      days: [
        {
          day: 'Saturday',
          title: 'Eve of Trinity',
          psalms: {
            evening: [
              '104'
            ]
          },
          lessons: {
            evening: {
              first: 'Sir 42:15–25',
              second: 'Eph 3:14–21'
            }
          }
        },
        {
          day: 'Sunday',
          title: {
            full: 'First Sunday after Pentecost: Trinity Sunday',
            main: 'Trinity Sunday',
            sub: 'First Sunday after Pentecost'
          },
          color: 'white',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Sir 43:1–12(27–33)',
            second: 'Eph 4:1–16',
            gospel: 'John 1:1–18'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 965
      },
      days: [
        {
          day: 'Saturday',
          title: 'Eve of Trinity',
          psalms: {
            evening: [
              '104'
            ]
          },
          lessons: {
            evening: {
              first: 'Sir 42:15–25',
              second: 'Eph 3:14–21'
            }
          }
        },
        {
          day: 'Sunday',
          title: {
            full: 'First Sunday after Pentecost: Trinity Sunday',
            main: 'Trinity Sunday',
            sub: 'First Sunday after Pentecost'
          },
          color: 'white',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Job 38:1–11; 42:1–5',
            second: 'Rev 19:4–16',
            gospel: 'John 1:29–34'
          }
        }
      ]
    }
  ]
}
