/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 6
 * @module _data/dailyOffice/proper6
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 6',
    collects: {
      contemporary: [
        {
          text: 'Keep, O Lord, your household the Church in your steadfast faith and love, that through your grace we may proclaim your truth with boldness, and minister your justice with compassion; for the sake of our Savior Jesus Christ, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 230
        }
      ],
      traditional: [
        {
          text: 'Keep, O Lord, we beseech thee, thy household the Church in thy steadfast faith and love, that by the help of thy grace we may proclaim thy truth with boldness, and minister thy justice with compassion; for the sake of our Savior Jesus Christ, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 178
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 970
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Sir 46:11–20',
            second: 'Rev 15:1–8',
            gospel: 'Matt 18:1–14'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: '1 Sam 1:1–20',
            second: 'Acts 1:1–14',
            gospel: 'Luke 20:9–19'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: '1 Sam 1:21–2:11',
            second: 'Acts 1:15–26',
            gospel: 'Luke 20:19–26'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: '1 Sam 2:12–26',
            second: 'Acts 2:1–21',
            gospel: 'Luke 20:27–40'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '34'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: '1 Sam 2:27–36',
            second: 'Acts 2:22–36',
            gospel: 'Luke 20:41–21:4'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: '1 Sam 3:1–21',
            second: 'Acts 2:37–47',
            gospel: 'Luke 21:5–19'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: '1 Sam 4:1b–11',
            second: 'Acts 4:32–5:11',
            gospel: 'Luke 21:20–28'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 971
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Num 6:22–27',
            second: 'Acts 13:1–12',
            gospel: 'Luke 12:41–48'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Num 9:15–23; 10:29–36',
            second: 'Rom 1:1–15',
            gospel: 'Matt 17:14–21'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Num 11:1–23',
            second: 'Rom 1:16–25',
            gospel: 'Matt 17:22–27'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Num 11:24–33(34–35)',
            second: 'Rom 1:28–2:11',
            gospel: 'Matt 18:1–9'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '34'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: 'Num 12:1–16',
            second: 'Rom 2:12–24',
            gospel: 'Matt 18:10–20'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Num 13:1–3, 21–30',
            second: 'Rom 2:25–3:8',
            gospel: 'Matt 18:21–35'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Num 13:31–14:25',
            second: 'Rom 3:9–20',
            gospel: 'Matt 19:1–12'
          }
        }
      ]
    }
  ]
}
