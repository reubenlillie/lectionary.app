/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 25
 * @module _data/dailyOffice/proper25
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 25',
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, increase in us the gifts of faith, hope, and charity; and, that we may obtain what you promise, make us love what you command; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 235
        }
      ],
      traditional: [
        {
          text: 'Almighty and everlasting God, give unto us the increase of faith, hope, and charity; and, that we may obtain that which thou dost promise, make us to love that which thou dost command; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 183
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 990
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Hag 1:1–2:9',
            second: 'Acts 18:24–19:7',
            gospel: 'Luke 10:25–37'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Zech 1:7–17',
            second: 'Rev 1:4–20',
            gospel: 'Matt 12:43–50'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Ezra 5:1–17',
            second: 'Rev 4:1–11',
            gospel: 'Matt 13:1–9'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Ezra 6:1–22',
            second: 'Rev 5:1–10',
            gospel: 'Matt 13:10–17'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '103'
            ]
          },
          lessons: {
            first: 'Neh 1:1–11',
            second: 'Rev 5:11–6:11',
            gospel: 'Matt 13:18–23'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Neh 2:1–20',
            second: 'Rev 6:12–7:4',
            gospel: 'Matt 13:24–30'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Neh 4:1–23',
            second: 'Rev 7:(4–8)9–17',
            gospel: 'Matt 13:31–55'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 991
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Sir 18:19–33',
            second: '1 Cor 10:15–24',
            gospel: 'Matt 18:15–20'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Sir 19:4–17',
            second: 'Rev 11:1–14',
            gospel: 'Luke 11:14–26'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Sir 24:1–12',
            second: 'Rev 11:14–19',
            gospel: 'Luke 11:27–36'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Sir 28:14–26',
            second: 'Rev 12:1–6',
            gospel: 'Luke 11:37–52'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '103'
            ]
          },
          lessons: {
            first: 'Sir 31:12–18, 25–32:2',
            second: 'Rev 12:7–17',
            gospel: 'Luke 11:53–12:12'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Sir 34:1–8, 18–22',
            second: 'Rev 13:1–10',
            gospel: 'Luke 12:13–31'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Sir 35:1–17',
            second: 'Rev 13:11–18',
            gospel: 'Luke 12:32–48'
          }
        }
      ]
    }
  ]
}
