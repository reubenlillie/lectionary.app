/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 3 Epiphany
 * @module _data/dailyOffice/epiphany3
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the Third Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'Give us grace, O Lord, to answer readily the call of our Savior Jesus Christ and proclaim to all people the Good News of his salvation, that we and the whole world may perceive the glory of his marvelous works; who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 215
        }
      ],
      traditional: [
        {
          text: 'Give us grace, O Lord, to answer readily the call of our Savior Jesus Christ and proclaim to all people the Good News of his salvation, that we and all the whole world may perceive the glory of his marvelous works; who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 163
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 944
      },
      days: [
        {
          day: 'Sunday',
          title: 'Third Sunday after the Epiphany',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Isa 47:1–15',
            second: 'Heb 10:19–31',
            gospel: 'John 5:2–18'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Isa 48:1–11',
            second: 'Gal 1:1–17',
            gospel: 'Mark 5:21–43'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Isa 48:12–21',
            second: 'Gal 1:18–2:10',
            gospel: 'Mark 6:1–13'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Isa 49:1–12',
            second: 'Gal 2:11–21',
            gospel: 'Mark 6:13–29'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '118'
            ]
          },
          lessons: {
            first: 'Isa 49:13–23',
            second: 'Gal 3:1–14',
            gospel: 'Mark 6:30–46'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Isa 50:1–11',
            second: 'Gal 3:15–22',
            gospel: 'Mark 6:47–56'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Isa 51:1–8',
            second: 'Gal 3:23–29',
            gospel: 'Mark 7:1–23'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 945
      },
      days: [
        {
          day: 'Sunday',
          title: 'Third Sunday after the Epiphany',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Gen 13:2–18',
            second: 'Gal 2:1–10',
            gospel: 'Mark 7:31–37'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Gen 14:(1–7)8–24',
            second: 'Heb 8:1–13',
            gospel: 'John 4:43–54'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Gen 15:1–11, 17–21',
            second: 'Heb 9:1–14',
            gospel: 'John 5:1–18'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Gen 16:1–14',
            second: 'Heb 9:15–28',
            gospel: 'John 5:19–29'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '118'
            ]
          },
          lessons: {
            first: 'Gen 16:15–17:14',
            second: 'Heb 10:1–10',
            gospel: 'John 5:30–47'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Gen 17:15–27',
            second: 'Heb 10:11–25',
            gospel: 'John 6:1–15'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Gen 18:1–16',
            second: 'Heb 10:26–39',
            gospel: 'John 6:16–27'
          }
        }
      ]
    }
  ]
}
