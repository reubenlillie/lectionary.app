/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 1 Epiphany
 * @module _data/dailyOffice/epiphany1
 * @since 0.1.0
 * @since 0.11.0 Add `color` properties
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the First Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'Father in heaven, who at the baptism of Jesus in the River Jordan proclaimed him your beloved Son and anointed him with the Holy Spirit: Grant that all who are baptized into his Name may keep the covenant they have made, and boldly confess him as Lord and Savior; who with you and the Holy Spirit lives and reigns, one God, in glory everlasting.',
          bcpPage: 214
        }
      ],
      traditional: [
        {
          text: 'Father in heaven, who at the baptism of Jesus in the River Jordan didst proclaim him thy beloved Son and anoint him with the Holy Spirit: Grant that all who are baptized into his Name may keep the covenant they have made, and boldly confess him as Lord and Savior; who with thee and the same Spirit liveth and reigneth, one God, in glory everlasting.',
          bcpPage: 163
        }
      ]
    }
  },
  years: [
    {

      props: {
        year: 'Year One',
        bcpPage: 942
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'First Sunday after the Epiphany: The Baptism of Our Lord Jesus Christ',
            main: 'Baptism of Christ',
            sub: 'First Sunday after the Epiphany'
          },
          color: 'white',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Isa 40:1–11',
            second: 'Heb 1:1–12',
            gospel: 'John 1:1–7, 19–20, 29–34'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Isa 40:12–23',
            second: 'Eph 1:1–14',
            gospel: 'Mark 1:1–13'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Isa 40:25–31',
            second: 'Eph 1:15–23',
            gospel: 'Mark 1:14–28'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Isa 41:1–16',
            second: 'Eph 2:1–10',
            gospel: 'Mark 1:29–45'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Isa 41:17–29',
            second: 'Eph 2:11–22',
            gospel: 'Mark 2:1–12'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Isa 42:(1–9)10–17',
            second: 'Eph 3:1–13',
            gospel: 'Mark 2:13–22'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Isa 43:1–13',
            second: 'Eph 3:14–21',
            gospel: 'Mark 2:23–3:6'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 943
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'First Sunday after the Epiphany: The Baptism of Our Lord Jesus Christ',
            main: 'Baptism of Christ',
            sub: 'First Sunday after the Epiphany'
          },
          color: 'white',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Gen 1:1–2:3',
            second: 'Eph 1:3–14',
            gospel: 'John 1:29–34'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Gen 2:4–9(10–15)16–25',
            second: 'Heb 1:1.14',
            gospel: 'John 1:1–18'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Gen 3:1–24',
            second: 'Heb 2:1–10',
            gospel: 'John 1:19–28'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Gen 4:1–16',
            second: 'Heb 2:11.18',
            gospel: 'John 1:(29–34)35–42'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Gen 4:17–26',
            second: 'Heb 3:1–11',
            gospel: 'John 1:43–51'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Gen 6:1–8',
            second: 'Heb 3:12–19',
            gospel: 'John 2:1–12'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Gen 6:9–22',
            second: 'Heb 4:1–13',
            gospel: 'John 2:13–22'
          }
        }
      ]
    }
  ]
}
