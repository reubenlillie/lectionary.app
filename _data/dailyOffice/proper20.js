/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 20
 * @module _data/dailyOffice/proper20
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 20',
    collects: {
      contemporary: [
        {
          text: 'Grant us, Lord, not to be anxious about earthly things, but to love things heavenly; and even now, while we are placed among things that are passing away, to hold fast to those that shall endure; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 234
        }
      ],
      traditional: [
        {
          text: 'Grant us, O Lord, not to mind earthly things, but to love things heavenly; and even now, while we are placed among things that are passing away, to cleave to those that shall abide; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 182
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 984
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: '2 Kgs 4:8–37',
            second: 'Acts 9:10–31',
            gospel: 'Luke 3:7–18'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: '2 Kgs 5:1–19',
            second: '1 Cor 4:8–21',
            gospel: 'Matt 5:21–26'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: '2 Kgs 5:19–27',
            second: '1 Cor 5:1–8',
            gospel: 'Matt 5:23–37'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: '2 Kgs 6:1–23',
            second: '1 Cor 5:9–6:8',
            gospel: 'Matt 5:38–48'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '116',
              '117'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: '2 Kgs 9:1–16',
            second: '1 Cor 6:12–20',
            gospel: 'Matt 6:1–6, 16–18'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: '2 Kgs 9:17–37',
            second: '1 Cor 7:1–9',
            gospel: 'Matt 6:7–15'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: '2 Kgs 11:1–20a',
            second: '1 Cor 7:10–24',
            gospel: 'Matt 6:19–24'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 985
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Esth 3:1–4:3',
            altFirst: 'Jdt 5:22–6:4, 10–21',
            second: 'Jas 1:19–27',
            gospel: 'Matt 6:1–6, 16–18'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Esth 4:4–17',
            altFirst: 'Jdt 7:1–7, 19–32',
            second: 'Acts 18:1–11',
            gospel: 'Luke (1:1–4); 3:1–14'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Esth 5:1–14',
            altFirst: 'Jdt 8:9–17; 9:1, 7–10',
            second: 'Acts 18:12–28',
            gospel: 'Luke 3:15–22'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Esth 6:1–14',
            altFirst: 'Jdt 10:1–23',
            second: 'Acts 19:1–10',
            gospel: 'Luke 4:1–13'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '116',
              '117'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: 'Esth 7:1–10',
            altFirst: 'Jdt 12:1–20',
            second: 'Acts 19:11–20',
            gospel: 'Luke 4:14–30'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Esth 8:1–8, 15–17',
            altFirst: 'Jdt 13:1–20',
            second: 'Acts 19:21–41',
            gospel: 'Luke 4:31–37'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Hos 1:1–2:1',
            second: 'Acts 20:1–16',
            gospel: 'Luke 4:38–44'
          }
        }
      ]
    }
  ]
}
