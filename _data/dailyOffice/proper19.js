/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 19
 * @module _data/dailyOffice/proper19
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 19',
    collects: {
      contemporary: [
        {
          text: 'O God, because without you we are not able to please you, mercifully grant that your Holy Spirit may in all things direct and rule our hearts; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 233
        }
      ],
      traditional: [
        {
          text: 'O God, forasmuch as without thee we are not able to please thee, mercifully grant that thy Holy Spirit may in all things direct and rule our hearts; through Jesus Christ our Lord, who with thee and the same Spirit liveth and reigneth, one God, now and for ever.',
          bcpPage: 182
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 984
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: '1 Kgs 19:8–21',
            second: 'Acts 5:34–42',
            gospel: 'John 11:45–57'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: '1 Kgs 21:1–16',
            second: '1 Cor 1:1–19',
            gospel: 'Matt 4:1–11'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)34–36'
            ]
          },
          lessons: {
            first: '1 Kgs 21:17–29',
            second: '1 Cor 1:20–31',
            gospel: 'Matt 4:12–17'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: '1 Kgs 22:1–28',
            second: '1 Cor 2:1–13',
            gospel: 'Matt 4:18–25'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: '1 Kgs 22:29–45',
            second: '1 Cor 2:14–3:15',
            gospel: 'Matt 5:1–10'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: '2 Kgs 1:2–17',
            second: '1 Cor 3:16–23',
            gospel: 'Matt 5:11–16'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: '2 Kgs 2:1–18',
            second: '1 Cor 4:1–7',
            gospel: 'Matt 5:17–20'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 985
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Job 38:1, 18–41',
            second: 'Rev 18:1–8',
            gospel: 'Matt 5:21–26'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Job 40:1–24',
            second: 'Acts 15:36–16:5',
            gospel: 'John 11:55–12:8'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)34–36'
            ]
          },
          lessons: {
            first: 'Job 40:1; 41:1–11',
            second: 'Acts 16:6–15',
            gospel: 'John 12:9–19'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Job 42:1–17',
            second: 'Acts 16:16–24',
            gospel: 'John 12:20–26'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Job 28:1–28',
            second: 'Acts 16:25–40',
            gospel: 'John 12:27–36a'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Esth 1:1–4, 10–19',
            altFirst: 'Jdt 4:1–15',
            second: 'Acts 19:11–20',
            gospel: 'Luke 4:14–30'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Esth 2:5–8, 15:–23',
            altFirst: 'Jdt 5:1–21',
            second: 'Acts 17:16–34',
            gospel: 'John 12:44–50'
          }
        }
      ]
    }
  ]
}
