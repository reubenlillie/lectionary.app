/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 3 Easter
 * @module _data/dailyOffice/easter3
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Easter',
    week: 'Week of the Third Sunday of Easter',
    collects: {
      contemporary: [
        {
          text: 'O God, whose blessed Son made himself known to his disciples in the breaking of bread: Open the eyes of our faith, that we may behold him in all his redeeming work; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
         bcpPage: 224
        }
      ],
      traditional: [
        {
          text: 'O God, whose blessed Son did manifest himself to his disciples in the breaking of bread: Open, we pray thee, the eyes of our faith, that we may behold him in all his redeeming work; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 173
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 960
      },
      days: [
        {
          day: 'Sunday',
          title: 'Third Sunday of Easter',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Dan 4:1–18',
            second: '1 Pet 4:7–11',
            gospel: 'John 21:15–25'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Dan 4:19–27',
            second: '1 John 3:19–4:6',
            gospel: 'Luke 4:14–30'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Dan 4:28–37',
            second: '1 John 4:7–21',
            gospel: 'Luke 4:31–37'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Dan 5:1–12',
            second: '1 John 5:1–12',
            gospel: 'Luke 4:38–44'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Dan 5:13–30',
            second: '1 John 5:13–20(21)',
            gospel: 'Luke 5:1–11'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Dan 6:1–15',
            second: '2 John 1–13',
            gospel: 'Luke 5:12–26'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Dan 6:16–28',
            second: '3 John 1–15',
            gospel: 'Luke 5:27–39'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 961
      },
      days: [
        {
          day: 'Sunday',
          title: 'Third Sunday of Easter',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Exod 18:1–12',
            second: '1 John 2:7–17',
            gospel: 'Mark 16:9–20'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Exod 18:13–27',
            second: '1 Pet 5:1–14',
            gospel: 'Matt (1:1–17); 3:1–6'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Exod 19:1–16',
            second: 'Col 1:1–14',
            gospel: 'Matt 3:7–12'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Exod 19:16–25',
            second: 'Col 1:15–23',
            gospel: 'Matt 3:13–17'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Exod 20:1–21',
            second: 'Col 1:24–2:7',
            gospel: 'Matt 4:1–11'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Exod 24:1–18',
            second: 'Col 2:8–23',
            gospel: 'Matt 4:12–17'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Exod 25:1–22',
            second: 'Col 3:1–17',
            gospel: 'Matt 4:18–25'
          }
        }
      ]
    }
  ]
}
