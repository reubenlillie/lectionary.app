/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 26
 * @module _data/dailyOffice/proper26
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 26',
    collects: {
      contemporary: [
        {
          text: 'Almighty and merciful God, it is only by your gift that your faithful people offer you true and laudable service: Grant that we may run without stumbling to obtain your heavenly promises; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 235
        }
      ],
      traditional: [
        {
          text: 'Almighty and merciful God, of whose only gift it cometh that thy faithful people do unto thee true and laudable service: Grant, we beseech thee, that we may run without stumbling to obtain thy heavenly promises; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 184
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 990
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Neh 5:1–19',
            second: 'Acts 20:7–12',
            gospel: 'Luke 12:22–31'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Neh 6:1–19',
            second: 'Rev 10:1–11',
            gospel: 'Matt 13:36–43'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Neh 12:27–31a, 42b–47',
            second: 'Rev 11:1–19',
            gospel: 'Matt 13:44–52'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Neh 13:4–22',
            second: 'Rev 12:1–12',
            gospel: 'Matt 13:53–58'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Ezra 7:(1–10)11–26',
            second: 'Rev 14:1–13',
            gospel: 'Matt 14:1–12'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Ezra 7:27–28; 8:21–36',
            second: 'Rev 15:1–8',
            gospel: 'Matt 14:13–21'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Ezra 9:1–15',
            second: 'Rev 17:1–14',
            gospel: 'Matt 14:22–36'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 991
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Sir 36:1–17',
            second: '1 Cor 12:27–13:13',
            gospel: 'Matt 18:21–35'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Sir 38:24–34',
            second: 'Rev 14:1–13',
            gospel: 'Luke 12:49–59'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Sir 43:1–22',
            second: 'Rev 14:14–15:8',
            gospel: 'Luke 13:1–9'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Sir 43:23–33',
            second: 'Rev 16:1–11',
            gospel: 'Luke 13:10–17'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Sir 44:1–15',
            second: 'Rev 16:12–21',
            gospel: 'Luke 13:18–30'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Sir 50:1, 11–24',
            second: 'Rev 17:1–18',
            gospel: 'Luke 13:31–35'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Sir 51:1–12',
            second: 'Rev 18:1–14',
            gospel: 'Luke 14:1–11'
          }
        }
      ]
    }
  ]
}
