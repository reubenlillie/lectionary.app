/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 29
 * @module _data/dailyOffice/proper29
 * @since 0.1.0
 * @since 0.11.0 Add `title` and `color` properties for Reign of Christ
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 29',
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, whose will it is to restore all things in your well-beloved Son, the King of kings and Lord of lords: Mercifully grant that the peoples of the earth, divided and enslaved by sin, may be freed and brought together under his most gracious rule; who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 236
        }
      ],
      traditional: [
        {
          text: 'Almighty and everlasting God, whose will it is to restore all things in thy well-beloved Son, the King of kings and Lord of lords: Mercifully grant that the peoples of the earth, divided and enslaved by sin, may be freed and brought together under his most gracious rule; who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 185
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 994
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'Last Sunday after Pentecost: The Reign of Our Lord Jesus Christ',
            main: 'Reign of Christ',
            sub: 'Last Sunday after Pentecost'
          },
          color: 'white',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Isa 19:19–25',
            second: 'Rom 15:5–13',
            gospel: 'Luke 19:11–27'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Joel 3:1–2, 9–17',
            second: '1 Pet 1:1–12',
            gospel: 'Matt 19:1–12'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Nah 1:1–13',
            second: '1 Pet 1:13–25',
            gospel: 'Matt 19:13–22'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Obad 15–21',
            second: '1 Pet 2:1–10',
            gospel: 'Matt 19:23–30'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Zeph 3:1–13',
            second: '1 Pet 2:11–25',
            gospel: 'Matt 20:1–16'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '142:1–11(12)'
            ]
          },
          lessons: {
            first: 'Isa 24:14–23',
            second: '1 Pet 3:13–4:6',
            gospel: 'Matt 20:17–28'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Mic 7:11–20',
            second: '1 Pet 4:7–19',
            gospel: 'Matt 20:29–34'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 995
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'Last Sunday after Pentecost: The Reign of Our Lord Jesus Christ',
            main: 'Reign of Christ',
            sub: 'Last Sunday after Pentecost'
          },
          color: 'white',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Zech 9:9–16',
            second: '1 Pet 3:13–22',
            gospel: 'Matt 21:1–13'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Zech 10:1–12',
            second: 'Gal 6:1–10',
            gospel: 'Luke 18:15–30'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Zech 11:4–17',
            second: '1 Cor 3:10–23',
            gospel: 'Luke 18:31–43'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Zech 12:1–10',
            second: 'Eph 1:3–14',
            gospel: 'Luke 19:1–10'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Zech 13:1–9',
            second: 'Eph 1:15–23',
            gospel: 'Luke 19:11–27'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '142:1–11(12)'
            ]
          },
          lessons: {
            first: 'Zech 14:1–11',
            second: 'Rom 15:7–13',
            gospel: 'Luke 19:28–40'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Zech 14:12–21',
            second: 'Phil 2:1–11',
            gospel: 'Luke 19:41–48'
          }
        }
      ]
    }
  ]
}
