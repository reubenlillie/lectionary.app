/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 3 Advent
 * @module _data/dailyOffice/advent3
 * @since 0.1.0
 * @since 0.11.0 Add `color` properties
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Advent',
    week: 'Week of the Third Sunday of Advent',
    collects: {
      contemporary: [
        {
          text: 'Stir up your power, O Lord, and with great might come among us; and, because we are sorely hindered by our sins, let your bountiful grace and mercy speedily help and deliver us; through Jesus Christ our Lord, to whom, with you and the Holy Spirit, be honor and glory, now and for ever.',
         bcpPage: 212
        }
      ],
      traditional: [
        {
          text: 'Stir up thy power, O Lord, and with great might come among us; and, because we are sorely hindered by our sins, let thy bountiful grace and mercy speedily help and deliver us; through Jesus Christ our Lord, to whom, with thee and the Holy Ghost, be honor and glory, world without end.',
          bcpPage: 160
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 938
      },
      days: [
        {
          day: 'Sunday',
          title: 'Third Sunday of Advent',
          color: 'pink',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Isa 13:6–13',
            second: 'Heb 12:18–29',
            gospel: 'John 3:22–30'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Isa 8:16–9:1',
            second: '2 Pet 1:1–11',
            gospel: 'Luke 22:39–53'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Isa 9:1–7',
            second: '2 Pet 1:12–21',
            gospel: 'Luke 22:54–69'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Isa 9:8–17',
            second: '2 Pet 2:1–10a',
            gospel: 'Mark 1:1–8'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '33'
            ]
          },
          lessons: {
            first: 'Isa 9:18–10:4',
            second: '2 Pet 2:10b–16',
            gospel: 'Matt 3:1–12'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Isa 10:5–19',
            second: '2 Pet 2:17–22',
            gospel: 'Matt 11:2–15'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Isa 10:20–27',
            second: 'Jude 17–25',
            gospel: 'Luke 3:19'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 939
      },
      days: [
        {
          day: 'Sunday',
          title: 'Third Sunday of Advent',
          color: 'pink',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Amos 9:11–15',
            second: '2 Thess 2:1–3, 13–17',
            gospel: 'John 5:30–47'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Zech 1:7–17',
            second: 'Rev 3:7–13',
            gospel: 'Matt 24:15–31'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Zech 2:1–13',
            second: 'Rev 3:14–22',
            gospel: 'Matt 24:32–44'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Zech 3:1–10',
            second: 'Rev 4:1–8',
            gospel: 'Matt 24:45–51'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '33'
            ]
          },
          lessons: {
            first: 'Zech 4:1–14',
            second: 'Rev 4:9–5:5',
            gospel: 'Matt 25:1–13'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Zech 7:8–8:8',
            second: 'Rev 5:6–14',
            gospel: 'Matt 25:14–30'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Zech 8:9–17',
            second: 'Rev 6:1–17',
            gospel: 'Matt 25:31–46'
          }
        }
      ]
    }
  ]
}
