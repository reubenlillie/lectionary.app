/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 1 Lent
 * @module _data/dailyOffice/lent1
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Lent',
    week: 'Week of the First Sunday in Lent',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, whose blessed Son was led by the Spirit to be tempted by Satan: Come quickly to help us who are assaulted by many temptations; and, as you know the weaknesses of each of us, let each one find you mighty to save; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 218
        }
      ],
      traditional: [
        {
          text: 'Almighty God, whose blessed Son was led by the Spirit to be tempted of Satan: Make speed to help thy servants who are assaulted by manifold temptations; and, as thou knowest their several infirmities, let each one find thee mighty to save; through Jesus Christ thy Son our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 166
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 952
      },
      days: [
        {
          day: 'Sunday',
          title: 'First Sunday in Lent',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Deut 8:1–10',
            second: '1 Cor 1:17–31',
            gospel: 'Mark 2:18–22'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Deut 8:11–20',
            second: 'Heb 2:11–18',
            gospel: 'John 2:1–12'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Deut 9:4–12',
            second: 'Heb 3:1–11',
            gospel: 'John 2:13–22'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Deut 9:13–21',
            second: 'Heb 3:12–19',
            gospel: 'John 2:23–3:15'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Deut 9:23–10:5',
            second: 'Heb 4:1–10',
            gospel: 'John 3:16–21'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Deut 10:12–22',
            second: 'Heb 4:11–16',
            gospel: 'John 3:22–36'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Deut 11:18–28',
            second: 'Heb 5:1–10',
            gospel: 'John 4:1–26'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 953
      },
      days: [
        {
          day: 'Sunday',
          title: 'First Sunday in Lent',
          psalms: {
            morning: [
              '63:1–8(9–11)',
              '98'
            ],
            evening: [
              '103'
            ]
          },
          lessons: {
            first: 'Dan 9:3–10',
            second: 'Heb 2:10–18',
            gospel: 'John 12:44–50'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '41',
              '52'
            ],
            evening: [
              '44'
            ]
          },
          lessons: {
            first: 'Gen 37:1–11',
            second: '1 Cor 1:1–19',
            gospel: 'Mark 1:1–13'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '45'
            ],
            evening: [
              '47',
              '48'
            ]
          },
          lessons: {
            first: 'Gen 37:12–24',
            second: '1 cor 1:20–31',
            gospel: 'Mark 1:14–28'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:49–72'
            ],
            evening: [
              '49',
              '[53]'
            ]
          },
          lessons: {
            first: 'Gen 37:25–36',
            second: '1 Cor 2:1–13',
            gospel: 'Mark 1:29–45'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '50'
            ],
            evening: [
              '[59]',
              '[60]',
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Gen 39:1–23',
            second: '1 Cor 2:14–3:15',
            gospel: 'Mark 2:1–12'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '40',
              '54'
            ],
            evening: [
              '51'
            ]
          },
          lessons: {
            first: 'Gen 40:1–23',
            second: '1 Cor 3:16–23',
            gospel: 'Mark 2:13–22'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '55'
            ],
            evening: [
              '138',
              '139:1–17(18–23)'
            ]
          },
          lessons: {
            first: 'Gen 41:1–13',
            second: '1 Cor 4:1–7',
            gospel: 'Mark 2:23–3:6'
          }
        }
      ]
    }
  ]
}
