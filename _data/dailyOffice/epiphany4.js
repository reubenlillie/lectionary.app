/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 4 Epiphany
 * @module _data/dailyOffice/epiphany4
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the Fourth Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, you govern all things both in heaven and on earth: Mercifully hear the supplications of your people, and in our time grant us your peace; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 215
        }
      ],
      traditional: [
        {
          text: 'Almighty and everlasting God, who dost govern all things in heaven and earth: Mercifully hear the supplications of thy people, and in our time grant us thy peace; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 164
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 946
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fourth Sunday after the Epiphany',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Isa 51:9–16',
            second: 'Heb 11:8–16',
            gospel: 'John 7:14–31'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Isa 51:17–23',
            second: 'Gal 4:1–11',
            gospel: 'Mark 7:24–37'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)'
            ]
          },
          lessons: {
            first: 'Isa 52:1–12',
            second: 'Gal 4:12–20',
            gospel: 'Mark 8:1–10'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Isa 54:1–10(11–17)',
            second: 'Gal 4:21–31',
            gospel: 'Mark 8:11–26'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Isa 55:1–13',
            second: 'Gal 5:1–15',
            gospel: 'Mark 8:27–9:1'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Isa 56:1–8',
            second: 'Gal 5:16–24',
            gospel: 'Mark 9:2–13'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Isa 57:3–13',
            second: 'Gal 5:25–6:10',
            gospel: 'Mark 9:14–29'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 947
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fourth Sunday after the Epiphany',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Gen 18:16–33',
            second: 'Gal 5:13–25',
            gospel: 'Mark 8:22–30'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Gen 19:1–17(18–23)',
            second: 'Heb 11:1–2',
            gospel: 'John 6:27–40'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)'
            ]
          },
          lessons: {
            first: 'Gen 21:1–21',
            second: 'Heb 11:13–22',
            gospel: 'John 6:41–51'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Gen 22:1–18',
            second: 'Heb 11:23–31',
            gospel: 'John 6:52–59'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Gen 23:1–20',
            second: 'Heb 11:32–12:2',
            gospel: 'John 6:60–71'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Gen 24:1–27',
            second: 'Heb 12:3–11',
            gospel: 'John 7:1–13'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Gen 24:28–38, 49–51',
            second: 'Heb 12:12–29',
            gospel: 'John 7:14–36'
          }
        }
      ]
    }
  ]
}
