/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 6 Epiphany
 * @module _data/dailyOffice/epiphany6
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the Sixth Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'O God, the strength of all who put their trust in you: Mercifully accept our prayers; and because in our weakness we can do nothing good without you, give us the help of your grace, that in keeping your commandments we may please you both in will and deed; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 216
        }
      ],
      traditional: [
        {
          text: 'O God, the strength of all those who put their trust in thee: Mercifully accept our prayers; and because, through the weakness of our mortal nature, we can do no good thing without thee, grant us the help of thy grace, that in keeping thy commandments we may please thee both in will and deed; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 164
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 946
      },
      days: [
        {
          day: 'Sunday',
          title: 'Sixth Sunday after the Epiphany',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Isa 62:6–12',
            second: '1 John 2:3–11',
            gospel: 'John 8:12–19'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Isa 63:1–6',
            second: '1 Tim 1:1–17',
            gospel: 'Mark 11:1–11'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Isa 63:7–14',
            second: '1 Tim 1:18–2:8',
            gospel: 'Mark 11:12–26'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: 'Isa 63:15–64:9',
            second: '1 Tim 3:1–16',
            gospel: 'Mark 11:27–12:12'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Isa 65:1–12',
            second: '1 Tim 4:1–16',
            gospel: 'Mark 12:13–27'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Isa 65:17–25',
            second: '1 Tim 5:17–22(23–25)',
            gospel: 'Mark 12:28–34'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Isa 66:1–6',
            second: '1 Tim 6:6–21',
            gospel: 'Mark 12:35–44'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 949
      },
      days: [
        {
          day: 'Sunday',
          title: 'Sixth Sunday after the Epiphany',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Gen 29:20–35',
            second: '1 Tim 3:14–4:10',
            gospel: 'Mark 10:23–31'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Gen 30:1–24',
            second: '1 John 1:1–10',
            gospel: 'John 9:1–17'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Gen 31:1–24',
            second: '1 John 2:1–11',
            gospel: 'John 9:18–41'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: 'Gen 31:25–50',
            second: '1 John 2:12–17',
            gospel: 'John 10:1–18'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Gen 32:3–21',
            second: '1 John 2:18–29',
            gospel: 'John 10:19–30'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Gen 32:22–33:17',
            second: '1 John 3:1–10',
            gospel: 'John 10:31–42'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Gen 35:1–20',
            second: '1 John 3:11–18',
            gospel: 'John 11:1–16'
          }
        }
      ]
    }
  ]
}
