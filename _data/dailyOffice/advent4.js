/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 4 Advent
 * @module _data/dailyOffice/advent4
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Advent',
    week: 'Week of the Fourth Sunday of Advent',
    collects: {
      contemporary: [
        {
          text: 'Purify our conscience, Almighty God, by your daily visitation, that your Son Jesus Christ, at his coming, may find in us a mansion prepared for himself; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
         bcpPage: 212
        }
      ],
      traditional: [
        {
          text: 'We beseech thee, Almighty God, to purify our consciences by thy daily visitation, that when thy Son our Lord cometh he may find in us a mansion prepared for himself; through the same Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 160
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 938
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fourth Sunday of Advent',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Isa 42:1–12',
            second: 'Eph 6:10–20',
            gospel: 'John 3:16–21'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '112',
              '115'
            ]
          },
          lessons: {
            first: 'Isa 11:1–9',
            second: 'Rev 20:1–10',
            gospel: 'John 5:30–47'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Isa 11:10–16',
            second: 'Rev 20:11–21:8',
            gospel: 'Luke 1:5–25'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '111',
              '113'
            ]
          },
          lessons: {
            first: 'Isa 28:9–22',
            second: 'Rev 21:9–21',
            gospel: 'Luke 1:26–38'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '146',
              '147'
            ]
          },
          lessons: {
            first: 'Isa 29:13–24',
            second: 'Rev 21:22–22:5',
            gospel: 'Luke 1:39–48'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '148',
              '150'
            ]
          },
          lessons: {
            first: 'Isa 33:17–22',
            second: 'Rev 22:6–11, 18–20',
            gospel: 'Luke 1:57–66'
          }
        },
        {
          day: 'Dec 24',
          title: 'Christmas Eve',
          psalms: {
            morning: [
              '45',
              '46'
            ],
            evening: [
              '89:1–29'
            ]
          },
          lessons: {
            morning: {
              first: 'Isa 35:1–10',
              second: 'Rev 22:12–17, 21',
              gospel: 'Luke 1:67–80'
            },
            evening: {
              first: 'Isa 59:15b–21',
              second: 'Phil 2:5–11'
            }
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 939
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fourth Sunday of Advent',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Gen 3:8–15',
            second: 'Rev 12:1–10',
            gospel: 'John 3:16–21'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '112',
              '115'
            ]
          },
          lessons: {
            first: 'Zeph 3:14–20',
            second: 'Titus 1:1–16',
            gospel: 'Luke 1:1–25'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '116',
              '117'
            ]
          },
          lessons: {
            first: '1 Sam 2:1b–10',
            second: 'Titus 2:1–10',
            gospel: 'Luke 1:26–38'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '111',
              '113'
            ]
          },
          lessons: {
            first: '2 Sam 7:1–17',
            second: 'Titus 2:11–3:8a',
            gospel: 'Luke 1:39–48a(48b–56)'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '146',
              '147'
            ]
          },
          lessons: {
            first: '2 Sam 7:18–29',
            second: 'Gal 3:1–14',
            gospel: 'Luke 1:57–66'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '148',
              '150'
            ]
          },
          lessons: {
            first: 'Bar 4:21–29',
            second: 'Gal 3:15–22',
            gospel: 'Luke 1:67–80',
            altGospel: 'Matt 1:1–17'
          }
        },
        {
          day: 'Dec 24',
          title: 'Christmas Eve',
          psalms: {
            morning: [
              '45',
              '46'
            ],
            evening: [
              '89:1–29'
            ]
          },
          lessons: {
            morning: {
              first: 'Bar 4:36–5:9',
              second: 'Gal 3:23–4:7',
              gospel: 'Matt 1:18–25'
            },
            evening: {
              first: 'Isa 59:15b–21',
              second: 'Phil 2:5–11'
            }
          }
        }
      ]
    }
  ]
}
