/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 2 Easter
 * @module _data/dailyOffice/easter2
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Easter',
    week: 'Week of the Second Sunday of Easter',
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, who in the Paschal mystery established the new covenant of reconciliation: Grant that all who have been reborn into the fellowship of Christ’s Body may show forth in their lives what they profess by their faith; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
         bcpPage: 224
        }
      ],
      traditional: [
        {
          text: 'Almighty and everlasting God, who in the Paschal mystery hast established the new covenant of reconciliation: Grant that all who have been reborn into the fellowship of Christ’s Body may show forth in their lives what they profess by their faith; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 172
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 958
      },
      days: [
        {
          day: 'Sunday',
          title: 'Second Sunday of Easter',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Isa 43:8–13',
            second: '1 Pet 2:2–10',
            gospel: 'John 14:1–7'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Dan 1:1–21',
            second: '1 John 1:1–10',
            gospel: 'John 17:1–11'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Dan 2:1–16',
            second: '1 John 2:1–11',
            gospel: 'John 17:12–19'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Dan 2:17–30',
            second: '1 John 2:12–17',
            gospel: 'John 17:20–26'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Dan 2:31–49',
            second: '1 John 2:18–29',
            gospel: 'Luke 3:1–14'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Dan 3:1–18',
            second: '1 John 3:1–10',
            gospel: 'Luke 3:15–22'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Dan 3:19–30',
            second: '1 John 3:11–18',
            gospel: 'Luke 4:1–13'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 959
      },
      days: [
        {
          day: 'Sunday',
          title: 'Second Sunday of Easter',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Exod 14:5–22',
            second: '1 John 1:1–7',
            gospel: 'John 14:1–7'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Exod 14:21–31',
            second: '1 Pet 1:1–12',
            gospel: 'John 14:(1–7)8–17'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Exod 15:1–21',
            second: '1 Pet 1:13–25',
            gospel: 'John 14:18–31'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Exod 15:22–16:10',
            second: '1 Pet 2:1–10',
            gospel: 'John 15:1–11'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Exod 16:10–22',
            second: '1 Pet 2:11–25',
            gospel: 'John 15:12–27'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Exod 16:23–36',
            second: '1 Pet 3:13–4:6',
            gospel: 'John 16:1–15'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Exod 17:1–16',
            second: '1 Pet 4:7–19',
            gospel: 'John 16:16–33'
          }
        }
      ]
    }
  ]
}
