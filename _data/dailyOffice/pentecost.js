/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of Pentecost
 * @module _data/dailyOffice/pentecost
 * @since 0.1.0
 * @since 0.11.0 Add `color` properties
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Easter',
    week: 'Week of Pentecost',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, on this day you opened the way of eternal life to every race and nation by the promised gift of your Holy Spirit: Shed abroad this gift throughout the world by the preaching of the Gospel, that it may reach to the ends of the earth; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, for ever and ever.',
         bcpPage: 227
        },
        {
          text: 'O God, who on this day taught the hearts of your faithful people by sending to them the light of your Holy Spirit: Grant us by the same Spirit to have a right judgment in all things, and evermore to rejoice in his holy comfort; through Jesus Christ your Son our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, for ever and ever.',
         bcpPage: 227
        }
      ],
      traditional: [
        {
          text: 'Almighty God, who on this day didst open the way of eternal life to every race and nation by the promised gift of thy Holy Spirit: Shed abroad this gift throughout the world by the preaching of the Gospel, that it may reach to the ends of the earth; through Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the same Spirit, one God, for ever and ever.',
          bcpPage: 175
        },
        {
          text: 'O God, who on this day didst teach the hearts of thy faithful people by sending to them the light of thy Holy Spirit: Grant us by the same Spirit to have a right judgment in all things, and evermore to rejoice in his holy comfort; through the merits of Christ Jesus our Savior, who liveth and reigneth with thee, in the unity of the same Spirit, one God, world without end.',
          bcpPage: 175
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 964
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'The Day of Pentecost: Whitsunday',
            main: 'Pentecost',
            sub: 'Whitsunday'
          },
          color: 'red',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Isa 11:1–9',
            second: '1 Cor 2:1–13',
            gospel: 'John 14:21–29'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 965
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'The Day of Pentecost: Whitsunday',
            main: 'Pentecost',
            sub: 'Whitsunday'
          },
          color: 'red',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Deut 16:9–12',
            second: 'Acts 4:18–21, 23–33',
            gospel: 'John 4:19–26'
          }
        }
      ]
    }
  ]
}
