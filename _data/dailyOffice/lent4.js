/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 4 Lent
 * @module _data/dailyOffice/lent4
 * @since 0.1.0
 * @since 0.11.0 Add `color` properties
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Lent',
    week: 'Week of the Fourth Sunday in Lent',
    collects: {
      contemporary: [
        {
          text: 'Gracious Father, whose blessed Son Jesus Christ came down from heaven to be the true bread which gives life to the world: Evermore give us this bread, that he may live in us, and we in him; who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 219
        }
      ],
      traditional: [
        {
          text: 'Gracious Father, whose blessed Son Jesus Christ came down from heaven to be the true bread which giveth life to the world: Evermore give us this bread, that he may live in us, and we in him; who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 167
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 954
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fourth Sunday in Lent',
          color: 'pink',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Jer 14:1–9, 17–22',
            second: 'Gal 4:21–5:1',
            gospel: 'Mark 8:11–21'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Jer 16:10–21',
            second: 'Rom 7:1–12',
            gospel: 'John 6:1–15'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Jer 17:19–27',
            second: 'Rom 7:13–25',
            gospel: 'John 6:16–27'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: 'Jer 18:1–11',
            second: 'Rom 8:1–11',
            gospel: 'John 6:27–40'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Jer 22:13–23',
            second: 'Rom 8:12–27',
            gospel: 'John 6:41–51'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Jer 23:1–8',
            second: 'Rom 8:28–39',
            gospel: 'John 6:52–59'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Jer 23:9–15',
            second: 'Rom 9:1–18',
            gospel: 'John 6:60–71'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 955
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fourth Sunday in Lent',
          color: 'pink',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Gen 48L8–22',
            second: 'Rom 8:11–25',
            gospel: 'John 6:27–40'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Gen 49:1–28',
            second: '1 Cor 10:14–11:2',
            gospel: 'Mark 7:24–37'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Gen 49:29–50:14',
            second: '1 Cor 11:17–34',
            gospel: 'Mark 8:1–10'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: 'Gen 50:15–26',
            second: '1 Cor 12:1–11',
            gospel: 'Mark 8:11–26'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Exod 1:6–22',
            second: '1 Cor 12:12–26',
            gospel: 'Mark 8:27–9:1'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Exod 2:1–22',
            second: '1 Cor 12:27–13:3',
            gospel: 'Mark 9:1–13'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Exod 2:23–3:15',
            second: '1 Cor 13:1–13',
            gospel: 'Mark 9:14–29'
          }
        }
      ]
    }
  ]
}
