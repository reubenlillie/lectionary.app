/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 5 Epiphany
 * @module _data/dailyOffice/epiphany5
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the Fifth Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'Set us free, O God, from the bondage of our sins, and give us the liberty of that abundant life which you have made known to us in your Son our Savior Jesus Christ; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 216
        }
      ],
      traditional: [
        {
          text: 'Set us free, O God, from the bondage of our sins and give us, we beseech thee, the liberty of that abundant life which thou hast manifested to us in thy Son our Savior Jesus Christ; who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 164
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 946
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fifth Sunday after the Epiphany',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Isa 57:14–21',
            second: 'Heb 12:1–6',
            gospel: 'John 7:37–46'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Isa 58:1–12',
            second: 'Gal 6:11–18',
            gospel: 'Mark 9:30–41'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Isa 59:1–15a',
            second: '2 Tim 1:1–14',
            gospel: 'Mark 9:42–50'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Isa 59:15b–21',
            second: '2 Tim 1:15–2:13',
            gospel: 'Mark 10:1–16'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '146',
              '147'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: 'Isa 60:1–17',
            second: '2 Tim 2:14–26',
            gospel: 'Mark 10:17–31'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Isa 61:1–9',
            second: '2 Tim 3:1–17',
            gospel: 'Mark 10:32–45'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Isa 61:10–62:5',
            second: '2 Tim 4:1–8',
            gospel: 'Mark 10:46–52'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 947
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fifth Sunday after the Epiphany',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Gen 24:50–67',
            second: '2 Tim 2:14–21',
            gospel: 'Mark 10:13–22'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Gen 25:19–34',
            second: 'Heb 13:1–16',
            gospel: 'John 7:37–52'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Gen 26:1–6. 12–33',
            second: 'Heb 13:17–25',
            gospel: 'John 7:53–8:11'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Gen 27:1–29',
            second: 'Rom 12:1–8',
            gospel: 'John 8:12–20'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '146',
              '147'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: 'Gen 27:30–45',
            second: 'Rom12:9–21',
            gospel: 'John 8:21–32'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Gen 27:46–28:4, 10–22',
            second: 'Rom 13:1–14',
            gospel: 'John 8:33–47'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Gen 29:1–20',
            second: 'Rom 14:1–23',
            gospel: 'John 8:47–59'
          }
        }
      ]
    }
  ]
}
