/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 17
 * @module _data/dailyOffice/proper17
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 17',
    collects: {
      contemporary: [
        {
          text: 'Lord of all power and might, the author and giver of all good things: Graft in our hearts the love of your Name; increase in us true religion; nourish us with all goodness; and bring forth in us the fruit of good works; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God for ever and ever.',
          bcpPage: 233
        }
      ],
      traditional: [
        {
          text: 'Lord of all power and might, who art the author and giver of all good things: Graft in our hearts the love of thy Name, increase in us true religion, nourish us with all goodness, and bring forth in us the fruit of good works; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 181
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 982
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: '1 Kgs 8:22–30(31–40)',
            second: '1 Tim 4:7b–16',
            gospel: 'John 8:47–59'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: '2 Chr 6:32–7:7',
            second: 'Jas 2:1–13',
            gospel: 'Mark 14:53–65'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: '1 Kgs 8:65–9:9',
            second: 'Jas 2:14–26',
            gospel: 'Mark 14:66–72'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: '1 Kgs 9:24–10:13',
            second: 'Jas 3:1–12',
            gospel: 'Mark 15:1–11'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–8'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: '1 Kgs 11:1–13',
            second: 'Jas 3:13–4:12',
            gospel: 'Mark 15:12–21'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: '1 Kgs 11:26–43',
            second: 'Jas 4:13–6:5',
            gospel: 'Mark 15:22–32'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: '1 Kgs 12:1–20',
            second: 'Jas 5:7–12, 19–20',
            gospel: 'Mark 15:33–39'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 983
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Job 11:1–9, 13–20',
            second: 'Rev 5:1–14',
            gospel: 'Matt 5:1–12'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Job 12:1–6, 13–25',
            second: 'Acts 11:19–30',
            gospel: 'John 8:21–32'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Job 12:1; 13:3–17, 21–27',
            second: 'Acts 12:1–17',
            gospel: 'John 8:33–47'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Job 12:1; 14:1–22',
            second: 'Acts 12:18–25',
            gospel: 'John 8:47–59'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–8'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Job 16:16–22; 17:1, 13–16',
            second: 'Acts 13:1–12',
            gospel: 'John 9:1–17'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Job 19:1–7, 14–27',
            second: 'Acts 13:13–25',
            gospel: 'John 9:18–41'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Job 22:1–4, 21–23:7',
            second: 'Acts 13:26–43',
            gospel: 'John 10:1–18'
          }
        }
      ]
    }
  ]
}
