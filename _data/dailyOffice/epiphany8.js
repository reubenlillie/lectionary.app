/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 8 Epiphany
 * @module _data/dailyOffice/epiphany8
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the Eighth Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'Most loving Father, whose will it is for us to give thanks for all things, to fear nothing but the loss of you, and to cast all our care on you who care for us: Preserve us from faithless fears and worldly anxieties, that no clouds of this mortal life may hide from us the light of that love which is immortal, and which you have manifested to us in your Son Jesus Christ our Lord; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 216
        }
      ],
      traditional: [
        {
          text: 'O most loving Father, who willest us to give thanks for all things, to dread nothing but the loss of thee, and to cast all our care on thee who carest for us: Preserve us from faithless fears and worldly anxieties, and grant that no clouds of this mortal life may hide from us the light of that love which is immortal, and which thou hast manifested unto us in thy Son Jesus Christ our Lord; who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 165
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 950
      },
      days: [
        {
          day: 'Sunday',
          title: 'Eighth Sunday after the Epiphany',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Deut 4:1–9',
            second: '2 Tim 4:1–8',
            gospel: 'John 12:1–8'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Deut 4:9–14',
            second: '2 Cor 10:1–18',
            gospel: 'Matt 6:7–15'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Deut 4:15–24',
            second: '2 Cor 11:1–21a',
            gospel: 'Matt 6:16–23'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Deut 4:23–31',
            second: '2 Cor 11:21b–33',
            gospel: 'Matt 6:24–34'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Deut 4:32–40',
            second: '2 Cor 12:1–10',
            gospel: 'Matt 7:1–12'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Deut 5:1–22',
            second: '2 Cor 12:11–21',
            gospel: 'Matt 7:13–21'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Deut 5:23–33',
            second: '2 Cor 13:1–14',
            gospel: 'Matt 7:22–29'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 951
      },
      days: [
        {
          day: 'Sunday',
          title: 'Eighth Sunday after the Epiphany',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Prov 9:1–12',
            second: '2 Cor 9:6b–15',
            gospel: 'Mark 10:46–52'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Prov 10:1–12',
            second: '2 Tim 1:15–2:13',
            gospel: 'John 12:27–36a'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Prov 15:16–33',
            second: '2 Tim 2:14–26',
            gospel: 'John 12:36b–50'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Prov 17:1–20',
            second: '2 Tim 3:1–17',
            gospel: 'John 13:1–20'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Prov 21:30–22:6',
            second: '2 Tim 4:1–8',
            gospel: 'John 13:21–30'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Prov 23:19–21, 29–24:2',
            second: '2 Tim 4:9–22',
            gospel: 'John 13:31–38'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Prov 25:15–28',
            second: 'Phil 1:1–11',
            gospel: 'John 18:1–14'
          }
        }
      ]
    }
  ]
}
