/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 5 Lent
 * @module _data/dailyOffice/lent5
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Lent',
    week: 'Week of the Fifth Sunday in Lent',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, you alone can bring into order the unruly wills and affections of sinners: Grant your people grace to love what you command and desire what you promise; that, among the swift and varied changes of the world, our hearts may surely there be fixed where true joys are to be found; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 219
        }
      ],
      traditional: [
        {
          text: 'O Almighty God, who alone canst order the unruly wills and affections of sinful men: Grant unto thy people that they may love the thing which thou commandest, and desire that which thou dost promise; that so, among the sundry and manifold changes of the world, our hearts may surely there be fixed where true joys are to be found; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 167
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 956
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fifth Sunday in Lent',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Jer 23:16–32',
            second: '1 Cor 9:19–27',
            gospel: 'Mark 8:31–9:1'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Jer 24:1–10',
            second: 'Rom 9:19–33',
            gospel: 'John 9:1–17'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Jer 25:8–17',
            second: 'Rom 10:1–13',
            gospel: 'John 9:18–41'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Jer 25:30–38',
            second: 'Rom 10:14–21',
            gospel: 'John 10:1–18'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '140',
              '142'
            ]
          },
          lessons: {
            first: 'Jer 26:1–16',
            second: 'Rom 11:1–12',
            gospel: 'John 10:19–42'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '22'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Jer 29:1, 4–13',
            second: 'Rom 11:13–24',
            gospel: 'John 11:1–27',
            altGospel: 'John 12:1–10'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Jer 31:27–34',
            second: 'Rom 11:25–36',
            gospel: 'John 11:28–44',
            altGospel: 'John 12:37–50'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 957
      },
      days: [
        {
          day: 'Sunday',
          title: 'Fifth Sunday in Lent',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Exod 3:16–4:12',
            second: 'Rom 12:1–21',
            gospel: 'John 8:46–59'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Exod 4:10–20(21–26)27–31',
            second: '1 Cor 14:1–19',
            gospel: 'Mark 9:30–41'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Exod 5:1–6:1',
            second: '1 Cor 14:20–33a, 39–40',
            gospel: 'Mark 9:42–50'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Exod 7:8–24',
            second: '2 Cor 2:14–3:6',
            gospel: 'Mark 10:1–16'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '140',
              '142'
            ]
          },
          lessons: {
            first: 'Exod 7:25–8:19',
            second: '2 Cor 3:7–18',
            gospel: 'Mark 10:176–31'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '22'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Exod 9:13–35',
            second: '2 Cor 4:1–12',
            gospel: 'Mark 10:32–45'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Exod 10:21–11:8',
            second: '2 Cor 4:13–18',
            gospel: 'Mark 10:46–52'
          }
        }
      ]
    }
  ]
}
