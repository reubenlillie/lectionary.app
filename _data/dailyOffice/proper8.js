/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 8
 * @module _data/dailyOffice/proper8
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 8',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, you have built your Church upon the foundation of the apostles and prophets, Jesus Christ himself being the chief cornerstone: Grant us so to be joined together in unity of spirit by their teaching, that we may be made a holy temple acceptable to you; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 230
        }
      ],
      traditional: [
        {
          text: 'O Almighty God, who hast built thy Church upon the foundation of the apostles and prophets, Jesus Christ himself being the chief cornerstone: Grant us so to be joined together in unity of spirit by their doctrine, that we may be made an holy temple acceptable unto thee; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 178
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 972
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: '1 Sam 10:1–16',
            second: 'Rom 4:13–25',
            gospel: 'Matt 21:23–32'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: '1 Sam 10:17–27',
            second: 'Acts 7:44–8:1a',
            gospel: 'Luke 22:52–62'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: '1 Sam 11:1–15',
            second: 'Acts 8:1–13',
            gospel: 'Luke 22:63–71'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: '1 Sam 12:1–6',
            second: 'Acts 8:14–25',
            gospel: 'Luke 23:1–12'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: '1 Sam 13:5–18',
            second: 'Acts 8:26–40',
            gospel: 'Luke 23:13–25'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: '1 Sam 13:19–14:15',
            second: 'Acts 9:1–9',
            gospel: 'Luke 23:26–31'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: '1 Sam 14:16–30',
            second: 'Acts 9:10–19a',
            gospel: 'Luke 23:32–43'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 973
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Num 21:4–9, 21–35',
            second: 'Acts 17:(12–21)22–34',
            gospel: 'Luke 13:10–17'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Num 22:1–21',
            second: 'Rom 6:12–23',
            gospel: 'Matt 21:12–22'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Num 22:21–38',
            second: 'Rom 7:1–12',
            gospel: 'Matt 21:23–32'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Num 22:41–23:12',
            second: 'Rom 7:13–25',
            gospel: 'Matt 21:33–46'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Num 23:11–26',
            second: 'Rom 8:1–11',
            gospel: 'Matt 22:1–14'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Num 24:1–13',
            second: 'Rom 8:12–17',
            gospel: 'Matt 22:15–22'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Num 24:12–25',
            second: 'Rom 8:18–25',
            gospel: 'Matt 22:23–40'
          }
        }
      ]
    }
  ]
}
