/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 3
 * @module _data/dailyOffice/proper3
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 3',
    collects: {
      contemporary: [
        {
          text: 'Grant, O Lord, that the course of this world may be peaceably governed by your providence; and that your Church may joyfully serve you in confidence and serenity; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 229
        }
      ],
      traditional: [
        {
          text: 'Grant, O Lord, we beseech thee, that the course of this world may be peaceably governed by thy providence, and that thy Church may joyfully serve thee in confidence and serenity; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 177
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 968
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Deut 4:1–9',
            second: 'Rev 7:1–4, 9–17',
            gospel: 'Matt 12:33–45'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Deut 4:9–14',
            second: '2 Cor 1:11',
            gospel: 'Luke 14:25–35'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Deut 4:15–24',
            second: '2 Cor 1:12–22',
            gospel: 'Luke 15:1–10'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Deut 4:25–31',
            second: '2 Cor 1:23–2:17',
            gospel: 'Luke 15:1–2, 11–32'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Deut 4:32–40',
            second: '2 Cor 3:1–18',
            gospel: 'Luke 16:1–9'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Deut 5:1–22',
            second: '2 Cor 4:1–12',
            gospel: 'Luke 16:10–17(18)'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Deut 5:22–33',
            second: '2 Cor 4:13–5:10',
            gospel: 'Luke 16:19–31'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 969
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Prov 9:1–12',
            second: 'Acts 8:14–25',
            gospel: 'Luke 10:25–28, 38–42'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Prov 10:1–12',
            second: '1 Tim 1:1–17',
            gospel: 'Matt 12:22–32'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Prov 15:16–33',
            second: '1 Tim 1:18–2:8',
            gospel: 'Matt 12:33–42'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Prov 17:1–20',
            second: '1 Tim 3:1–16',
            gospel: 'Matt 12:43–50'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Prov 21:30–22:6',
            second: '1 Tim 4:1–16',
            gospel: 'Matt 13:24–30'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Prov 23:19–21, 29–24:2',
            second: '1 Tim 5:17–22(23–25)',
            gospel: 'Matt 13:31–35'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Prov 25:15–28',
            second: '1 Tim 6:6–21',
            gospel: 'Matt 13:36–43'
          }
        }
      ]
    }
  ]
}
