/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 22
 * @module _data/dailyOffice/proper22
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 22',
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, you are always more ready to hear than we to pray, and to give more than we either desire or deserve: Pour upon us the abundance of your mercy, forgiving us those things of which our conscience is afraid, and giving us those good things for which we are not worthy to ask, except through the merits and mediation of Jesus Christ our Savior; who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 234
        }
      ],
      traditional: [
        {
          text: 'Almighty and everlasting God, who art always more ready to hear than we to pray, and art wont to give more than either we desire or deserve: Pour down upon us the abundance of thy mercy, forgiving us those things whereof our conscience is afraid, and giving us those good things which we are not worthy to ask, but through the merits and mediation of Jesus Christ thy Son our Lord; who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 182
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 986
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: '2 Kgs 20:1–21',
            second: 'Acts 12:1–17',
            gospel: 'Luke 7:11–17'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: '2 Kgs 21:1–18',
            second: '1 Cor 10:14–11:1',
            gospel: 'Matt 8:28–34'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: '2 Kgs 22:1–13',
            second: '1 Cor 11:2, 17–22',
            gospel: 'Matt 9:1–8'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: '2 Kgs 22:14–23:3',
            second: '1 Cor 11:23–34',
            gospel: 'Matt 9:9–17'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: '2 Kgs 23:4–25',
            second: '1 Cor 12:1–11',
            gospel: 'Matt 9:18–26'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: '2 Kgs 23:36–24:17',
            second: '1 Cor 12:12–26',
            gospel: 'Matt 9:27–34'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Jer 35:1–19',
            second: '1 Cor 12:27–13:3',
            gospel: 'Matt 9:35–10:4'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 987
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Hos 13:4–14',
            second: '1 Cor 2:6–16',
            gospel: 'Matt 14:1–12'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Hos 14:1–9',
            second: 'Acts 22:30–23:11',
            gospel: 'Luke 6:39–49'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Mic 1:1–9',
            second: 'Acts 23:12–24',
            gospel: 'Luke 7:1–17'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Mic 2:1–13',
            second: 'Acts 23:23–35',
            gospel: 'Luke 7:18–35'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Mic 3:1–8',
            second: 'Acts 24:1–23',
            gospel: 'Luke 7:36–50'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Mic 3:9–4:5',
            second: 'Acts 24:24–25:12',
            gospel: 'Luke 8:1–15'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Mic 5:1–4, 10–15',
            second: 'Acts 25:13–27',
            gospel: 'Luke 8:16–25'
          }
        }
      ]
    }
  ]
}
