/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 7 Easter
 * @module _data/dailyOffice/easter7
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Easter',
    week: 'Week of the Seventh Sunday of Easter',
    collects: {
      contemporary: [
        {
          text: 'O God, the King of glory, you have exalted your only Son Jesus Christ with great triumph to your kingdom in heaven: Do not leave us comfortless, but send us your Holy Spirit to strengthen us, and exalt us to that place where our Savior Christ has gone before; who lives and reigns with you and the Holy Spirit, one God, in glory everlasting.',
         bcpPage: 226
        }
      ],
      traditional: [
        {
          text: 'O God, the King of glory, who hast exalted thine only Son Jesus Christ with great triumph unto thy kingdom in heaven: We beseech thee, leave us not comfortless, but send to us thine Holy Ghost to comfort us, and exalt us unto the same place whither our Savior Christ is gone before; who liveth and reigneth with thee and the same Holy Ghost, one God, world without end.',
          bcpPage: 175
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 964
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'Seventh Sunday of Easter: The Sunday after Ascension Day',
            main: 'Sunday after the Ascension',
            sub: 'Seventh Sunday of Easter'
          },
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Ezek 3:16–27',
            second: 'Eph 2:1–10',
            gospel: 'Matt 10:24–33, 40–42'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Ezek 4:1–17',
            second: 'Heb 6:1–12',
            gospel: 'Luke 9:51–62'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Ezek 7:10–15, 23b–27',
            second: 'Heb 6:13–20',
            gospel: 'Luke 10:1–17'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '11:121–144'
            ]
          },
          lessons: {
            first: 'Ezek 11:14–25',
            second: 'Heb 7:1–17',
            gospel: 'Luke 10:17–24'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Ezek 18:1–4, 19:32',
            second: 'Heb 7:18–28',
            gospel: 'Luke 10:25–37'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Ezek 34:17–31',
            second: 'Heb 8:1–13',
            gospel: 'Luke 10:38–42'
          }
        },
        {
          day: 'Saturday',
          title: 'Eve of Pentecost',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            morning: {
              first: 'Ezek 43:1–12',
              second: 'Heb 9:1–14',
              gospel: 'Luke 11:14–23'
            },
            evening: {
              first: 'Exod 19:3–8a, 16–20',
              second: '1 Pet 2:4–10'
            }
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 965
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'Seventh Sunday of Easter: The Sunday after Ascension Day',
            main: 'Sunday after the Ascension',
            sub: 'Seventh Sunday of Easter'
          },
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Exod 3:1–12',
            second: 'Heb 12:18–29',
            gospel: 'Luke 10:17–24'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Josh 1:1–9',
            second: 'Eph 3:1–13',
            gospel: 'Matt 8:5–17'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: '1 Sam 16:1–13a',
            second: 'Eph 3:14–21',
            gospel: 'Matt 8:18–27'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '11:121–144'
            ]
          },
          lessons: {
            first: 'Isa 4:2–6',
            second: 'Eph 4:1–16',
            gospel: 'Matt 8:28–34'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Zech 4:1–14',
            second: 'Eph 4:17–32',
            gospel: 'Matt 9:1–8'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Jer 31:27–34',
            second: 'Eph 5:1–20',
            gospel: 'Matt 9:9–17'
          }
        },
        {
          day: 'Saturday',
          title: 'Eve of Pentecost',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            morning: {
              first: 'Ezek 36:22–27',
              second: 'Eph 6:10–24',
              gospel: 'Matt 9:18–26'
            },
            evening: {
              first: 'Exod 19:3–8a, 16–20',
              second: '1 Pet 2:4–10'
            }
          }
        }
      ]
    }
  ]
}
