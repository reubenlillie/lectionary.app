/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 7 Epiphany
 * @module _data/dailyOffice/epiphany7
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the Seventh Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'O Lord, you have taught us that without love whatever we do is worth nothing: Send your Holy Spirit and pour into our hearts your greatest gift, which is love, the true bond of peace and of all virtue, without which whoever lives is accounted dead before you. Grant this for the sake of your only Son Jesus Christ, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 216
        }
      ],
      traditional: [
        {
          text: 'O Lord, who hast taught us that all our doings without charity are nothing worth: Send thy Holy Ghost and pour into our hearts that most excellent gift of charity, the very bond of peace and of all virtues, without which whosoever liveth is counted dead before thee. Grant this for thine only Son Jesus Christ’s sake, who liveth and reigneth with thee and the same Holy Ghost, one God, now and for ever.',
          bcpPage: 164
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 948
      },
      days: [
        {
          day: 'Sunday',
          title: 'Seventh Sunday after the Epiphany',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Isa 66:7–14',
            second: '1 John 3:4–10',
            gospel: 'John 10:7–16'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Ruth 1:1–14',
            second: '2 Cor 1:1–11',
            gospel: 'Matt 5:1–12'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Ruth 1:15–22',
            second: '2 Cor 1:12–22',
            gospel: 'Matt 5:13–20'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:146–175'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Ruth 2:14–23',
            second: '2 Cor 1:23–2:17',
            gospel: 'Matt 5:21–26'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Ruth 2:14–23',
            second: '2 Cor 3:1–18',
            gospel: 'Matt 5:27–37'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Ruth 3:1–18',
            second: '2 Cor 4:1–12',
            gospel: 'Matt 5:38–48'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Ruth 4:1–17',
            second: '2 Cor 4:13–5:10',
            gospel: 'Matt 6:1–16'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        season: 'Epiphany',
        week: 'Week of the Seventh Sunday after the Epiphany',
        bcpPage: 949
      },
      days: [
        {
          day: 'Sunday',
          title: 'Seventh Sunday after the Epiphany',
          psalms: {
            morning: [
              '118'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Prov 1:20–33',
            second: '2 Cor 5:11.21',
            gospel: 'Mark 10:35–45'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Prov 3:11–20',
            second: '1 John 3:18–4:6',
            gospel: 'John 11:17–29'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Prov 4:1–27',
            second: '1 John 4:7–21',
            gospel: 'John 11:30–44'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:146–175'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Prov 6:1–19',
            second: '1 John 5:1–12',
            gospel: 'John 11:45–54'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Prov 7:1–27',
            second: '1 John 5:13–21',
            gospel: 'John 11:55–12:8'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Prov 8:1–21',
            second: 'Phlm 1–25',
            gospel: 'John 12:9–19'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Prov 8:22–36',
            second: '2 Tim 1:1–14',
            gospel: 'John 12:20–26'
          }
        }
      ]
    }
  ]
}
