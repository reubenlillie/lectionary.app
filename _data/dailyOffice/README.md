# `./_data/dailyOffice/`

This directory contains [global data files](https://www.11ty.dev/docs/data-global/) for the Daily Office Lectionary from the [_Book of Common Prayer_](https://episcopalchurch.org/files/book_of_common_prayer.pdf) (<abbr>BCP</abbr> 2016).

## Summary

Lectionary entries follow the same basic pattern. For each entry, there is at least (a) one psalm for either Morning Prayer and/or Evening Prayer and (b) one lesson from the Hebrew Bible, deuterocanonical works, and/or New Testament for either a specific calendar date or a day of the week within a season of the church year. Entries vary in the number of psalms, lessons, and any alternative lessons.

## File name and structure

Each week or proper entry has its own `*.js` module file. The name for each file follows that entry’s location within the lectionary.  Entries follow the headings within <abbr>BCP</abbr> (1979[2016]).

> For example: Year One, First Sunday of Advent is `oneAdvent1.js`.

Because of the change in season, Ash Wednesday and the days leading up to the first full week in Lent are in a separate file from the Sunday, Monday, and Tuesday of the Last Week after the Epiphany.

Simiarly, the <abbr>BCP</abbr> (1979[2016]) appends Pentecost and Trinity to the entry for the Seventh Week of Easter. In order to access these entries more easily, the Eve of Pentecost and the Day of Pentecost are grouped in a separate file as are the Eve of Trinity Sunday and Trinity Sunday.

## Schema

The following schema models this pattern while allowing for the particular shape any given entry may take.

```js
// example from oneAdvent1.js

/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, 
 * Year One, Week of 1 Advent
 * @module _data/dailyOffice/oneAdvent1
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: { // Properties common to all days in this entry file
    year: 'Year One',
    season: 'Advent', //  or 'Christmas', 'Epiphany', 'Lent', 'Easter', 'The Season after Pentecost'
    week: 'Week or proper name',
    bcpPage: 937 // page range: 934–1001
  },
  days: [ // An array of objects for each day/service within this week or proper
    {
      day: 'Sunday', // Weekday name or formatted date (short month, numeric day, e.g., Jan 1)
      title: 'First Sunday of Advent', // For Sundays and Holy Day, see below
      psalms: {
        morning: [
          '146', // Note: like other references, psalms are strings, not numbers
          '147'
        ],
        evening: [
          '111',
          '112',
          '113'
        ]
      },
      lessons: { // optionally group lessons in morning and evening objects
        first: 'Isa 1:1–9', // optional altFirst
        second: '2 Pet 3:1–10', // optional altSecond
        gospel: 'Matt 25:1–13' // optional altGospel
      }
    }
    … etc.
  ]
}
```

Or, for when an item in the `days` array may have different versions of its `title`, such as certain Sundays or Holy Days. For example:

```js
// example from oneChristmas.js

export default {
  props: {… etc.},
  days: [
    {
      title: {
        full: 'The Nativity of Our Lord Jesus Christ: Christmas Day',
        main: 'Christmas Day',
        sub: 'The Nativity of Our Lord Jesus Christ'
      }
      … etc.
    }
  ]
}
```

Note that `psalms.morning` and `psalms.evening` are _arrays of strings_, not numbers, and that at least one array appears in each entry. But `lessons` (or `lessons.morning` and `lessons.evening` when applicable) is an _object_ containing the named properties `first`, `second`, `gospel`, and so forth.

## Formatting

Entries are combined for days that have special evening readings in addition to their regular readings. For example, December 24 and Christmas Eve are separate items in <abbr>BCP</abbr> (1979[2016]([<abbr>BCP</abbr> 2016, 938–939](https://www.episcopalchurch.org/files/book_of_common_prayer.pdf#938))), but they are a single object with both a `day` and `title` for the purposes of this project.

Values for the `title` property are adapted from “The Titles of the Seasons, Sundays, and Major Holy Days” ([<abbr>BCP</abbr> 2016, 31](https://www.episcopalchurch.org/files/book_of_common_prayer.pdf#page=31)).

Bible book abbreviations have been adapted to follow the _SBL Handbook of Style_, 2nd ed., from the [Society of Biblical Literature](https://www.sbl-site.org/).

Ranges between chapters and verses are marked by en dashes (`–`), not hyphens (`-`).

A few objects in the `days` array contain a `notes` object. Only `notes` that apply to specific days or services and could not otherwise be interpretted within the schema are included.

For example, the note for Jan 3 and Jan 4 (see [<abbr>BCP</abbr> 2016, 940](https://episcopalchurch.org/files/book_of_common_prayer.pdf#page=940)) says “If today is Saturday, use Psalms 23 and 27 at Evening Prayer.” But the alternate readings in Judith in place of Esther for Proper 18 and Proper 19 in Year Two are treated as `altFirst` on those days. Also, notes that apply to several days have been omitted—like Psalm 95 is “For the Invitatory” on Fridays in Lent. That way, such notes can be handled more programmatically rather than duplicating the same data across multiple objects.
