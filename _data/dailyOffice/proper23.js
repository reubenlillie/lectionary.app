/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 23
 * @module _data/dailyOffice/proper23
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 23',
    collects: {
      contemporary: [
        {
          text: 'Lord, we pray that your grace may always precede and follow us, that we may continually be given to good works; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 234
        }
      ],
      traditional: [
        {
          text: 'Lord, we pray thee that thy grace may always precede and follow us, and make us continually to be given to all good works; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 183
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 988
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Jer 36:1–10',
            second: 'Acts 14:8–18',
            gospel: 'Luke 7:36–50'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Jer 36:11–26',
            second: '1 Cor 13:(1–3)4–13',
            gospel: 'Matt 10:5–15'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Jer 36:27:37–2',
            second: '1 Cor 14:1–12',
            gospel: 'Matt 10:16–23'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Jer 37:3–21',
            second: '1 Cor 14:13–25',
            gospel: 'Matt 10:24–33'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Jer 38:1–13',
            second: '1 Cor 14:26–33a, 37–40',
            gospel: 'Matt 10:34–42'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Jer 38:14–28',
            second: '1 Cor 15:1–11',
            gospel: 'Matt 11:1–6'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: '2 Kgs 25:8–12, 22–26',
            second: '1 Cor 15:12–29',
            gospel: 'Matt 11:7–15'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 989
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Mic 6:1–8',
            second: '1 Cor 4:9–16',
            gospel: 'Matt 15:21–28'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Mic 7:1–7',
            second: 'Acts 26:1–23',
            gospel: 'Luke 8:26–39'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Jonah 1:1–17a',
            second: 'Acts 26:24–27:8',
            gospel: 'Luke 8:40–56'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Jonah 1:17–2:10',
            second: 'Acts 27:9–26',
            gospel: 'Luke 9:1–17'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Jonah 3:1–4:11',
            second: 'Acts 27:27–44',
            gospel: 'Luke 9:18–27'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Sir 1:1–10, 18–27',
            second: 'Acts 28:1–16',
            gospel: 'Luke 9:28–36'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Sir 3:17–31',
            second: 'Acts 28:17–31',
            gospel: 'Luke 9:37–50'
          }
        }
      ]
    }
  ]
}
