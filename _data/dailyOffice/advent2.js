/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 2 Advent
 * @module _data/dailyOffice/advent2
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Advent',
    week: 'Week of the Second Sunday of Advent',
    collects: {
      contemporary: [
        {
          text: 'Merciful God, who sent your messengers the prophets to preach repentance and prepare the way for our salvation: Give us grace to heed their warnings and forsake our sins, that we may greet with joy the coming of Jesus Christ our Redeemer; who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 211
        }
      ],
      traditional: [
        {
          text: 'Merciful God, who sent thy messengers the prophets to preach repentance and prepare the way for our salvation: Give us grace to heed their warnings and forsake our sins, that we may greet with joy the coming of Jesus Christ our Redeemer; who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 159
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 936
      },
      days: [
        {
          day: 'Sunday',
          title: 'Second Sunday of Advent',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Isa 5:1–7',
            second: '2 Pet 3:11–18',
            gospel: 'Luke 7:28–35'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Isa 5:8–12, 18–23',
            second: '1 Thess 5:1–11',
            gospel: 'Luke 21:20–28'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Isa 5:13–17, 24–25',
            second: '1 Thess 5:12–28',
            gospel: 'Luke 21:29–38'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Isa 6:1–13',
            second: '2 Thess 1:1–12',
            gospel: 'John 7:53–8:11'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Isa 7:1–9',
            second: '2 Thess 2:1–12',
            gospel: 'Luke 22:1–13'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Isa 7:10–25',
            second: '2 Thess 2:13–3:5',
            gospel: 'Luke 22:14–30'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Isa 8:1–15',
            second: '2 Thess 3:6–18',
            gospel: 'Luke 22:31–38'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 937
      },
      days: [
        {
          day: 'Sunday',
          title: 'Second Sunday of Advent',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Amos 6:1-14',
            second: '2 Thess 1:5–12',
            gospel: 'Luke 1:57–68'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Amos 7:1–9',
            second: 'Rev 1:1–8',
            gospel: 'Matt 22:23–33'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Amos 7:10–17',
            second: 'Rev 1:9–16',
            gospel: 'Matt 22:34–46'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Amos 8:1–14',
            second: 'Rev 1:17–2:7',
            gospel: 'Matt 23:1–12'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Amos 9:1–10',
            second: 'Rev 2:8–17',
            gospel: 'Matt 23:13–26'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Hag 1:1–15',
            second: 'Rev 2:18–29',
            gospel: 'Matt 23:27–39'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Hag 2:1–9',
            second: 'Rev 3:1–6',
            gospel: 'Matt 24:1–14'
          }
        }
      ]
    }
  ]
}
