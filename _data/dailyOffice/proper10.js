/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 10
 * @module _data/dailyOffice/proper10
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 10',
    collects: {
      contemporary: [
        {
          text: 'O Lord, mercifully receive the prayers of your people who call upon you, and grant that they may know and understand what things they ought to do, and also may have grace and power faithfully to accomplish them; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 231
        }
      ],
      traditional: [
        {
          text: 'O Lord, we beseech thee mercifully to receive the prayers of thy people who call upon thee, and grant that they may both perceive and know what things they ought to do, and also may have grace and power faithfully to fulfill the same; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 179
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 974
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115',
              '117'
            ]
          },
          lessons: {
            first: '1 Sam 17:50–18:4',
            second: 'Rom 10:4–17',
            gospel: 'Matt 23:29–39'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: '1 Sam 18:5–16, 27b–30',
            second: 'Acts 11:19–30',
            gospel: 'Mark 1:29–45'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: '1 Sam 19:1–18',
            second: 'Acts 12:1–17',
            gospel: 'Mark 2:1–12'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: '1 Sam 20:1–23',
            second: 'Acts 12:18–25',
            gospel: 'Mark 2:13–22'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: '1 Sam 20:24–42',
            second: 'Acts 13:1–12',
            gospel: 'Mark 2:23–3:6'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: '1 Sam 21:1–15',
            second: 'Acts 13:13–25',
            gospel: 'Mark 3:7–19a'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: '1 Sam 22:1–23',
            second: 'Acts 13:16–43',
            gospel: 'Mark 3:19b–35'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 975
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115',
              '117'
            ]
          },
          lessons: {
            first: 'Josh 1:1–18',
            second: 'Acts 21:3–15',
            gospel: 'Mark 1:21–27'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Josh 2:1–14',
            second: 'Rom 11:1–12',
            gospel: 'Matt 25:1–13'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Josh 2:15–24',
            second: 'Rom 11:13–24',
            gospel: 'Matt 25:14–30'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Josh 3:1–13',
            second: 'Rom 11:25–36',
            gospel: 'Matt 25:31–46'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Josh 3:14–4:7',
            second: 'Rom 12:1–8',
            gospel: 'Matt 26:1–16'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Josh 4:19–5:1, 10–15',
            second: 'Rom 12:9–21',
            gospel: 'Matt 26:17–25'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Josh 6:1–14',
            second: 'Rom 13:1–7',
            gospel: 'Matt 26:26–35'
          }
        }
      ]
    }
  ]
}
