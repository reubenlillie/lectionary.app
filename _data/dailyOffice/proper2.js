/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 2
 * @module _data/dailyOffice/proper2
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 2',
    collects: {
      contemporary: [
        {
          text: 'Almighty and merciful God, in your goodness keep us, we pray, from all things that may hurt us, that we, being ready both in mind and body, may accomplish with free hearts those things which belong to your purpose; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 228
        }
      ],
      traditional: [
        {
          text: 'O Almighty and most merciful God, of thy bountiful goodness keep us, we beseech thee, from all things that may hurt us, that we, being ready both in body and soul, may with free hearts accomplish those things which belong to thy purpose; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 177
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 966
      },
      days: [
        {
          day: 'Monday',
          psalms: {
            morning: [
              1,
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Ruth 1:1–18',
            second: '1 Tim 1:1–17',
            gospel: 'Luke 13:1–9'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Ruth 1:19–2:13',
            second: '1 Tim 1:18–2:8',
            gospel: 'Luke 13:10–17'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Ruth 2:14–23',
            second: '1 Tim 3:1–16',
            gospel: 'Luke 13:18–30'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Ruth 3:1–18',
            second: '1 Tim 4:1–16',
            gospel: 'Luke 13:31–35'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Ruth 4:1–17',
            second: '1 Tim 5:17–22(23–25)',
            gospel: 'Luke 14:1–11'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Deut 1:1–8',
            second: '1 Tim 6:6–21',
            gospel: 'Luke 14:12–24'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 967
      },
      days: [
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Prov 3:11–20',
            second: '1 John 3:18–4:6',
            gospel: 'Matt 11:1–6'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Prov 4:1–27',
            second: '1 John 4:7–21',
            gospel: 'Matt 11:7–15'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Prov 6:1–19',
            second: '1 John 5:1–12',
            gospel: 'Matt 11:16–24'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Prov 7:1–27',
            second: '1 John 5:13–21',
            gospel: 'Matt 11:25–30'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Prov 8:1–21',
            second: '2 John 1–13',
            gospel: 'Matt 12:1–14'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Prov 8:22–36',
            second: '3 John 1–15',
            gospel: 'Matt 12:15–21'
          }
        }
      ]
    }
  ]
}
