/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 13
 * @module _data/dailyOffice/proper13
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 13',
    collects: {
      contemporary: [
        {
          text: 'Let your continual mercy, O Lord, cleanse and defend your Church; and, because it cannot continue in safety without your help, protect and govern it always by your goodness; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 232
        }
      ],
      traditional: [
        {
          text: 'O Lord, we beseech thee, let thy continual pity cleanse and defend thy Church, and, because it cannot continue in safety without thy succor, preserve it evermore by thy help and goodness; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 180
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 978
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: '2 Sam 6:12–23',
            second: 'Rom 14:7–12',
            gospel: 'John 1:43–51'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: '2 Sam 7:1–17',
            second: 'Acts 18:1–11',
            gospel: 'Mark 8:11–21'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: '2 Sam 7:18–29',
            second: 'Acts 18:12–28',
            gospel: 'Mark 8:22–33'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: '2 Sam 9:1–13',
            second: 'Acts 19:1–10',
            gospel: 'Mark 8:34–9:1'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '145'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: '2 Sam 11:1–27',
            second: 'Acts 19:11–20',
            gospel: 'Mark 9:2–13'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: '2 Sam 12:1–14',
            second: 'Acts 19:21–41',
            gospel: 'Mark 9:14–29'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: '2 Sam 12:15–31',
            second: 'Acts 20:1–16',
            gospel: 'Mark 9:30–41'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 979
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Judg 6:1–24',
            second: '2 Cor 9:6–15',
            gospel: 'Mark 3:20–30'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Judg 6:25–40',
            second: 'Acts 37–47',
            gospel: 'John 1:1–18'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Judg 7:1–18',
            second: 'Acts 3:1–11',
            gospel: 'John 1:19–28'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Judg 7:19–8:12',
            second: 'Acts 3:12–26',
            gospel: 'John 1:29–42'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '145'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: 'Judg 8:22–35',
            second: 'Acts 4:1–12',
            gospel: 'John 1:43–51'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Judg 9:1–16, 19–21',
            second: 'Acts 4:13–31',
            gospel: 'John 2:1–12'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Judg 9:22–25, 50–57',
            second: 'Acts 4:32–5:11',
            gospel: 'John 2:13–25'
          }
        }
      ]
    }
  ]
}
