/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 24
 * @module _data/dailyOffice/proper24
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 24',
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, in Christ you have revealed your glory among the nations: Preserve the works of your mercy, that your Church throughout the world may persevere with steadfast faith in the confession of your Name; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 235
        }
      ],
      traditional: [
        {
          text: 'Almighty and everlasting God, who in Christ hast revealed thy glory among the nations: Preserve the works of thy mercy, that thy Church throughout the world may persevere with steadfast faith in the confession of thy Name; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 183
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 988
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Jer 29:1, 4–14',
            second: 'Acts 16:6–15',
            gospel: 'Luke 10:1–12, 17–20'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Jer 44:1–14',
            second: '1 Cor 15:30–41',
            gospel: 'Matt 11:16–24'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Lam 1:1–5(6–9)10–12',
            second: '1 Cor 15:41–50',
            gospel: 'Matt 11:25–30'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Lam 2:8–15',
            second: '1 Cor 15:51–58',
            gospel: 'Matt 12:1–14'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Ezra 1:1–11',
            second: '1 Cor 16:1–9',
            gospel: 'Matt 12:15–21'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Ezra 3:1–13',
            second: '1 Cor 16:10–24',
            gospel: 'Matt 12:22–32'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Ezra 4:7, 11–24',
            second: 'Phlm 1–24',
            gospel: 'Matt 12:33–42'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 989
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Sir 4:1–10',
            second: '1 Cor 10:1–13',
            gospel: 'Matt 16:13–20'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Sir 4:20–5:7',
            second: 'Rev 7:1–8',
            gospel: 'Luke 9:51–62'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Sir 6:5–17',
            second: 'Rev 7:9–17',
            gospel: 'Luke 10:1–16'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Sir 7:4–14',
            second: 'Rev 3:1–13',
            gospel: 'Luke 10:17–24'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Sir 10:1–18',
            second: 'Rev 9:1–12',
            gospel: 'Luke 10:25–37'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Sir 11:2–20',
            second: 'Rev 9:13–21',
            gospel: 'Luke 10:38–42'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Sir 15:9–20',
            second: 'Rev 10:1–11',
            gospel: 'Luke 11:1–13'
          }
        }
      ]
    }
  ]
}
