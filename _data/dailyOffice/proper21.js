/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 21
 * @module _data/dailyOffice/proper21
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 21',
    collects: {
      contemporary: [
        {
          text: 'O God, you declare your almighty power chiefly in showing mercy and pity: Grant us the fullness of your grace, that we, running to obtain your promises, may become partakers of your heavenly treasure; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 234
        }
      ],
      traditional: [
        {
          text: 'O God, who declarest thy almighty power chiefly in showing mercy and pity: Mercifully grant unto us such a measure of thy grace, that we, running to obtain thy promises, may be made partakers of thy heavenly treasure; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 182
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 986
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: '2 Kgs 17:1–18',
            second: 'Acts 9:36–43',
            gospel: 'Luke 5:1–11'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: '2 Kgs 17:24–41',
            second: '1 Cor 7:25–31',
            gospel: 'Matt 6:25–34'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: '2 Chr 29:1–3; 30:1(2–9)10–27',
            second: '1 Cor 7:32–40',
            gospel: 'Matt 7:1–12'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: '2 Kgs 18:9–25',
            second: '1 Cor 8:1–13',
            gospel: 'Matt 7:13–21'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: '2 Kgs 18:28–37',
            second: '1 Cor 9:1–15',
            gospel: 'Matt 7:22–29'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: '2 Kgs 19:1–20',
            second: '1 Cor 9:16–27',
            gospel: 'Matt 8:1–17'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: '2 Kgs 19:21–36',
            second: '1 Cor 10:1–13',
            gospel: 'Matt 8:18–27'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 987
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Hos 2:2–14',
            second: 'Jas 3:1–13',
            gospel: 'Matt 13:44–52'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Hos 2:14–23',
            second: 'Acts 20:17–38',
            gospel: 'Luke 5:1–11'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Hos 4:1–10',
            second: 'Acts 21:1–14',
            gospel: 'Luke 5:12–26'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: 'Hos 4:11–19',
            second: 'Acts 21:15–26',
            gospel: 'Luke 5:27–39'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Hos 5:8–6:6',
            second: 'Acts 21:27–36',
            gospel: 'Luke 6:1–11'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Hos 10:1–15',
            second: 'Acts 21:37–22:16',
            gospel: 'Luke 6:12–26'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Hos 11:1–9',
            second: 'Acts 22:17–29',
            gospel: 'Luke 6:27–38'
          }
        }
      ]
    }
  ]
}
