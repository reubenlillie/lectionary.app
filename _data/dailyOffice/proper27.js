/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 27
 * @module _data/dailyOffice/proper27
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 27',
    collects: {
      contemporary: [
        {
          text: 'O God, whose blessed Son came into the world that he might destroy the works of the devil and make us children of God and heirs of eternal life: Grant that, having this hope, we may purify ourselves as he is pure; that, when he comes again with power and great glory, we may be made like him in his eternal and glorious kingdom; where he lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 236
        }
      ],
      traditional: [
        {
          text: 'O God, whose blessed Son was manifested that he might destroy the works of the devil and make us the children of God and heirs of eternal life: Grant us, we beseech thee, that, having this hope, we may purify ourselves even as he is pure; that, when he shall appear again with power and great glory, we may be made like unto him in his eternal and glorious kingdom; where with thee, O Father, and thee, O Holy Ghost, he liveth and reigneth ever, one God, world without end.',
          bcpPage: 184
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 992
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Ezra 10:1–17',
            second: 'Acts 24:10–21',
            gospel: 'Luke 14:12–24'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Neh 9:1–15(16–25)',
            second: 'Rev 18:1–8',
            gospel: 'Matt 15:1–20'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Neh 9:26–38',
            second: 'Rev 18:9–20',
            gospel: 'Matt 15:21–28'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Neh 7:73b–8:3, 5–18',
            second: 'Rev 18:21–24',
            gospel: 'Matt 15:29–39'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '23',
              '27'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: '1 Macc 1:1–28',
            second: 'Rev 19:1–10',
            gospel: 'Matt 16:1–12'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: '1 Macc 1:41–63',
            second: 'Rev 19:11–16',
            gospel: 'Matt 16:1–12'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: '1 Macc 2:1–28',
            second: 'Rev 20:1–6',
            gospel: 'Matt 16:21–28'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 993
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Sir 51:13–22',
            second: '1 Cor 14:1–12',
            gospel: 'Matt 20:1–16'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Joel 1:1–13',
            second: 'Rev 18:15–24',
            gospel: 'Luke 14:12–24'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Joel 1:15–2:2(3–11)',
            second: 'Rev 19:1–10',
            gospel: 'Luke 14:25–35'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '81',
              '82'
            ]
          },
          lessons: {
            first: 'Joel 2:12–19',
            second: 'Rev 19:11–21',
            gospel: 'Luke 15:1–10'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[83]',
              '23',
              '27'
            ],
            evening: [
              '85',
              '86'
            ]
          },
          lessons: {
            first: 'Joel 2:21–27',
            second: 'Jas 1:1–15',
            gospel: 'Luke 15:1–2, 11–32'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '88'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Joel 2:28–3:8',
            second: 'Jas 1:16–27',
            gospel: 'Luke 16:1–9'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Joel 3:9–17',
            second: 'Jas 2:1–13',
            gospel: 'Luke 16:10–17(18)'
          }
        }
      ]
    }
  ]
}
