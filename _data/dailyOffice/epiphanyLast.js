/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of Last Epiphany
 * @module _data/dailyOffice/epiphanyLast
 * @since 0.1.0
 * @since 0.11.0 Add `color` properties
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the Last Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'O God, who before the passion of your only-begotten Son revealed his glory upon the holy mountain: Grant to us that we, beholding by faith the light of his countenance, may be strengthened to bear our cross, and be changed into his likeness from glory to glory; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever. ',
          bcpPage: 217
        }
      ],
      traditional: [
        {
          text: 'O God, who before the passion of thy only-begotten Son didst reveal his glory upon the holy mount: Grant unto us that we, beholding by faith the light of his countenance, may be strengthened to bear our cross, and be changed into his likeness from glory to glory; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 165
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 950
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'Last Sunday after the Epiphany: The Transfiguration of Christ',
            main: 'Transfiguration Sunday',
            sub: 'Last Sunday after the Epiphany'
          },
          color: 'white',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Deut 6:1–9',
            second: 'Heb 12:18–29',
            gospel: 'John 12:24–32'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Deut 6:10–15',
            second: 'Heb 1:1–14',
            gospel: 'John 1:1–18'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Deut 6:16–25',
            second: 'Heb 2:1–10',
            gospel: 'John 1:19–28'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 951
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'Last Sunday after the Epiphany: The Transfiguration of Christ',
            main: 'Transfiguration Sunday',
            sub: 'Last Sunday after the Epiphany'
          },
          color: 'white',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Sir 48:1–11',
            second: '2 Cor 3:7–18',
            gospel: 'Luke 9:18–27'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Prov 27:1–6, 10–12',
            second: 'Phil 2:1–13',
            gospel: 'John 18:15–18, 25–27'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Prov 30:1–4, 24–33',
            second: 'Phil 3:1–11',
            gospel: 'John 18:28–38'
          }
        }
      ]
    }
  ]
}
