/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Christmas and Following
 * @module _data/dailyOffice/christmas
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Christmas',
    week: 'Christmas Day and Following',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, you have given your only-begotten Son to take our nature upon him, and to be born of a pure virgin: Grant that we, who have been born again and made your children by adoption and grace, may daily be renewed by your Holy Spirit; through our Lord Jesus Christ, to whom with you and the same Spirit be honor and glory, now and for ever.',
          bcpPage: 213
        }
      ],
      traditional: [
        {
          text: 'Almighty God, who hast given us thy only-begotten Son to take our nature upon him and as at this time to be born of a pure virgin: Grant that we, being regenerate and made thy children by adoption and grace, may daily be renewed by thy Holy Spirit; through the same our Lord Jesus Christ, who liveth and reigneth with thee and the same Spirit ever, one God, world without end.',
          bcpPage: 161
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 940
      },
      days: [
        {
          day: 'Dec 25',
          title: {
            full: 'The Nativity of Our Lord Jesus Christ: Christmas Day',
            main: 'Christmas Day',
            sub: 'The Nativity of Our Lord Jesus Christ'
          },
          psalms: {
            morning: [
              '2',
              '85'
            ],
            evening: [
              '110:1–5(6–7)',
              '132'
            ]
          },
          lessons: {
            first: 'Zech 2:10–13',
            second: '1 John 4:7–16',
            gospel: 'John 3:31–36'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, you make us glad by the yearly festival of the birth of your only Son Jesus Christ: Grant that we, who joyfully receive him as our Redeemer, may with sure confidence behold him when he comes to be our Judge; who lives and reigns with you and the Holy Spirit, one God, now and for ever',
                bcpPage: 212
              },
              {
                text: 'O God, you have caused this holy night to shine with the brightness of the true Light: Grant that we, who have known the mystery of that Light on earth, may also enjoy him perfectly in heaven; where with you and the Holy Spirit he lives and reigns, one God, in glory everlasting.',
                bcpPage: 212
              },
              {
                text: 'Almighty God, you have given your only-begotten Son to take our nature upon him, and to be born of a pure virgin: Grant that we, who have been born again and made your children by adoption and grace, may daily be renewed by your Holy Spirit; through our Lord Jesus Christ, to whom with you and the same Spirit be honor and glory, now and for ever.',
                bcpPage: 213
              }
            ],
            traditional: [
              {
                text: 'O God, who makest us glad with the yearly remembrance of the birth of thy only Son Jesus Christ: Grant that as we joyfully receive him for our Redeemer, so we may with sure confidence behold him when he shall come to be our Judge; who liveth and reigneth with thee and the Holy Ghost, one God, world without end.',
                bcpPage: 160
              },
              {
                text: 'O God, who hast caused this holy night to shine with the illumination of the true Light: Grant us, we beseech thee, that as we have known the mystery of that Light upon earth, so may we also perfectly enjoy him in heaven; where with thee and the Holy Spirit he liveth and reigneth, one God, in glory everlasting. ',
                bcpPage: 161
              },
              {
                text: 'Almighty God, who hast given us thy only-begotten Son to take our nature upon him and as at this time to be born of a pure virgin: Grant that we, being regenerate and made thy children by adoption and grace, may daily be renewed by thy Holy Spirit; through the same our Lord Jesus Christ, who liveth and reigneth with thee and the same Spirit ever, one God, world without end.',
                bcpPage: 161
              }
            ]
          }
        },
        {
          day: 'Sunday',
          title: 'First Sunday after Christmas',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '36'
            ]
          },
          lessons: {
            first: 'Isa 62:6–7, 10–12',
            second: 'Heb 2:10–18',
            gospel: 'Matt 1:18–25'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty God, you have poured upon us the new light of your incarnate Word: Grant that this light, enkindled in our hearts, may shine forth in our lives; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
               bcpPage: 213
              }
            ],
            traditional: [
              {
                text: 'Almighty God, who hast poured upon us the new light of thine incarnate Word: Grant that the same light, enkindled in our hearts, may shine forth in our lives; through the same Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
                bcpPage: 161
              }
            ]
          }
        },
        {
          day: 'Dec 29',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Isa 12:1–6',
            second: 'Rev 1:1–8',
            gospel: 'John 7:37–52'
          },
          notes: [
            'If today is Saturday, use Psalms 23 and 27 at Evening Prayer.'
          ]
        },
        {
          day: 'Dec 30',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Isa 25:1–9',
            second: 'Rev 1:9–20',
            gospel: 'John 7:53–8:11'
          }
        },
        {
          day: 'Dec 31',
          title: 'Eve of Holy Name',
          psalms: {
            morning: [
              '46',
              '48'
            ],
            evening: [
              '90'
            ]
          },
          lessons: {
            morning: {
              first: 'Isa 26:1–9',
              second: '2 Cor 5:16–6:2',
              gospel: 'John 8:12–19'
            },
            evening: {
              first: 'Isa 65:15b–25',
              second: 'Rev 21:1–6'
            }
          }
        },
        {
          day: 'Jan 1',
          title: {
            full: 'The Holy Name of Our Lord Jesus Christ',
            main: 'Holy Name',
            sub: 'of Our Lord Jesus Christ'
          },
          psalms: {
            morning: [
              '103'
            ],
            evening: [
              '148'
            ]
          },
          lessons: {
            first: 'Gen 17:1–12a, 15–16',
            second: 'Col 2:6–12',
            gospel: 'John 16:23b–30'
          },
          collects: {
            contemporary: [
              {
                text: 'Eternal Father, you gave to your incarnate Son the holy name of Jesus to be the sign of our salvation: Plant in every heart, we pray, the love of him who is the Savior of the world, our Lord Jesus Christ; who lives and reigns with you and the Holy Spirit, one God, in glory everlasting.',
               bcpPage: 213
              }
            ],
            traditional: [
              {
                text: 'Eternal Father, who didst give to thine incarnate Son the holy name of Jesus to be the sign of our salvation: Plant in every heart, we beseech thee, the love of him who is the Savior of the world, even our Lord Jesus Christ; who liveth and reigneth with thee and the Holy Spirit, one God, in glory everlasting.',
                bcpPage: 162
              }
            ]
          }
        },
        {
          day: 'Sunday',
          title: 'Second Sunday after Christmas',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Sir 3:3–9, 14–17',
            second: '1 John 2:12–17',
            gospel: 'John 6:41–47'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, who wonderfully created, and yet more wonderfully restored, the dignity of human nature: Grant that we may share the divine life of him who humbled himself to share our humanity, your Son Jesus Christ; who lives and reigns with you, in the unity of the Holy Spirit, one God, for ever and ever.',
               bcpPage: 214
              }
            ],
            traditional: [
              {
                text: 'O God, who didst wonderfully create, and yet more wonderfully restore, the dignity of human nature: Grant that we may share the divine life of him who humbled himself to share our humanity, thy Son Jesus Christ; who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, for ever and ever.',
                bcpPage: 162
              }
            ]
          }
        },
        {
          day: 'Jan 2',
          psalms: {
            morning: [
              '34'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Gen 12:1–7',
            second: 'Heb 11:1–12',
            gospel: 'John 6:35–41, 48–51'
          }
        },
        {
          day: 'Jan 3',
          psalms: {
            morning: [
              '68'
            ],
            evening: [
              '72'
            ]
          },
          lessons: {
            first: 'Gen 28:10–22',
            second: 'Heb 11:13–22',
            gospel: 'John 10:7–17'
          },
          notes: [
            'If today is Saturday, use Psalm 136 at Evening Prayer.'
          ]
        },
        {
          day: 'Jan 4',
          psalms: {
            morning: [
              '85',
              '87'
            ],
            evening: [
              '89:1–29'
            ]
          },
          lessons: {
            first: 'Exod 3:1–12',
            second: 'Heb 11:23–31',
            gospel: 'John 14:6–14'
          },
          notes: [
            'If today is Saturday, use Psalm 136 at Evening Prayer.'
          ]
        },
        {
          day: 'Jan 5',
          title: 'Eve of Epiphany',
          psalms: {
            morning: [
              '2',
              '110:1–5(6–7)'
            ],
            evening: [
              '29',
              '98'
            ]
          },
          lessons: {
            morning: {
              first: 'Josh 1:1–9',
              second: 'Heb 11:32–12:2',
              gospel: 'John 15:1–16'
            },
            evening: {
              first: 'Isa 66:18–23',
              second: 'Rom 15:7–13'
            }
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 941
      },
      days: [
        {
          day: 'Dec 25',
          title: {
            full: 'The Nativity of Our Lord Jesus Christ: Christmas Day',
            main: 'Christmas Day',
            sub: 'The Nativity of Our Lord Jesus Christ'
          },
          psalms: {
            morning: [
              '2',
              '85'
            ],
            evening: [
              '110:1–5(6–7)',
              '132'
            ]
          },
          lessons: {
            first: 'Mic 4:1–5; 5:2–4',
            second: '1 John 4:7–16',
            gospel: 'John 3:31–36'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, you make us glad by the yearly festival of the birth of your only Son Jesus Christ: Grant that we, who joyfully receive him as our Redeemer, may with sure confidence behold him when he comes to be our Judge; who lives and reigns with you and the Holy Spirit, one God, now and for ever',
               bcpPage: 212
              },
              {
                text: 'O God, you have caused this holy night to shine with the brightness of the true Light: Grant that we, who have known the mystery of that Light on earth, may also enjoy him perfectly in heaven; where with you and the Holy Spirit he lives and reigns, one God, in glory everlasting.',
               bcpPage: 212
              },
              {
                text: 'Almighty God, you have given your only-begotten Son to take our nature upon him, and to be born [this day] of a pure virgin: Grant that we, who have been born again and made your children by adoption and grace, may daily be renewed by your Holy Spirit; through our Lord Jesus Christ, to whom with you and the same Spirit be honor and glory, now and for ever.',
               bcpPage: 213
              }
            ],
            traditional: [
              {
                text: 'O God, who makest us glad with the yearly remembrance of the birth of thy only Son Jesus Christ: Grant that as we joyfully receive him for our Redeemer, so we may with sure confidence behold him when he shall come to be our Judge; who liveth and reigneth with thee and the Holy Ghost, one God, world without end.',
                bcpPage: 160
              },
              {
                text: 'O God, who hast caused this holy night to shine with the illumination of the true Light: Grant us, we beseech thee, that as we have known the mystery of that Light upon earth, so may we also perfectly enjoy him in heaven; where with thee and the Holy Spirit he liveth and reigneth, one God, in glory everlasting. ',
                bcpPage: 161
              },
              {
                text: 'Almighty God, who hast given us thy only-begotten Son to take our nature upon him and as at this time to be born of a pure virgin: Grant that we, being regenerate and made thy children by adoption and grace, may daily be renewed by thy Holy Spirit; through the same our Lord Jesus Christ, who liveth and reigneth with thee and the same Spirit ever, one God, world without end.',
               bcpPage: 161
              }
            ]
          }
        },
        {
          day: 'Sunday',
          title: 'First Sunday after Christmas',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '36'
            ]
          },
          lessons: {
            first: '1 Sam 1:1–2, 7b–28',
            second: 'Col 1:9–20',
            gospel: 'Luke 2:22–40'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty God, you have poured upon us the new light of your incarnate Word: Grant that this light, enkindled in our hearts, may shine forth in our lives; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
               bcpPage: 213
              }
            ],
            traditional: [
              {
                text: 'Almighty God, who hast poured upon us the new light of thine incarnate Word: Grant that the same light, enkindled in our hearts, may shine forth in our lives; through the same Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
                bcpPage: 161
              }
            ]
          }
        },
        {
          day: 'Dec 29',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: '2 Sam 23:13–17b',
            second: '2 John 1–13',
            gospel: 'John 2:1–11'
          },
          notes: [
            'If today is Saturday, use Psalms 23 and 27 at Evening Prayer.'
          ]
        },
        {
          day: 'Dec 30',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: '1 Kgs 17:17–24',
            second: '3 John 1–15',
            gospel: 'John 4:46–54'
          }
        },
        {
          day: 'Dec 31',
          title: 'Eve of Holy Name',
          psalms: {
            morning: [
              '46',
              '48'
            ],
            evening: [
              '90'
            ]
          },
          lessons: {
            morning: {
              first: '1 Kgs 3:5–14',
              second: 'Jas 4:13–17; 5:7–11',
              gospel: 'John 5:1–15'
            },
            evening: {
              first: 'Isa 65:15b–25',
              second: 'Rev 21:1–6'
            }
          }
        },
        {
          day: 'Jan 1',
          title: {
            full: 'The Holy Name of Our Lord Jesus Christ',
            main: 'Holy Name',
            sub: 'of Our Lord Jesus Christ'
          },
          psalms: {
            morning: [
              '103'
            ],
            evening: [
              '148'
            ]
          },
          lessons: {
            first: 'Isa 62:1–5, 10–12',
            second: 'Rev 19:11–16',
            gospel: 'Matt 1:18–25'
          },
          collects: {
            contemporary: [
              {
                text: 'Eternal Father, you gave to your incarnate Son the holy name of Jesus to be the sign of our salvation: Plant in every heart, we pray, the love of him who is the Savior of the world, our Lord Jesus Christ; who lives and reigns with you and the Holy Spirit, one God, in glory everlasting.',
               bcpPage: 213
              }
            ],
            traditional: [
              {
                text: 'Eternal Father, who didst give to thine incarnate Son the holy name of Jesus to be the sign of our salvation: Plant in every heart, we beseech thee, the love of him who is the Savior of the world, even our Lord Jesus Christ; who liveth and reigneth with thee and the Holy Spirit, one God, in glory everlasting.',
                bcpPage: 162
              }
            ]
          }
        },
        {
          day: 'Sunday',
          title: 'Second Sunday after Christmas',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '145'
            ]
          },
          lessons: {
            first: 'Wis 7:3–14',
            second: 'Col 3:12–17',
            gospel: 'John 6:41–47'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, who wonderfully created, and yet more wonderfully restored, the dignity of human nature: Grant that we may share the divine life of him who humbled himself to share our humanity, your Son Jesus Christ; who lives and reigns with you, in the unity of the Holy Spirit, one God, for ever and ever.',
               bcpPage: 214
              }
            ],
            traditional: [
              {
                text: 'O God, who didst wonderfully create, and yet more wonderfully restore, the dignity of human nature: Grant that we may share the divine life of him who humbled himself to share our humanity, thy Son Jesus Christ; who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, for ever and ever.',
                bcpPage: 162
              }
            ]
          }
        },
        {
          day: 'Jan 2',
          psalms: {
            morning: [
              '34'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: '1 Kgs 19:1–8',
            second: 'Eph 4:1–16',
            gospel: 'John 6:1–14'
          }
        },
        {
          day: 'Jan 3',
          psalms: {
            morning: [
              '68'
            ],
            evening: [
              '72'
            ]
          },
          lessons: {
            first: '1 Kgs 19:9–18',
            second: 'Eph 4:17–32',
            gospel: 'John 6:15–27'
          },
          notes: [
            'If today is Saturday, use Psalm 136 at Evening Prayer.'
          ]
        },
        {
          day: 'Jan 4',
          psalms: {
            morning: [
              '85',
              '87'
            ],
            evening: [
              '89:1–29'
            ]
          },
          lessons: {
            first: 'Josh 3:14–4:7',
            second: 'Eph 5:1–20',
            gospel: 'John 9:1–12, 35–38'
          },
          notes: [
            'If today is Saturday, use Psalm 136 at Evening Prayer.'
          ]
        },
        {
          day: 'Jan 5',
          title: 'Eve of Epiphany',
          psalms: {
            morning: [
              '2',
              '110:1–5(6–7)'
            ],
            evening: [
              '29',
              '98'
            ]
          },
          lessons: {
            morning: {
              first: 'Jonah 2:2–9',
              second: 'Eph 6:10–20',
              gospel: 'John 11:17–27, 38–44'
            },
            evening: {
              first: 'Isa 66:18–23',
              second: 'Rom 15:7–13'
            }
          }
        }
      ]
    }
  ]
}
