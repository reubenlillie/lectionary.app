/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 2 Lent
 * @module _data/dailyOffice/lent2
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Lent',
    week: 'Week of the Second Sunday in Lent',
    collects: {
      contemporary: [
        {
          text: 'O God, whose glory it is always to have mercy: Be gracious to all who have gone astray from your ways, and bring them again with penitent hearts and steadfast faith to embrace and hold fast the unchangeable truth of your Word, Jesus Christ your Son; who with you and the Holy Spirit lives and reigns, one God, for ever and ever.',
          bcpPage: 218
        }
      ],
      traditional: [
        {
          text: 'O God, whose glory it is always to have mercy: Be gracious to all who have gone astray from thy ways, and bring them again with penitent hearts and steadfast faith to embrace and hold fast the unchangeable truth of thy Word, Jesus Christ thy Son; who with thee and the Holy Spirit liveth and reigneth, one God, for ever and ever.',
          bcpPage: 166
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 952
      },
      days: [
        {
          day: 'Sunday',
          title: 'Second Sunday in Lent',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Jer 1:1–10',
            second: '1 Cor 3:11–23',
            gospel: 'Mark 3:31–4:9'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Jer 1:11–19',
            second: 'Rom 1:1–15',
            gospel: 'John 4:27–42'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Jer 2:1–13',
            second: 'Rom 1:16–25',
            gospel: 'John 4:43–54'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Jer 3:6–18',
            second: 'Rom 1:28–2:11',
            gospel: 'John 5:1–18'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Jer 4:9–10, 19–28',
            second: 'Rom 2:12–24',
            gospel: 'John 5:19–29'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Jer 5:1–9',
            second: 'Rom 2:25–3:18',
            gospel: 'John 5:30–47'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Jer 5:20–31',
            second: 'Rom 3:19–31',
            gospel: 'John 7:1–13'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 953
      },
      days: [
        {
          day: 'Sunday',
          title: 'Second Sunday in Lent',
          psalms: {
            morning: [
              '24',
              '29'
            ],
            evening: [
              '8',
              '84'
            ]
          },
          lessons: {
            first: 'Gen 41:14–45',
            second: 'Rom 6:3–14',
            gospel: 'John 5:19–24'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '56',
              '57',
              '[58]'
            ],
            evening: [
              '64',
              '65'
            ]
          },
          lessons: {
            first: 'Gen 41:46–57',
            second: '1 Cor 4:8–20(21)',
            gospel: 'Mark 3:7–19a'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '61',
              '62'
            ],
            evening: [
              '68:1–20(21–23)24–36'
            ]
          },
          lessons: {
            first: 'Gen 42:1–17',
            second: '1 Cor5:1–8',
            gospel: 'Mark 3:19b–35'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '72'
            ],
            evening: [
              '119:73–96'
            ]
          },
          lessons: {
            first: 'Gen 42:18–28',
            second: '1 Cor 5:9–6:8',
            gospel: 'Mark 4:1–20'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '[70]',
              '71'
            ],
            evening: [
              '74'
            ]
          },
          lessons: {
            first: 'Gen 42:29–38',
            second: '1 Cor 6:12–20',
            gospel: 'Mark 4:21–34'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '95',
              '69:1–23(24–30)31–38'
            ],
            evening: [
              '73'
            ]
          },
          lessons: {
            first: 'Gen 43:1–15',
            second: '1 Cor 7:1–9',
            gospel: 'Mark 4:35–41'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '75',
              '76'
            ],
            evening: [
              '23',
              '27'
            ]
          },
          lessons: {
            first: 'Gen 43:16–34',
            second: '1 Cor 7:10–24',
            gospel: 'Mark 5:1–20'
          }
        }
      ]
    }
  ]
}
