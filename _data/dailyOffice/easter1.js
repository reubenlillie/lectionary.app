/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Easter Week
 * @module _data/dailyOffice/easter1
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Easter',
    week: 'Easter Week',
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 958
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'The Sunday of the Resurrection, or Easter Day',
            main: 'Easter Day',
            sub: 'Sunday of the Resurrection'
          },
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '113',
              '114',
              '118'
            ]
          },
          lessons: {
            morning: {
              first: 'Exod 12:1–14',
              gospel: 'John 1:1–18'
            },
            evening: {
              first: 'Isa 51:9–11',
              gospel: 'Luke 24:13–35',
              altGospel: 'John 20:19–23'
            }
          },
          collects: {
            contemporary: [
              {
                text: 'O God, who for our redemption gave your only-begotten Son to the death of the cross, and by his glorious resurrection delivered us from the power of our enemy: Grant us so to die daily to sin, that we may evermore live with him in the joy of his resurrection; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
                bcpPage: 222
              },
              {
                text: 'O God, who made this most holy night to shine with the glory of the Lord’s resurrection: Stir up in your Church that Spirit of adoption which is given to us in Baptism, that we, being renewed both in body and mind, may worship you in sincerity and truth; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
                bcpPage: 222
              },
              {
                text: 'Almighty God, who through your only-begotten Son Jesus Christ overcame death and opened to us the gate of everlasting life: Grant that we, who celebrate with joy the day of the Lord’s resurrection, may be raised from the death of sin by your life-giving Spirit; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
                bcpPage: 222
              }
            ],
            traditional: [
              {
                text: 'O God, who for our redemption didst give thine only-begotten Son to the death of the cross, and by his glorious resurrection hast delivered us from the power of our enemy: Grant us so to die daily to sin, that we may evermore live with him in the joy of his resurrection; through the same thy Son Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
                bcpPage: 170
              },
              {
                text: 'O God, who didst make this most holy night to shine with the glory of the Lord’s resurrection: Stir up in thy Church that Spirit of adoption which is given to us in Baptism, that we, being renewed both in body and mind, may worship thee in sincerity and truth; through the same Jesus Christ our Lord, who liveth and reigneth with thee in the unity of the same Spirit, one God, now and for ever.',
                bcpPage: 170
              },
              {
                text: 'Almighty God, who through thine only-begotten Son Jesus Christ hast overcome death and opened unto us the gate of everlasting life: Grant that we, who celebrate with joy the day of the Lord’s resurrection, may be raised from the death of sin by thy life-giving Spirit; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the same Spirit ever, one God, world without end.',
                bcpPage: 170
              }
            ]
          }
        },
        {
          day: 'Monday',
          title: 'Monday in Easter Week',
          psalms: {
            morning: [
              '93',
              '98'
            ],
            evening: [
              '66'
            ]
          },
          lessons: {
            first: 'Jonah 2:1–9',
            second: 'Acts 2:14, 22–32',
            gospel: 'John 14:1–14'
          },
          collects: {
            contemporary: [
              {
                text: 'Grant, we pray, Almighty God, that we who celebrate with awe the Paschal feast may be found worthy to attain to everlasting joys; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
                bcpPage: 222
              }
            ],
            traditional: [
              {
                text: 'Grant, we beseech thee, Almighty God, that we who celebrate with reverence the Paschal feast may be found worthy to attain to everlasting joys; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
               bcpPage: 170
              }
            ]
          }
        },
        {
          day: 'Tuesday',
          title: 'Tuesday in Easter Week',
          psalms: {
            morning: [
              '103'
            ],
            evening: [
              '111',
              '114'
            ]
          },
          lessons: {
            first: 'Isa 30:18–21',
            second: 'Acts 2:36–41(42–47)',
            gospel: 'John 14:15–31'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, who by the glorious resurrection of your Son Jesus Christ destroyed death and brought life and immortality to light: Grant that we, who have been raised with him, may abide in his presence and rejoice in the hope of eternal glory; through Jesus Christ our Lord, to whom, with you and the Holy Spirit, be dominion and praise for ever and ever.',
                bcpPage: 223
              }
            ],
            traditional: [
              {
                text: 'O God, who by the glorious resurrection of thy Son Jesus Christ destroyed death and brought life and immortality to light: Grant that we, who have been raised with him, may abide in his presence and rejoice in the hope of eternal glory; through the same Jesus Christ our Lord, to whom, with thee and the Holy Spirit, be dominion and praise for ever and ever. ',
               bcpPage: 171
              }
            ]
          }
        },
        {
          day: 'Wednesday',
          title: 'Wednesday in Easter Week',
          psalms: {
            morning: [
              '97',
              '99'
            ],
            evening: [
              '115'
            ]
          },
          lessons: {
            first: 'Mic 7:7–15',
            second: 'Acts 3:1–10',
            gospel: 'John 15:1–11'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, whose blessed Son made himself known to his disciples in the breaking of bread: Open the eyes of our faith, that we may behold him in all his redeeming work; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
                bcpPage: 223
              }
            ],
            traditional: [
              {
                text: 'O God, whose blessed Son did manifest himself to his disciples in the breaking of bread: Open, we pray thee, the eyes of our faith, that we may behold him in all his redeeming work; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
               bcpPage: 171
              }
            ]
          }
        },
        {
          day: 'Thursday',
          title: 'Thursday in Easter Week',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '148',
              '149'
            ]
          },
          lessons: {
            first: 'Ezek 37:1–14',
            second: 'Acts 3:11–26',
            gospel: 'John 15:12–27'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty and everlasting God, who in the Paschal mystery established the new covenant of reconciliation: Grant that all who have been reborn into the fellowship of Christ’s Body may show forth in their lives what they profess by their faith; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 223
              }
            ],
            traditional: [
              {
                text: 'Almighty and everlasting God, who in the Paschal mystery hast established the new covenant of reconciliation: Grant that all who have been reborn into the fellowship of Christ’s Body may show forth in their lives what they profess by their faith; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
               bcpPage: 172
              }
            ]
          }
        },
        {
          day: 'Friday',
          title: 'Friday in Easter Week',
          psalms: {
            morning: [
              '136'
            ],
            evening: [
              '118'
            ]
          },
          lessons: {
            first: 'Dan 12:1–4, 13',
            second: 'Acts 4:1–12',
            gospel: 'John 16:1–15'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty Father, who gave your only Son to die for our sins and to rise for our justification: Give us grace so to put away the leaven of malice and wickedness, that we may always serve you in pureness of living and truth; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
                bcpPage: 224
              }
            ],
            traditional: [
              {
                text: 'Almighty Father, who hast given thine only Son to die for our sins and to rise again for our justification: Grant us so to put away the leaven of malice and wickedness, that we may always serve thee in pureness of living and truth; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
               bcpPage: 172
              }
            ]
          }
        },
        {
          day: 'Saturday',
          title: 'Saturday in Easter Week',
          psalms: {
            morning: [
              '145'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Isa 25:1–9',
            second: 'Acts 4:13–21(22–31)',
            gospel: 'John 16:16–33'
          },
          collects: {
            contemporary: [
              {
                text: 'We thank you, heavenly Father, that you have delivered us from the dominion of sin and death and brought us into the kingdom of your Son; and we pray that, as by his death he has recalled us to life, so by his love he may raise us to eternal joys; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
                bcpPage: 224
              }
            ],
            traditional: [
              {
                text: 'We thank thee, heavenly Father, for that thou hast delivered us from the dominion of sin and death and hast brought us into the kingdom of thy Son; and we pray thee that, as by his death he hath recalled us to life, so by his love he may raise us to joys eternal; who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
               bcpPage: 172
              }
            ]
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 959
      },
      days: [
        {
          day: 'Sunday',
          title: {
            full: 'The Sunday of the Resurrection, or Easter Day',
            main: 'Easter Day',
            sub: 'Sunday of the Resurrection'
          },
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '113',
              '114',
              '118'
            ]
          },
          lessons: {
            morning: {
              first: 'Exod 12:1–14',
              gospel: 'John 1:1–18'
            },
            evening: {
              first: 'Isa 51:9–11',
              gospel: 'Luke 24:13–35',
              altGospel: 'John 20:19–23'
            }
          },
          collects: {
            contemporary: [
              {
                text: 'O God, who for our redemption gave your only-begotten Son to the death of the cross, and by his glorious resurrection delivered us from the power of our enemy: Grant us so to die daily to sin, that we may evermore live with him in the joy of his resurrection; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
                bcpPage: 222
              },
              {
                text: 'O God, who made this most holy night to shine with the glory of the Lord’s resurrection: Stir up in your Church that Spirit of adoption which is given to us in Baptism, that we, being renewed both in body and mind, may worship you in sincerity and truth; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
                bcpPage: 222
              },
              {
                text: 'Almighty God, who through your only-begotten Son Jesus Christ overcame death and opened to us the gate of everlasting life: Grant that we, who celebrate with joy the day of the Lord’s resurrection, may be raised from the death of sin by your life-giving Spirit; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
                bcpPage: 222
              }
            ],
            traditional: [
              {
                text: 'O God, who for our redemption didst give thine only-begotten Son to the death of the cross, and by his glorious resurrection hast delivered us from the power of our enemy: Grant us so to die daily to sin, that we may evermore live with him in the joy of his resurrection; through the same thy Son Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
                bcpPage: 170
              },
              {
                text: 'O God, who didst make this most holy night to shine with the glory of the Lord’s resurrection: Stir up in thy Church that Spirit of adoption which is given to us in Baptism, that we, being renewed both in body and mind, may worship thee in sincerity and truth; through the same Jesus Christ our Lord, who liveth and reigneth with thee in the unity of the same Spirit, one God, now and for ever.',
                bcpPage: 170
              },
              {
                text: 'Almighty God, who through thine only-begotten Son Jesus Christ hast overcome death and opened unto us the gate of everlasting life: Grant that we, who celebrate with joy the day of the Lord’s resurrection, may be raised from the death of sin by thy life-giving Spirit; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the same Spirit ever, one God, world without end.',
                bcpPage: 170
              }
            ]
          }
        },
        {
          day: 'Monday',
          title: 'Monday in Easter Week',
          psalms: {
            morning: [
              '93',
              '98'
            ],
            evening: [
              '66'
            ]
          },
          lessons: {
            first: 'Exod 12:14–27',
            second: '1 Cor 15:1–11',
            gospel: 'Mark 16:1–8'
          },
          collects: {
            contemporary: [
              {
                text: 'Grant, we pray, Almighty God, that we who celebrate with awe the Paschal feast may be found worthy to attain to everlasting joys; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
                bcpPage: 222
              }
            ],
            traditional: [
              {
                text: 'Grant, we beseech thee, Almighty God, that we who celebrate with reverence the Paschal feast may be found worthy to attain to everlasting joys; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
               bcpPage: 170
              }
            ]
          }
        },
        {
          day: 'Tuesday',
          title: 'Tuesday in Easter Week',
          psalms: {
            morning: [
              '103'
            ],
            evening: [
              '111',
              '114'
            ]
          },
          lessons: {
            first: 'Exod 12:28–39',
            second: '1 Cor 15:12–28',
            gospel: 'Matt 16:9–20'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, who by the glorious resurrection of your Son Jesus Christ destroyed death and brought life and immortality to light: Grant that we, who have been raised with him, may abide in his presence and rejoice in the hope of eternal glory; through Jesus Christ our Lord, to whom, with you and the Holy Spirit, be dominion and praise for ever and ever.',
                bcpPage: 223
              }
            ],
            traditional: [
              {
                text: 'O God, who by the glorious resurrection of thy Son Jesus Christ destroyed death and brought life and immortality to light: Grant that we, who have been raised with him, may abide in his presence and rejoice in the hope of eternal glory; through the same Jesus Christ our Lord, to whom, with thee and the Holy Spirit, be dominion and praise for ever and ever. ',
               bcpPage: 171
              }
            ]
          }
        },
        {
          day: 'Wednesday',
          title: 'Wednesday in Easter Week',
          psalms: {
            morning: [
              '97',
              '99'
            ],
            evening: [
              '115'
            ]
          },
          lessons: {
            first: 'Exod 12:40–51',
            second: '1 Cor 15:(29)30–41',
            gospel: 'Matt 28:1–16'
          },
          collects: {
            contemporary: [
              {
                text: 'O God, whose blessed Son made himself known to his disciples in the breaking of bread: Open the eyes of our faith, that we may behold him in all his redeeming work; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
                bcpPage: 223
              }
            ],
            traditional: [
              {
                text: 'O God, whose blessed Son did manifest himself to his disciples in the breaking of bread: Open, we pray thee, the eyes of our faith, that we may behold him in all his redeeming work; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
               bcpPage: 171
              }
            ]
          }
        },
        {
          day: 'Thursday',
          title: 'Thursday in Easter Week',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '148',
              '149'
            ]
          },
          lessons: {
            first: 'Exod 13:3–10',
            second: '1 Cor 15:41–50',
            gospel: 'Matt 28:16–20'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty and everlasting God, who in the Paschal mystery established the new covenant of reconciliation: Grant that all who have been reborn into the fellowship of Christ’s Body may show forth in their lives what they profess by their faith; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 223
              }
            ],
            traditional: [
              {
                text: 'Almighty and everlasting God, who in the Paschal mystery hast established the new covenant of reconciliation: Grant that all who have been reborn into the fellowship of Christ’s Body may show forth in their lives what they profess by their faith; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
               bcpPage: 172
              }
            ]
          }
        },
        {
          day: 'Friday',
          title: 'Friday in Easter Week',
          psalms: {
            morning: [
              '136'
            ],
            evening: [
              '118'
            ]
          },
          lessons: {
            first: 'Exod 13:1–2, 11–16',
            second: '1 Cor 15:51–58',
            gospel: 'Luke 24:1–12'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty Father, who gave your only Son to die for our sins and to rise for our justification: Give us grace so to put away the leaven of malice and wickedness, that we may always serve you in pureness of living and truth; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
                bcpPage: 224
              }
            ],
            traditional: [
              {
                text: 'Almighty Father, who hast given thine only Son to die for our sins and to rise again for our justification: Grant us so to put away the leaven of malice and wickedness, that we may always serve thee in pureness of living and truth; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
               bcpPage: 172
              }
            ]
          }
        },
        {
          day: 'Saturday',
          title: 'Saturday in Easter Week',
          psalms: {
            morning: [
              '145'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Exod 13:17–14:4',
            second: '2 Cor 4:16–5:10',
            gospel: 'Mark 12:18–27'
          },
          collects: {
            contemporary: [
              {
                text: 'We thank you, heavenly Father, that you have delivered us from the dominion of sin and death and brought us into the kingdom of your Son; and we pray that, as by his death he has recalled us to life, so by his love he may raise us to eternal joys; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
                bcpPage: 224
              }
            ],
            traditional: [
              {
                text: 'We thank thee, heavenly Father, for that thou hast delivered us from the dominion of sin and death and hast brought us into the kingdom of thy Son; and we pray thee that, as by his death he hath recalled us to life, so by his love he may raise us to joys eternal; who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
               bcpPage: 172
              }
            ]
          }
        }
      ]
    }
  ]
}
