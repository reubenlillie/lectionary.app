/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 9
 * @module _data/dailyOffice/proper9
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 9',
    collects: {
      contemporary: [
        {
          text: 'O God, you have taught us to keep all your commandments by loving you and our neighbor: Grant us the grace of your Holy Spirit, that we may be devoted to you with our whole heart, and united to one another with pure affection; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 230
        }
      ],
      traditional: [
        {
          text: 'O God, who hast taught us to keep all thy commandments by loving thee and our neighbor: Grant us the grace of thy Holy Spirit, that we may be devoted to thee with our whole heart, and united to one another with pure affection; through Jesus Christ our Lord, who liveth and reigneth with thee and the same Spirit, one God, for ever and ever.',
          bcpPage: 179
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 974
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: '1 Sam 14:36–45',
            second: 'Rom 5:1–11',
            gospel: 'Matt 22:1–14'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: '1 Sam 15:1–3, 7–23',
            second: 'Acts 9:19b–31',
            gospel: 'Luke 23:44–56a'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: '1 Sam 15:24–35',
            second: 'Acts 9:32–43',
            gospel: 'Luke 23:56b–24:11'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: '1 Sam 16:1–13',
            second: 'Acts 10:1–16',
            gospel: 'Luke 24:13–35'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: '1 Sam 16:14–17:11',
            second: 'Acts 10:17–33',
            gospel: 'Luke 24:36–53'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: '1 Sam 17:17–30',
            second: 'Acts 10:34–48',
            gospel: 'Mark 1:1–13'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: '1 Sam 17:31–49',
            second: 'Acts 11:1–18',
            gospel: 'Mark 1:14–28'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 975
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '146',
              '147'
            ],
            evening: [
              '111',
              '112',
              '113'
            ]
          },
          lessons: {
            first: 'Num 27:12–23',
            second: 'Acts 19:11–20',
            gospel: 'Mark 1:14–20'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '1',
              '2',
              '3'
            ],
            evening: [
              '4',
              '7'
            ]
          },
          lessons: {
            first: 'Num 32:1–6, 16–27',
            second: 'Rom 8:26–30',
            gospel: 'Mark 23:1–12'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '5',
              '6'
            ],
            evening: [
              '10',
              '11'
            ]
          },
          lessons: {
            first: 'Num 35:1–3, 9–15, 30–34',
            second: 'Rom 8:31–39',
            gospel: 'Matt 23:13–26'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:1–24'
            ],
            evening: [
              '12',
              '13',
              '14'
            ]
          },
          lessons: {
            first: 'Deut 1:1–18',
            second: 'Rom 9:1–18',
            gospel: 'Matt 23:27–39'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '18:1–20'
            ],
            evening: [
              '18:21–50'
            ]
          },
          lessons: {
            first: 'Deut 3:18–28',
            second: 'Rom 9:19–33',
            gospel: 'Matt 24:1–14'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '16',
              '17'
            ],
            evening: [
              '22'
            ]
          },
          lessons: {
            first: 'Deut 31:7–13, 24–32:4',
            second: 'Rom 10:1–13',
            gospel: 'Matt 24:15–31'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '20',
              '21:1–7(8–13)'
            ],
            evening: [
              '110:1–5(6–7)',
              '116',
              '117'
            ]
          },
          lessons: {
            first: 'Deut 34:1–12',
            second: 'Rom 10:14–21',
            gospel: 'Matt 24:32–51'
          }
        }
      ]
    }
  ]
}
