/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 1
 * @module _data/dailyOffice/proper1
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 1',
    collects: {
      contemporary: [
        {
          text: 'Remember, O Lord, what you have wrought in us and not what we deserve; and, as you have called us to your service, make us worthy of our calling; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 228
        }
      ],
      traditional: [
        {
          text: 'Remember, O Lord, what thou hast wrought in us and not what we deserve; and, as thou hast called us to thy service, make us worthy of our calling; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 176
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 966
      },
      days: [
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Isa 63:7–14',
            second: '2 Tim 1:1–14',
            gospel: 'Luke 11:24–36'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Isa 63:15–64:9',
            second: '2 Tim 1:15–2:13',
            gospel: 'Luke 11:37–52'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Isa 65:1–12',
            second: '2 Tim 2:14–26',
            gospel: 'Luke 11:53–12:12'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Isa 65:17–25',
            second: '2 Tim 3:1–17',
            gospel: 'Luke 12:13–31'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Isa 66:1–6',
            second: '2 Tim 4:1–8',
            gospel: 'Luke 12:32–48'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Isa 66:7–14',
            second: '2 Tim 4:9–22',
            gospel: 'Luke 12:49–59'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 967
      },
      days: [
        {
          day: 'Monday',
          psalms: {
            morning: [
              '106:1–18'
            ],
            evening: [
              '106:19–48'
            ]
          },
          lessons: {
            first: 'Ezek 33:1–11',
            second: '1 John 1:1–10',
            gospel: 'Matt 9:27–34'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '[120]',
              '121',
              '122',
              '123'
            ],
            evening: [
              '124',
              '125',
              '126',
              '[127]'
            ]
          },
          lessons: {
            first: 'Ezek 33:21–33',
            second: '1 John 2:1–11',
            gospel: 'Matt 9:35–10:4'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '119:145–176'
            ],
            evening: [
              '128',
              '129',
              '130'
            ]
          },
          lessons: {
            first: 'Ezek 34:1–16',
            second: '1 John 2:12–17',
            gospel: 'Matt 10:5–15'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '131',
              '132',
              '[133]'
            ],
            evening: [
              '134',
              '135'
            ]
          },
          lessons: {
            first: 'Ezek 37:21b–28',
            second: '1 John 2:18–29',
            gospel: 'Matt 10:16–23'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '140',
              '142'
            ],
            evening: [
              '141',
              '143:1–11(12)'
            ]
          },
          lessons: {
            first: 'Ezek 39:21–29',
            second: '1 John 3:1–10',
            gospel: 'Matt 10:24–33'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '137:1–6(7–9)',
              '144'
            ],
            evening: [
              '104'
            ]
          },
          lessons: {
            first: 'Ezek 47:1–12',
            second: '1 John 3:11–18',
            gospel: 'Matt 10:34–42'
          }
        }
      ]
    }
  ]
}
