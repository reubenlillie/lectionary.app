/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Proper 7
 * @module _data/dailyOffice/proper7
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'The Season after Pentecost',
    week: 'Proper 7',
    collects: {
      contemporary: [
        {
          text: 'O Lord, make us have perpetual love and reverence for your holy Name, for you never fail to help and govern those whom you have set upon the sure foundation of your loving-kindness; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 230
        }
      ],
      traditional: [
        {
          text: 'O Lord, we beseech thee, make us to have a perpetual fear and love of thy holy Name, for thou never failest to help and govern those whom thou hast set upon the sure foundation of thy loving-kindness; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 178
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 972
      },
      days: [
      {
          day: 'Sunday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: '1 Sam 4:12–22',
            second: 'James 1:18',
            gospel: 'Matt 19:23–30'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: '1 Sam 5:1–12',
            second: 'Acts 5:12–26',
            gospel: 'Luke 21:29–36'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: '1 Sam 6:1–16',
            second: 'Acts 5:27–42',
            gospel: 'Luke 21:37–22:13'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: '1 Sam 7:2–17',
            second: 'Acts 6:1–15',
            gospel: 'Luke 22:14–23'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: '1 Sam 8:1–22',
            second: 'Acts 6:15–7:16',
            gospel: 'Luke 22:24–30'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: '1 Sam 9:1–14',
            second: 'Acts 7:17–29',
            gospel: 'Luke 22:31–38'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: '1 Sam 9:15–10:1',
            second: 'Acts 7:30–43',
            gospel: 'Luke 22:39–51'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 973
      },
      days: [
        {
          day: 'Sunday',
          psalms: {
            morning: [
              '66',
              '67'
            ],
            evening: [
              '19',
              '46'
            ]
          },
          lessons: {
            first: 'Num 14:26–45',
            second: 'Acts 15:1–12',
            gospel: 'Luke 12:49–56'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '89:1–18'
            ],
            evening: [
              '89:19–52'
            ]
          },
          lessons: {
            first: 'Num 16:1–19',
            second: 'Rom 3:21–31',
            gospel: 'Matt 19:13–22'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '97',
              '99',
              '[100]'
            ],
            evening: [
              '94',
              '[95]'
            ]
          },
          lessons: {
            first: 'Num 15:20–35',
            second: 'Rom 4:1–12',
            gospel: 'Matt 19:23–30'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '101',
              '109:1–4(5–19)20–30'
            ],
            evening: [
              '119:121–144'
            ]
          },
          lessons: {
            first: 'Num 16:36–50',
            second: 'Rom 4:13–25',
            gospel: 'Matt 20:1–16'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '105:1–22'
            ],
            evening: [
              '105:23–45'
            ]
          },
          lessons: {
            first: 'Num 17:1–11',
            second: 'Rom 5:1–11',
            gospel: 'Matt 20:17–28'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '102'
            ],
            evening: [
              '107:1–32'
            ]
          },
          lessons: {
            first: 'Num 20:1–13',
            second: 'Rom 5:12–21',
            gospel: 'Matt 20:29–34'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '107:33–43',
              '108:1–6(7–13)'
            ],
            evening: [
              '33'
            ]
          },
          lessons: {
            first: 'Num 20:14–29',
            second: 'Rom 6:1–11',
            gospel: 'Matt 21:1–11'
          }
        }
      ]
    }
  ]
}
