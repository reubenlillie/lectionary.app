/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary Holy Days
 * @module _data/dailyOffice/holyDays
 * @since 0.1.0
 * @since 0.11.0 Rekey second readings that are gospels and their alternates; add `color` properties
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default [
  {
    day: 'Nov 30',
    title: {
      full: 'Saint Andrew the Apostle',
      main: 'St. Andrew',
      sub: 'The Apostle'
    },
    color: 'red',
    psalms: {
      morning: [
        '34'
      ],
      evening: [
        '96',
        '100'
      ]
    },
    lessons: {
      morning: {
        first: 'Isa 49:1–6',
        second: '1 Cor 4:1–16'
      },
      evening: {
        first: 'Isa 55:1–5',
        gospel: 'John 1:35–42'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, who gave such grace to your apostle Andrew that he readily obeyed the call of your Son Jesus Christ, and brought his brother with him: Give us, who are called by your Holy Word, grace to follow him without delay, and to bring those near to us into his gracious presence; who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 237
        }
      ],
      traditional: [
        {
          text: 'Almighty God, who didst give such grace to thine apostle Andrew that he readily obeyed the call of thy Son Jesus Christ, and brought his brother with him: Give unto us, who are called by thy Word, grace to follow him without delay, and to bring those near to us into his gracious presence; who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 185
        }
      ]
    }
  },
  {
    day: 'Dec 21',
    title: {
      full: 'Saint Thomas the Apostle',
      main: 'St. Thomas',
      sub: 'The Apostle'
    },
    color: 'red',
    psalms: {
      morning: [
        '23',
        '121'
      ],
      evening: [
        '27'
      ]
    },
    lessons: {
      morning: {
        first: 'Job 42:1–6',
        second: '1 Pet 1:3–9'
      },
      evening: {
        first: 'Isa 43:8–13',
        gospel: 'John 14:1–7'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Everliving God, who strengthened your apostle Thomas with firm and certain faith in your Son’s resurrection: Grant us so perfectly and without doubt to believe in Jesus Christ, our Lord and our God, that our faith may never be found wanting in your sight; through him who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 237
        }
      ],
      traditional: [
        {
          text: 'Everliving God, who didst strengthen thine apostle Thomas with sure and certain faith in thy Son’s resurrection: Grant us so perfectly and without doubt to believe in Jesus Christ, our Lord and our God, that our faith may never be found wanting in thy sight; through him who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 185
        }
      ]
    }
  },
  {
    day: 'Dec 26',
    title: {
      full: 'Saint Stephen, Deacon and Martyr',
      main: 'St. Stephen',
      sub: 'Deacon and Martyr'
    },
    color: 'red',
    psalms: {
      morning: [
        '28',
        '30'
      ],
      evening: [
        '118'
      ]
    },
    lessons: {
      morning: {
        first: '2 Chr 24:17–22',
        second: 'Acts 6:1–7'
      },
      evening: {
        first: 'Wis 4:7–15',
        second: 'Acts 7:59–8:8'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'We give you thanks, O Lord of glory, for the example of the first martyr Stephen, who looked up to heaven and prayed for his persecutors to your Son Jesus Christ, who stands at your right hand; where he lives and reigns with you and the Holy Spirit, one God, in glory everlasting.',
          bcpPage: 237
        }
      ],
      traditional: [
        {
          text: 'We give thee thanks, O Lord of glory, for the example of the first martyr Stephen, who looked up to heaven and prayed for his persecutors to thy Son Jesus Christ, who standeth at thy right hand; where he liveth and reigneth with thee and the Holy Spirit, one God, in glory everlasting.',
          bcpPage: 186
        }
      ]
    }
  },
  {
    day: 'Dec 27',
    title: {
      full: 'Saint John, Apostle and Evangelist',
      main: 'St. John',
      sub: 'Apostle and Evangelist'
    },
    color: 'white',
    psalms: {
      morning: [
        '97',
        '98'
      ],
      evening: [
        '145'
      ]
    },
    lessons: {
      morning: {
        first: 'Prov 8:22–30',
        gospel: 'John 13:20–35'
      },
      evening: {
        first: 'Isa 44:1–8',
        second: '1 John 5:1–12'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Shed upon your Church, O Lord, the brightness of your light, that we, being illumined by the teaching of your apostle and evangelist John, may so walk in the light of your truth, that at length we may attain to the fullness of eternal life; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 238
        }
      ],
      traditional: [
        {
          text: 'Shed upon thy Church, we beseech thee, O Lord, the brightness of thy light; that we, being illumined by the teaching of thine apostle and evangelist John, may so walk in the light of thy truth, that we may at length attain to the fullness of life everlasting; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 186
        }
      ]
    }
  },
  {
    day: 'Dec 28',
    title: 'Holy Innocents',
    color: 'red',
    psalms: {
      morning: [
        '2',
        '26'
      ],
      evening: [
        '19',
        '126'
      ]
    },
    lessons: {
      morning: {
        first: 'Isa 49:13–23',
        gospel: 'Matt 18:1–14'
      },
      evening: {
        first: 'Isa 54:1–13',
        gospel: 'Mark 10:13–16'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'We remember today, O God, the slaughter of the holy innocents of Bethlehem by King Herod. Receive, we pray, into the arms of your mercy all innocent victims; and by your great might frustrate the designs of evil tyrants and establish your rule of justice, love, and peace; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, for ever and ever.',
          bcpPage: 238
        }
      ],
      traditional: [
        {
          text: 'We remember this day, O God, the slaughter of the holy innocents of Bethlehem by the order of King Herod. Receive, we beseech thee, into the arms of thy mercy all innocent victims; and by thy great might frustrate the designs of evil tyrants and establish thy rule of justice, love, and peace; through Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, for ever and ever.',
          bcpPage: 186
        }
      ]
    }
  },
  {
    day: 'Jan 18',
    title: {
      full: 'The Confession of Saint Peter the Apostle',
      main: 'Confession of St. Peter',
      sub: 'The Apostle'
    },
    color: 'red',
    psalms: {
      morning: [
        '66',
        '67'
      ],
      evening: [
        '118'
      ]
    },
    lessons: {
      morning: {
        first: 'Ezek 3:4–11',
        second: 'Acts 10:34–44'
      },
      evening: {
        first: 'Ezek 34:11–16',
        gospel: 'John 21:15–22'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty Father, who inspired Simon Peter, first among the apostles, to confess Jesus as Messiah and Son of the living God: Keep your Church steadfast upon the rock of this faith, so that in unity and peace we may proclaim the one truth and follow the one Lord, our Savior Jesus Christ; who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 238
        }
      ],
      traditional: [
        {
          text: 'Almighty Father, who didst inspire Simon Peter, first among the apostles, to confess Jesus as Messiah and Son of the living God: Keep thy Church steadfast upon the rock of this faith, that in unity and peace we may proclaim the one truth and follow the one Lord, our Savior Jesus Christ; who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 187
        }
      ]
    }
  },
  {
    day: 'Jan 25',
    title: {
      full: 'The Conversion of Saint Paul the Apostle',
      main: 'Conversion of St. Paul',
      sub: 'The Apostle'
    },
    color: 'white',
    psalms: {
      morning: [
        '19'
      ],
      evening: [
        '119:89–112'
      ]
    },
    lessons: {
      morning: {
        first: 'Isa 45:18–25',
        second: 'Phil 3:4b–11'
      },
      evening: {
        first: 'Sir 39:1–10',
        second: 'Acts 9:1–22'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'O God, by the preaching of your apostle Paul you have caused the light of the Gospel to shine throughout the world: Grant, we pray, that we, having his wonderful conversion in remembrance, may show ourselves thankful to you by following his holy teaching; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 238
        }
      ],
      traditional: [
        {
          text: 'O God, who, by the preaching of thine apostle Paul, hast caused the light of the Gospel to shine throughout the world: Grant, we beseech thee, that we, having his wonderful conversion in remembrance, may show forth our thankfulness unto thee for the same by following the holy doctrine which he taught; through Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 187
        }
      ]
    }
  },
  {
    day: 'Feb 1',
    title: 'Eve of the Presentation',
    psalms: {
      evening: [
        '113',
        '122'
      ]
    },
    lessons: {
      evening: {
        first: '1 Sam 1:20–28a',
        second: 'Rom 8:14–21'
      }
    },
  },
  {
    day: 'Feb 2',
    title: {
      full: 'The Presentation of Our Lord Jesus Christ in the Temple, or the Purification of Saint Mary,',
      main: 'The Presentation',
      sub: 'of Christ in the Temple'
    },
    color: 'white',
    psalms: {
      morning: [
        '42',
        '43'
      ],
      evening: [
        '48',
        '87'
      ]
    },
    lessons: {
      morning: {
        first: '1 Sam 2:1–10',
        gospel: 'John 8:31–36'
      },
      evening: {
        first: 'Hag 2:1–9',
        second: '1 John 3:1–8'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty and everliving God, we humbly pray that, as your only-begotten Son was this day presented in the temple, so we may be presented to you with pure and clean hearts by Jesus Christ our Lord; who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 239
        }
      ],
      traditional: [
        {
          text: 'Almighty and everliving God, we humbly beseech thee that, as thy only-begotten Son was this day presented in the temple, so we may be presented unto thee with pure and clean hearts by the same thy Son Jesus Christ our Lord; who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 187
        }
      ]
    }
  },
  {
    day: 'Feb 24',
    title: {
      full: 'Saint Matthias the Apostle',
      main: 'St. Matthias',
      sub: 'The Apostle'
    },
    color: 'red',
    psalms: {
      morning: [
        '80'
      ],
      evening: [
        '33'
      ]
    },
    lessons: {
      morning: {
        first: '1 Sam 16:1–13',
        second: '1 John 2:18–25'
      },
      evening: {
        first: '1 Sam 12:1–5',
        second: 'Acts 20:17–35'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, who in the place of Judas chose your faithful servant Matthias to be numbered among the Twelve: Grant that your Church, being delivered from false apostles, may always be guided and governed by faithful and true pastors; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 239
        }
      ],
      traditional: [
        {
          text: 'O Almighty God, who into the place of Judas didst choose thy faithful servant Matthias to be of the number of the Twelve: Grant that thy Church, being delivered from false apostles, may always be ordered and guided by faithful and true pastors; through Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 188
        }
      ]
    }
  },
  {
    day: 'Mar 19',
    title: {
      full: 'Saint Joseph',
      main: 'St. Joseph'
    },
    color: 'white',
    psalms: {
      morning: [
        '132'
      ],
      evening: [
        '34'
      ]
    },
    lessons: {
      morning: {
        first: 'Isa 63:7–16',
        gospel: 'Matt 1:18–25'
      },
      evening: {
        first: '2 Chr 6:12–17',
        second: 'Eph 3:14–21'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'O God, who from the family of your servant David raised up Joseph to be the guardian of your incarnate Son and the spouse of his virgin mother: Give us grace to imitate his uprightness of life and his obedience to your commands; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 239
        }
      ],
      traditional: [
        {
          text: 'O God, who from the family of thy servant David didst raise up Joseph to be the guardian of thy incarnate Son and the spouse of his virgin mother: Give us grace to imitate his uprightness of life and his obedience to thy commands; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 188
        }
      ]
    }
  },
  {
    day: 'Mar 24',
    title: 'Eve of the Annunciation',
    psalms: {
      evening: [
        '8',
        '138'
      ]
    },
    lessons: {
      evening: {
        first: 'Gen 3:1–15',
        second: 'Eph 3:14–21'
      }
    }
  },
  {
    day: 'Mar 25',
    title: {
      full: 'The Annunciation of Our Lord Jesus Christ to Saint Mary',
      main: 'The Annuciation',
      sub: 'of Christ to St. Mary'
    },
    color: 'white',
    psalms: {
      morning: [
        '85',
        '87'
      ],
      evening: [
        '110:1–5(6–7)',
        '132'
      ]
    },
    lessons: {
      morning: {
        first: 'Isa 52:7–12',
        second: 'Heb 2:5–10'
      },
      evening: {
        first: 'Wis 9:1–12',
        gospel: 'John 1:9–14'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Pour your grace into our hearts, O Lord, that we who have known the incarnation of your Son Jesus Christ, announced by an angel to the Virgin Mary, may by his cross and passion be brought to the glory of his resurrection; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 240
        }
      ],
      traditional: [
        {
          text: 'We beseech thee, O Lord, pour thy grace into our hearts, that we who have known the incarnation of thy Son Jesus Christ, announced by an angel to the Virgin Mary, may by his cross and passion be brought unto the glory of his resurrection; who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 188
        }
      ]
    }
  },
  {
    day: 'Apr 25',
    title: {
      full: 'Saint Mark the Evangelist',
      main: 'St. Mark',
      sub: 'The Evangelist'
    },
    color: 'red',
    psalms: {
      morning: [
        '145'
      ],
      evening: [
        '67',
        '96'
      ]
    },
    lessons: {
      morning: {
        first: 'Sir 2:1–11',
        second: 'Acts 12:25–13:3'
      },
      evening: {
        first: 'Isa 62:6–12',
        second: '2 Tim 4:1–11'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, by the hand of Mark the evangelist you have given to your Church the Gospel of Jesus Christ the Son of God: We thank you for this witness, and pray that we may be firmly grounded in its truth; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 240
        }
      ],
      traditional: [
        {
          text: 'Almighty God, who by the hand of Mark the evangelist hast given to thy Church the Gospel of Jesus Christ the Son of God: We thank thee for this witness, and pray that we may be firmly grounded in its truth; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 188
        }
      ]
    }
  },
  {
    day: 'May 1',
    title: {
      full: 'Saint Philip and Saint James, Apostles',
      main: 'St. Philip and St. James',
      sub: 'Apostles'
    },
    color: 'red',
    psalms: {
      morning: [
        '119:137–160'
      ],
      evening: [
        '139'
      ]
    },
    lessons: {
      morning: {
        first: 'Job 23:1–12',
        gospel: 'John 1:42–51'
      },
      evening: {
        first: 'Prov 4:7–8',
        gospel: 'John 12:20–26'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, who gave to your apostles Philip and James grace and strength to bear witness to the truth: Grant that we, being mindful of their victory of faith, may glorify in life and death the Name of our Lord Jesus Christ; who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 240
        }
      ],
      traditional: [
        {
          text: 'Almighty God, who didst give to thine apostles Philip and James grace and strength to bear witness to the truth: Grant that we, being mindful of their victory of faith, may glorify in life and death the Name of our Lord Jesus Christ; who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 189
        }
      ]
    }
  },
  {
    day: 'May 30',
    title: 'Eve of the Visitation',
    color: 'white',
    psalms: {
      evening: [
        '146',
        '147'
      ]
    },
    lessons: {
      evening: {
        first: 'Isa 11:1–10',
        second: 'Heb 2:11–18'
      }
    }
  },
  {
    day: 'May 31',
    title: {
      full: 'The Visitation of St. Mary to Elizabeth',
      main: 'The Visitation',
      sub: 'of St. Mary to Elizabeth'
    },
    color: 'white',
    psalms: {
      morning: [
        '72'
      ],
      evening: [
        '146',
        '147'
      ]
    },
    lessons: {
      morning: {
        first: '1 Sam 1:1–20',
        second: 'Heb 3:1–6'
      },
      evening: {
        first: 'Zech 2:10–13',
        gospel: 'John 3:25–30'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Father in heaven, by your grace the virgin mother of your incarnate Son was blessed in bearing him, but still more blessed in keeping your word: Grant us who honor the exaltation of her lowliness to follow the example of her devotion to your will; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 240
        }
      ],
      traditional: [
        {
          text: 'Father in heaven, by whose grace the virgin mother of thy incarnate Son was blessed in bearing him, but still more blessed in keeping thy word: Grant us who honor the exaltation of her lowliness to follow the example of her devotion to thy will; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 189
        }
      ]
    }
  },
  {
    day: 'Jun 11',
    title: {
      full: 'Saint Barnabas the Apostle',
      main: 'St. Barnabas',
      sub: 'The Apostle'
    },
    color: 'red',
    psalms: {
      morning: [
        '15',
        '67'
      ],
      evening: [
        '19',
        '146'
      ]
    },
    lessons: {
      morning: {
        first: 'Sir 31:3–11',
        second: 'Acts 4:32–37'
      },
      evening: {
        first: 'Job 29:1–16',
        second: 'Acts 9:26–31'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Grant, O God, that we may follow the example of your faithful servant Barnabas, who, seeking not his own renown but the well-being of your Church, gave generously of his life and substance for the relief of the poor and the spread of the Gospel; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 241
        }
      ],
      traditional: [
        {
          text: 'Grant, O God, that we may follow the example of thy faithful servant Barnabas, who, seeking not his own renown but the well-being of thy Church, gave generously of his life and substance for the relief of the poor and the spread of the Gospel; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 189
        }
      ]
    }
  },
  {
    day: 'Jun 23',
    title: 'Eve of St. John the Baptist',
    psalms: {
      evening: [
        '103'
      ]
    },
    lessons: {
      evening: {
        first: 'Sir 48:1–11',
        gospel: 'Luke 1:5–23'
      }
    }
  },
  {
    day: 'Jun 24',
    title: {
      full: 'The Nativity of Saint John the Baptist',
      main: 'Nativity of St. John the Baptist',
    },
    color: 'white',
    psalms: {
      morning: [
        '82',
        '98'
      ],
      evening: [
        '80'
      ]
    },
    lessons: {
      morning: {
        first: 'Mal 3:1–5',
        gospel: 'John 3:22–30'
      },
      evening: {
        first: 'Mal 4:1–6',
        gospel: 'Matt 11:2–19'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, by whose providence your servant John the Baptist was wonderfully born, and sent to prepare the way of your Son our Savior by preaching repentance: Make us so to follow his teaching and holy life, that we may truly repent according to his preaching; and, following his example, constantly speak the truth, boldly rebuke vice, and patiently suffer for the truth’s sake; through Jesus Christ your Son our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 241
        }
      ],
      traditional: [
        {
          text: 'Almighty God, by whose providence thy servant John the Baptist was wonderfully born, and sent to prepare the way of thy Son our Savior by preaching repentance: Make us so to follow his doctrine and holy life, that we may truly repent according to his preaching; and after his example constantly speak the truth, boldly rebuke vice, and patiently suffer for the truth’s sake; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit one God, for ever and ever.',
          bcpPage: 190
        }
      ]
    }
  },
  {
    day: 'Jun 29',
    title: {
      full: 'Saint Peter and Saint Paul, Apostles',
      main: 'St. Peter and St. Paul',
      sub: 'Apostles'
    },
    color: 'red',
    psalms: {
      morning: [
        '66'
      ],
      evening: [
        '97',
        '138'
      ]
    },
    lessons: {
      morning: {
        first: 'Ezek 2:1–7',
        second: 'Acts 11:1–18'
      },
      evening: {
        first: 'Isa 49:1–6',
        second: 'Gal 2:1–9'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, whose blessed apostles Peter and Paul glorified you by their martyrdom: Grant that your Church, instructed by their teaching and example, and knit together in unity by your Spirit, may ever stand firm upon the one foundation, which is Jesus Christ our Lord; who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 241
        }
      ],
      traditional: [
        {
          text: 'Almighty God, whose blessed apostles Peter and Paul glorified thee by their martyrdom: Grant that thy Church, instructed by their teaching and example, and knit together in unity by thy Spirit, may ever stand firm upon the one foundation, which is Jesus Christ our Lord; who liveth and reigneth with thee, in the unity of the same Spirit, one God, for ever and ever.',
          bcpPage: 190
        }
      ]
    }
  },
  {
    day: 'Jul 22',
    title: {
      full: 'Saint Mary Magdalene',
      main: 'St. Mary Magdalene',
    },
    color: 'white',
    psalms: {
      morning: [
        '116'
      ],
      evening: [
        '30',
        '149'
      ]
    },
    lessons: {
      morning: {
        first: 'Zeph 3:14–20',
        gospel: 'Mark 15:47–16:7'
      },
      evening: {
        first: 'Exod 15:19–21',
        second: '2 Cor 1:3–7'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, whose blessed Son restored Mary Magdalene to health of body and of mind, and called her to be a witness of his resurrection: Mercifully grant that by your grace we may be healed from all our infirmities and know you in the power of his unending life; who with you and the Holy Spirit lives and reigns, one God, now and for ever.',
          bcpPage: 241
        }
      ],
      traditional: [
        {
          text: 'Almighty God, whose blessed Son restored Mary Magdalene to health of body and mind, and called her to be a witness of his resurrection: Mercifully grant that by thy grace we may be healed of all our infirmities and know thee in the power of his endless life; who with thee and the Holy Spirit liveth and reigneth, one God, now and for ever.',
          bcpPage: 191
        }
      ]
    }
  },
  {
    day: 'Jul 25',
    title: {
      full: 'Saint James the Apostle',
      main: 'St. James',
      sub: 'The Apostle'
    },
    color: 'red',
    psalms: {
      morning: [
        '34'
      ],
      evening: [
        '33'
      ]
    },
    lessons: {
      morning: {
        first: 'Jer 16:14–21',
        gospel: 'Mark 1:14–20'
      },
      evening: {
        first: 'Jer 26:1–15',
        gospel: 'Matt 10:16–32'
      }
    }
  },
  {
    day: 'Aug 5',
    title: 'Eve of the Transfiguration',
    psalms: {
      evening: [
        '84'
      ]
    },
    lessons: {
      evening: {
        first: '1 Kings 19:1–12',
        second: '2 Cor 3:1–9, 18'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'O gracious God, we remember before you today your servant and apostle James, first among the Twelve to suffer martyrdom for the Name of Jesus Christ; and we pray that you will pour out upon the leaders of your Church that spirit of self-denying service by which alone they may have true authority among your people; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 242
        }
      ],
      traditional: [
        {
          text: 'O gracious God, we remember before thee this day thy servant and apostle James, first among the Twelve to suffer martyrdom for the Name of Jesus Christ; and we pray that thou wilt pour out upon the leaders of thy Church that spirit of self-denying service by which alone they may have true authority among thy people; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 191
        }
      ]
    }
  },
  {
    day: 'Aug 6',
    title: {
      full: 'The Transfiguration of Our Lord Jesus Christ',
      main: 'The Transfiguration',
      sub: 'of Christ'
    },
    color: 'white',
    psalms: {
      morning: [
        '2',
        '24'
      ],
      evening: [
        '72'
      ]
    },
    lessons: {
      morning: {
        first: 'Exod 24:12–18',
        second: '2 Cor 4:1–6'
      },
      evening: {
        first: 'Dan 7:9–10, 13–14',
        gospel: 'John 12:27–36a'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'O God, who on the holy mount revealed to chosen witnesses your well-beloved Son, wonderfully transfigured, in raiment white and glistening: Mercifully grant that we, being delivered from the disquietude of this world, may by faith behold the King in his beauty; who with you, O Father, and you, O Holy Spirit, lives and reigns, one God, for ever and ever.',
          bcpPage: 243
        }
      ],
      traditional: [
        {
          text: 'O God, who on the holy mount didst reveal to chosen witnesses thy well-beloved Son, wonderfully transfigured, in raiment white and glistening: Mercifully grant that we, being delivered from the disquietude of this world, may by faith behold the King in his beauty; who with thee, O Father, and thee, O Holy Ghost, liveth and reigneth, one God, world without end.',
          bcpPage: 191
        }
      ]
    }
  },
  {
    day: 'Aug 15',
    title: {
      full: 'Saint Mary, Mother of Christ',
      main: 'St. Mary',
      sub: 'Mother of Christ'
    },
    color: 'white',
    psalms: {
      morning: [
        '113',
        '115'
      ],
      evening: [
        '45',
        '[138]',
        '[149]'
      ]
    },
    lessons: {
      morning: {
        first: '1 Sam 2:1–10',
        gospel: 'John 2:1–12'
      },
      evening: {
        first: 'Jer 31:1–14',
        altFirst: 'Zech 2:10–13',
        gospel: 'John 19:23–27',
        altGospel: 'Acts 1:6–14'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'O God, you have taken to yourself the blessed Virgin Mary, mother of your incarnate Son: Grant that we, who have been redeemed by his blood, may share with her the glory of your eternal kingdom; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 243
        }
      ],
      traditional: [
        {
          text: 'O God, who hast taken to thyself the blessed Virgin Mary, mother of thy incarnate Son: Grant that we, who have been redeemed by his blood, may share with her the glory of thine eternal kingdom; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 192
        }
      ]
    }
  },
  {
    day: 'Aug 24',
    title: {
      full: 'Saint Bartholomew the Apostle',
      main: 'St. Bartholomew',
      sub: 'The Apostle'
    },
    color: 'red',
    psalms: {
      morning: [
        '86'
      ],
      evening: [
        '15',
        '67'
      ]
    },
    lessons: {
      morning: {
        first: 'Gen 28:10–17',
        gospel: 'John 1:43–51'
      },
      evening: {
        first: 'Isa 66:1–2, 18–23',
        second: '1 Pet 5:1–11'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty and everlasting God, who gave to your apostle Bartholomew grace truly to believe and to preach your Word: Grant that your Church may love what he believed and preach what he taught; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 243
        }
      ],
      traditional: [
        {
          text: 'O Almighty and everlasting God, who didst give to thine apostle Bartholomew grace truly to believe and to preach thy Word: Grant, we beseech thee, unto thy Church to love what he believed and to preach what he taught; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 192
        }
      ]
    }
  },
  {
    day: 'Sep 13',
    title: 'Eve of Holy Cross',
    psalms: {
      evening: [
        '46',
        '87'
      ]
    },
    lessons: {
      evening: {
        first: '1 Kgs 8:22–30',
        second: 'Eph 2:11–22'
      }
    }
  },
  {
    day: 'Sep 14',
    title: 'Holy Cross Day',
    color: 'white',
    psalms: {
      morning: [
        '66'
      ],
      evening: [
        '118'
      ]
    },
    lessons: {
      morning: {
        first: 'Num 21:4–9',
        gospel: 'John 3:11–17'
      },
      evening: {
        first: 'Gen 3:1–15',
        second: '1 Pet 3:17–22'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, whose Son our Savior Jesus Christ was lifted high upon the cross that he might draw the whole world to himself: Mercifully grant that we, who glory in the mystery of our redemption, may have grace to take up our cross and follow him; who lives and reigns with you and the Holy Spirit, one God, in glory everlasting.',
          bcpPage: 244
        }
      ],
      traditional: [
        {
          text: 'Almighty God, whose Son our Savior Jesus Christ was lifted high upon the cross that he might draw the whole world unto himself: Mercifully grant that we, who glory in the mystery of our redemption, may have grace to take up our cross and follow him; who liveth and reigneth with thee and the Holy Spirit, one God, in glory everlasting.',
          bcpPage: 192
        }
      ]
    }
  },
  {
    day: 'Sep 21',
    title: {
      full: 'Saint Matthew, Apostle and Evangelist',
      main: 'St. Matthew',
      sub: 'Apostle and Evangelist'
    },
    color: 'red',
    psalms: {
      morning: [
        '119:41–64'
      ],
      evening: [
        '19',
        '112'
      ]
    },
    lessons: {
      morning: {
        first: 'Isa 8:11–20',
        second: 'Rom 10:1–15'
      },
      evening: {
        first: 'Job 28:12–28',
        gospel: 'Matt 13:44–52'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'We thank you, heavenly Father, for the witness of your apostle and evangelist Matthew to the Gospel of your Son our Savior; and we pray that, after his example, we may with ready wills and hearts obey the calling of our Lord to follow him; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 244
        }
      ],
      traditional: [
        {
          text: 'We thank thee, heavenly Father, for the witness of thine apostle and evangelist Matthew to the Gospel of thy Son our Savior; and we pray that, after his example, we may with ready wills and hearts obey the calling of our Lord to follow him; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 192
        }
      ]
    }
  },
  {
    day: 'Sep 29',
    title: 'Saint Michael and All Angels',
    color: 'white',
    psalms: {
      morning: [
        '8',
        '148'
      ],
      evening: [
        '34',
        '150',
        '[104]'
      ]
    },
    lessons: {
      morning: {
        first: 'Job 38:1–7',
        second: 'Heb 1:1–14'
      },
      evening: {
        first: 'Dan 12:1–3',
        altFirst: '2 Kgs 6:8–17',
        gospel: 'Mark 13:21–27',
        altGospel: 'Rev 5:1–14'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Everlasting God, you have ordained and constituted in a wonderful order the ministries of angels and mortals: Mercifully grant that, as your holy angels always serve and worship you in heaven, so by your appointment they may help and defend us here on earth; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 244
        }
      ],
      traditional: [
        {
          text: 'O everlasting God, who hast ordained and constituted the ministries of angels and men in a wonderful order: Mercifully grant that, as thy holy angels always serve and worship thee in heaven, so by thy appointment they may help and defend us on earth; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 193
        }
      ]
    }
  },
  {
    day: 'Oct 18',
    title: {
      full: 'Saint Luke the Evangelist',
      main: 'St. Luke the Evangelist',
      sub: 'The Evangelist'
    },
    color: 'red',
    psalms: {
      morning: [
        '103'
      ],
      evening: [
        '67',
        '96'
      ]
    },
    lessons: {
      morning: {
        first: 'Ezek 47:1–12',
        gospel: 'Luke 1:1–4'
      },
      evening: {
        first: 'Isa 52:7–10',
        second: 'Acts 1:1–8'
      },
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, who inspired your servant Luke the physician to set forth in the Gospel the love and healing power of your Son: Graciously continue in your Church this love and power to heal, to the praise and glory of your Name; through Jesus Christ our Lord, who lives and reigns with you, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 244
        }
      ],
      traditional: [
        {
          text: 'Almighty God, who didst inspire thy servant Luke the physician to set forth in the Gospel the love and healing power of thy Son: Graciously continue in thy Church the like love and power to heal, to the praise and glory of thy Name; through the same thy Son Jesus Christ our Lord, who liveth and reigneth with thee, in the unity of the Holy Spirit, one God, now and for ever.',
          bcpPage: 193
        }
      ]
    }
  },
  {
    day: 'Oct 23',
    title: {
      full: 'Saint James of Jerusalem, Brother of Christ, and Martyr',
      main: 'St. James of Jerusalem',
      sub: 'Brother of Christ and Martyr'
    },
    color: 'red',
    psalms: {
      morning: [
        '119:145–168'
      ],
      evening: [
        '122',
        '125'
      ]
    },
    lessons: {
      morning: {
        first: 'Jer 11:18–23',
        gospel: 'Matt 10:16–22'
      },
      evening: {
        first: 'Isa 65:17–25',
        second: 'Heb 12:12–24'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Grant, O God, that, following the example of your servant James the Just, brother of our Lord, your Church may give itself continually to prayer and to the reconciliation of all who are at variance and enmity; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, now and for ever.',
          bcpPage: 245
        }
      ],
      traditional: [
        {
          text: 'Grant, we beseech thee, O God, that after the example of thy servant James the Just, brother of our Lord, thy Church may give itself continually to prayer and to the reconciliation of all who are at variance and enmity; through the same our Lord Jesus Christ, who liveth and reigneth with thee and the Holy Spirit, one God, now and for ever.',
          bcpPage: 193
        }
      ]
    }
  },
  {
    day: 'Oct 28',
    title: {
      full: 'Saint Simon and Saint Jude, Apostles',
      main: 'St. Simon and St. Jude',
      sub: 'Apostles'
    },
    color: 'red',
    psalms: {
      morning: [
        '66'
      ],
      evening: [
        '116',
        '117'
      ]
    },
    lessons: {
      morning: {
        first: 'Isa 28:9–16',
        second: 'Eph 4:1–16'
      },
      evening: {
        first: 'Isa 4:2–9',
        gospel: 'John 14:15–31'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'O God, we thank you for the glorious company of the apostles, and especially on this day for Simon and Jude; and we pray that, as they were faithful and zealous in their mission, so we may with ardent devotion make known the love and mercy of our Lord and Savior Jesus Christ; who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 245
        }
      ],
      traditional: [
        {
          text: 'O God, we thank thee for the glorious company of the apostles, and especially on this day for Simon and Jude; and we pray that, as they were faithful and zealous in their mission, so we may with ardent devotion make known the love and mercy of our Lord and Savior Jesus Christ; who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 194
        }
      ]
    }
  },
  {
    day: 'Oct 31',
    title: 'Eve of All Saints',
    psalms: {
      evening: [
        '34'
      ]
    },
    lessons: {
      evening: {
        first: 'Wis 3:1–9',
        second: 'Rev 19:1, 4–10'
      }
    }
  },
  {
    day: 'Nov 1',
    title: 'All Saints’ Day',
    color: 'white',
    psalms: {
      morning: [
        '111',
        '112'
      ],
      evening: [
        '148',
        '150'
      ]
    },
    lessons: {
      morning: {
        first: '2 Esd 2:42–47',
        second: 'Heb 11:32–12:2'
      },
      evening: {
        first: 'Wis 5:1–5, 14–16',
        second: 'Rev 21:1–4, 22–22:5'
      }
    },
    collects: {
      contemporary: [
        {
          text: 'Almighty God, you have knit together your elect in one communion and fellowship in the mystical body of your Son Christ our Lord: Give us grace so to follow your blessed saints in all virtuous and godly living, that we may come to those ineffable joys that you have prepared for those who truly love you; through Jesus Christ our Lord, who with you and the Holy Spirit lives and reigns, one God, in glory everlasting.',
          bcpPage: 245
        }
      ],
      traditional: [
        {
          text: 'O Almighty God, who hast knit together thine elect in one communion and fellowship in the mystical body of thy Son Christ our Lord: Grant us grace so to follow thy blessed saints in all virtuous and godly living, that we may come to those ineffable joys which thou hast prepared for those who unfeignedly love thee; through the same Jesus Christ our Lord, who with thee and the Holy Spirit liveth and reigneth, one God, in glory everlasting.',
          bcpPage: 194
        }
      ]
    }
  }
]
