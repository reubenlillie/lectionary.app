/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 6 Easter
 * @module _data/dailyOffice/easter6
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Easter',
    week: 'Week of the Sixth Sunday of Easter',
    collects: {
      contemporary: [
        {
          text: 'O God, you have prepared for those who love you such good things as surpass our understanding: Pour into our hearts such love towards you, that we, loving you in all things and above all things, may obtain your promises, which exceed all that we can desire; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
         bcpPage: 225
        }
      ],
      traditional: [
        {
          text: 'O God, who hast prepared for those who love thee such good things as pass man’s understanding: Pour into our hearts such love toward thee, that we, loving thee in all things and above all things, may obtain thy promises, which exceed all that we can desire; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
          bcpPage: 174
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 962
      },
      days: [
        {
          day: 'Sunday',
          title: 'Sixth Sunday of Easter',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Sir 43:1–12, 27–32',
            second: '1 Tim 3:14–4:5',
            gospel: 'Matt 13:24–34a'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Deut 8:1–10',
            second: 'James 1:1–15',
            gospel: 'Luke 9:18–27'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Deut 8:11–20',
            second: 'James 1:16–27',
            gospel: 'Luke 11:1–13'
          }
        },
        {
          day: 'Wednesday',
          title: 'Eve of Ascension',
          psalms: {
            morning: [
              '119:97–120'
            ],
            'evening': [
              '68:1–20'
            ]
          },
          lessons: {
            morning: {
              first: 'Bar 3:24–37',
              second: 'James 1:16–27',
              gospel: 'Luke 11:1–13'
            },
            evening: {
              first: '2 Kgs 2:1–15',
              second: 'Rev 5:1–14'
            }
          }
        },
        {
          day: 'Thursday',
          title: 'Ascension Day',
          psalms: {
            morning: [
              '8',
              '47'
            ],
            evening: [
              '24',
              '96'
            ]
          },
          lessons: {
            first: 'Ezek 1:14, 24–28b',
            second: 'Heb 2:5–18',
            gospel: 'Matt 28:1–20'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty God, whose blessed Son our Savior Jesus Christ ascended far above all heavens that he might fill all things: Mercifully give us faith to perceive that, according to his promise, he abides with his Church on earth, even to the end of the ages; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, in glory everlasting.',
               bcpPage: 226
              },
              {
                text: 'Grant, we pray, Almighty God, that as we believe your only-begotten Son our Lord Jesus Christ to have ascended into heaven, so we may also in heart and mind there ascend, and with him continually dwell; who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
               bcpPage: 226
              }
            ],
            traditional: [
              {
                text: 'O Almighty God, whose blessed Son our Savior Jesus Christ ascended far above all heavens that he might fill all things: Mercifully give us faith to perceive that, according to his promise, he abideth with his Church on earth, even unto the end of the ages; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, in glory everlasting.',
                bcpPage: 174
              },
              {
                text: 'O God, who hast prepared for those who love thee such good things as pass man’s understanding: Pour into our hearts such love toward thee, that we, loving thee in all things and above all things, may obtain thy promises, which exceed all that we can desire; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 174
              }
            ]
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '85',
              '86'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: 'Ezek 1:28–3:3',
            second: 'Heb 4:14–5:6',
            gospel: 'Luke 9:28–36'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Ezek 3:4–17',
            second: 'Heb 5:7–14',
            gospel: 'Luke 9:37–50'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 963
      },
      days: [
        {
          day: 'Sunday',
          title: 'Sixth Sunday of Easter',
          psalms: {
            morning: [
              '93',
              '96'
            ],
            evening: [
              '34'
            ]
          },
          lessons: {
            first: 'Lev 25:1–17',
            second: 'Jas 1:2–8, 16–18',
            gospel: 'Luke 12:13–21'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '80'
            ],
            evening: [
              '77',
              '[79]'
            ]
          },
          lessons: {
            first: 'Lev 25:35–55',
            second: 'Col 1:9–14',
            gospel: 'Matt 13:1–16'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '78:1–39'
            ],
            evening: [
              '78:40–72'
            ]
          },
          lessons: {
            first: 'Lev 26:1–20',
            second: '1 Tim 2:1–6',
            gospel: 'Matt 13:18–23'
          }
        },
        {
          day: 'Wednesday',
          title: 'Eve of Ascension',
          psalms: {
            morning: [
              '119:97–120'
            ],
            evening: [
              '68:1–20'
            ]
          },
          lessons: {
            morning: {
              first: 'Lev 26:27–42',
              second: 'Eph 1:1–10',
              gospel: 'Matt 22:41–46'
            },
            evening: {
              first: '2 Kgs 2:1–15',
              second: 'Rev 5:1–14'
            }
          }
        },
        {
          day: 'Thursday',
          title: 'Ascension Day',
          psalms: {
            morning: [
              '8',
              '47'
            ],
            evening: [
              '24',
              '96'
            ]
          },
          lessons: {
            first: 'Dan 7:9–14',
            second: 'Heb 2:5–18',
            gospel: 'Matt 28:1–20'
          },
          collects: {
            contemporary: [
              {
                text: 'Almighty God, whose blessed Son our Savior Jesus Christ ascended far above all heavens that he might fill all things: Mercifully give us faith to perceive that, according to his promise, he abides with his Church on earth, even to the end of the ages; through Jesus Christ our Lord, who lives and reigns with you and the Holy Spirit, one God, in glory everlasting.',
               bcpPage: 226
              },
              {
                text: 'Grant, we pray, Almighty God, that as we believe your only-begotten Son our Lord Jesus Christ to have ascended into heaven, so we may also in heart and mind there ascend, and with him continually dwell; who lives and reigns with you and the Holy Spirit, one God, for ever and ever.',
               bcpPage: 226
              }
            ],
            traditional: [
              {
                text: 'O Almighty God, whose blessed Son our Savior Jesus Christ ascended far above all heavens that he might fill all things: Mercifully give us faith to perceive that, according to his promise, he abideth with his Church on earth, even unto the end of the ages; through the same Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, in glory everlasting.',
                bcpPage: 174
              },
              {
                text: 'O God, who hast prepared for those who love thee such good things as pass man’s understanding: Pour into our hearts such love toward thee, that we, loving thee in all things and above all things, may obtain thy promises, which exceed all that we can desire; through Jesus Christ our Lord, who liveth and reigneth with thee and the Holy Spirit, one God, for ever and ever.',
                bcpPage: 174
              }
            ]
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '85',
              '86'
            ],
            evening: [
              '91',
              '92'
            ]
          },
          lessons: {
            first: '1 Sam 2:1–10',
            second: 'Eph 2:1–10',
            gospel: 'Matt 7:22–27'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '87',
              '90'
            ],
            evening: [
              '136'
            ]
          },
          lessons: {
            first: 'Num 11:16–17, 24–29',
            second: 'Eph 2:11–22',
            gospel: 'Matt 7:28–8:4'
          }
        }
      ]
    }
  ]
}
