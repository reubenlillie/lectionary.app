/**
 * @file Contains global data for the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global data module for the Daily Office Lectionary, Week of 2 Epiphany
 * @module _data/dailyOffice/epiphany2
 * @since 0.1.0
 * @since 2.0.0 Merge Year One and Year Two
 * @since 2.1.0 Add collects
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  props: {
    season: 'Epiphany',
    week: 'Week of the Second Sunday after the Epiphany',
    collects: {
      contemporary: [
        {
          text: 'Almighty God, whose Son our Savior Jesus Christ is the light of the world: Grant that your people, illumined by your Word and Sacraments, may shine with the radiance of Christ’s glory, that he may be known, worshiped, and obeyed to the ends of the earth; through Jesus Christ our Lord, who with you and the Holy Spirit lives and reigns, one God, now and for ever.',
          bcpPage: 215
        }
      ],
      traditional: [
        {
          text: 'Almighty God, whose Son our Savior Jesus Christ is the light of the world: Grant that thy people, illumined by thy Word and Sacraments, may shine with the radiance of Christ’s glory, that he may be known, worshiped, and obeyed to the ends of the earth; through the same Jesus Christ our Lord, who with thee and the Holy Spirit liveth and reigneth, one God, now and for ever.',
          bcpPage: 163
        }
      ]
    }
  },
  years: [
    {
      props: {
        year: 'Year One',
        bcpPage: 944
      },
      days: [
        {
          day: 'Sunday',
          title: 'Second Sunday after the Epiphany',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Isa 43:14–44:5',
            second: 'Heb 6:17–7:10',
            gospel: 'John 4:27–42'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Isa 44:6–8, 21–23',
            second: 'Eph 4:1–16',
            gospel: 'Mark 3:7–19a'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Isa 44:9–20',
            second: 'Eph 4:17–32',
            gospel: 'Mark 3:19b–35'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Isa 44:24–45:7',
            second: 'Eph 5:1–14',
            gospel: 'Mark 4:1–20'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Isa 45:5–17',
            second: 'Eph 5:15–33',
            gospel: 'Mark 4:21–34'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Isa 45:18–25',
            second: 'Eph 6:1–9',
            gospel: 'Mark 4:35–41'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Isa 46:1–13',
            second: 'Eph 6:10–24',
            gospel: 'Mark 5:1–20'
          }
        }
      ]
    },
    {
      props: {
        year: 'Year Two',
        bcpPage: 945
      },
      days: [
        {
          day: 'Sunday',
          title: 'Second Sunday after the Epiphany',
          psalms: {
            morning: [
              '148',
              '149',
              '150'
            ],
            evening: [
              '114',
              '115'
            ]
          },
          lessons: {
            first: 'Gen 7:1–10, 17–23',
            second: 'Eph 4:1–16',
            gospel: 'Mark 3:7–19'
          }
        },
        {
          day: 'Monday',
          psalms: {
            morning: [
              '25'
            ],
            evening: [
              '9',
              '15'
            ]
          },
          lessons: {
            first: 'Gen 8:6–22',
            second: 'Heb 4:14–5:6',
            gospel: 'John 2:23–3:15'
          }
        },
        {
          day: 'Tuesday',
          psalms: {
            morning: [
              '26',
              '28'
            ],
            evening: [
              '36',
              '39'
            ]
          },
          lessons: {
            first: 'Gen 9:1–17',
            second: 'Heb 5:7–14',
            gospel: 'John 3:16–21'
          }
        },
        {
          day: 'Wednesday',
          psalms: {
            morning: [
              '38'
            ],
            evening: [
              '119:25–48'
            ]
          },
          lessons: {
            first: 'Gen 9:18–29',
            second: 'Heb 6:1–12',
            gospel: 'John 3:22–36'
          }
        },
        {
          day: 'Thursday',
          psalms: {
            morning: [
              '37:1–18'
            ],
            evening: [
              '37:19–42'
            ]
          },
          lessons: {
            first: 'Gen 11:1–9',
            second: 'Heb 6:13–20',
            gospel: 'John 4:1–15'
          }
        },
        {
          day: 'Friday',
          psalms: {
            morning: [
              '31'
            ],
            evening: [
              '35'
            ]
          },
          lessons: {
            first: 'Gen 11:27–12:8',
            second: 'Heb 7:1–17',
            gospel: 'John 4:16–26'
          }
        },
        {
          day: 'Saturday',
          psalms: {
            morning: [
              '30',
              '32'
            ],
            evening: [
              '42',
              '43'
            ]
          },
          lessons: {
            first: 'Gen 12:9–13:1',
            second: 'Heb 7:18–28',
            gospel: 'John 4:27–42'
          }
        }
      ]
    }
  ]
}
