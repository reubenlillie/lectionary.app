/**
 * @file Contains global metadata for copyright information
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Global copyright data module
 * @module _data/copyright
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  year: new Date().getFullYear(),
  holder: 'Church of the Nazarene in the Loop',
  license: {
    abbr: 'CC BY-SA',
    name: 'Creative Commons Attribution-ShareAlike 4.0 International license',
  },
  url: 'https://creativecommons.org/licenses/by-sa/4.0/'
}
