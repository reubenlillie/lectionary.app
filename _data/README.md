# `./_data/`

This directory contains [global data files](https://www.11ty.dev/docs/data-global/) to add properties to Eleventy’s global [`data`](https://www.11ty.dev/docs/data/) object. Eleventy makes this data available in all templates.

* `app.js` holds metadata for the app
* `bcp.js` holds metadata about the cited edition of the [_Book of Common Prayer_](https://episcopalchurch.org/files/book_of_common_prayer.pdf)
* `bibleBooks/` holds names and abbreviations for books of the Bible
* `colors.js` holds color codes (HEX and RGB) used in app templates
* `contact.js` holds postal and electronic contact information data for the Lectionary.app team
* `copyright.js` holds copyright and permissions data
* `dailyOffice/` holds entries from the Daily Office Lectionary from the BCP
* `project.js` exposes `ELEVENTY_ENV` [environment variables](https://www.11ty.dev/docs/data-js/#example-exposing-environment-variables) to templates
