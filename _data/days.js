/**
 * @file Contains global data for days
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 */

// Local data
import * as dailyOffice from './dailyOffice/index.js'

// Local template functions
import addDays from '../_includes/functions/add-days.js'
import getEntry from '../_includes/functions/get-entry.js'
import getAdvent from '../_includes/functions/get-advent.js'
import getChristmas from '../_includes/functions/get-christmas.js'
import getEpiphany from '../_includes/functions/get-epiphany.js'
import getAshWednesday from '../_includes/functions/get-ash-wednesday.js'
import computus from '../_includes/functions/computus.js'
import getPentecost from '../_includes/functions/get-pentecost.js'
import getSeason from '../_includes/functions/get-season.js'

/** @type {Object.<Date>} Calculate data properties based on this date */
var today = new Date()

/** @type {number} Four-digit year for today */
var year = today.getFullYear()

/** @type {number} Four-digit year for one year before today */
var lastYear = year - 1

/** @type {number} Four-digit year for one year after today */
var nextYear = year + 1

/** @type {Object.<Date>} Day of Pentecost for the current year */
var pentecost = getPentecost(year)

/** @type {Object.<Date>} Day of Pentecost for the current year */
var nextPentecost = getPentecost(nextYear)

/*
 * Dates for testing
 */
// var today = new Date(2021, 10, 28) // First Sunday of Advent 2021
// var today = new Date(2021, 10, 30) // St. Andrew 2021
// var today = new Date(2021, 11, 24) // Christmas Eve 2021
// var today = new Date(2021, 11, 25) // Christmas Day 2021
// var today = new Date(2022, 0, 1) // Holy Name 2022
// var today = new Date(2022, 0, 5) // Eve of Epiphany 2022
// var today = new Date(2022, 0, 6) // Epiphany 2022
// var today = new Date(2022, 0, 9) // First Sunday after the Epiphany 2022
// var today = new Date(2022, 0, 16) // 2 Epiphany 2022
// var today = new Date(2022, 1, 27) // Last Epiphany 2022
// var today = new Date(2022, 1, 2) // The Presentation 2022
// var today = new Date(2022, 2, 2) // Ash Wednesday 2022
// var today = new Date(2022, 2, 6) // First Sunday in Lent 2022
// var today = new Date(2022, 2, 25) // Annunciation 2022
// var today = new Date(2022, 3, 10) // Palm Sunday 2022
// var today = new Date(2022, 3, 15) // Good Friday 2022
// var today = new Date(2022, 3, 17) // Easter 2022
// var today = new Date(2022, 3, 25) // St. Mark 2022
// var today = new Date(2022, 4, 26) // Ascension 2022
// var today = new Date(2022, 4, 31) // The Visitiation 2022
// var today = new Date(2022, 5, 5) // Pentecost 2022
// var today = new Date(2022, 5, 12) // Trinity 2022
// var today = new Date(2022, 5, 19) // Second Sunday after Pentecost 2022
// var today = new Date(2029, 5, 24) // Nativity of St. John the Baptist

/**
 * Defines a template for a day’s data object
 * @since 0.5.0
 * @param {Object.<Date>} [today=Current date] The date to use
 * @return {Object} Data, including the Daily Office Lectionary entry
 */
function dayTemplate(today = new Date()) {
  return {
    date: today,
    entry: getEntry(dailyOffice, today)
  }
}

/**
 * Global `days` data module
 * @module _data/days
 * @since 0.5.0
 * @since 0.11.0 Add starting and ending dates for liturgical seasons
 * @since 1.0.0 Add season key
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data in Eleventy}
 */
export default {
  today: dayTemplate(today),
  yesterday: dayTemplate(addDays(-1, today)),
  tomorrow: dayTemplate(addDays(1, today)),
  year: year,
  lastYear: lastYear,
  nextYear: nextYear,
  advent: getAdvent(year),
  nextAdvent: getAdvent(nextYear),
  christmas: getChristmas(year),
  lastChristmas: getChristmas(lastYear),
  holyName: new Date(year, 0, 1),
  nextHolyName: new Date(nextYear, 0, 1),
  epiphany: getEpiphany(year),
  nextEpiphany: getEpiphany(nextYear),
  ashWednesday: getAshWednesday(year),
  nextAshWednesday: getAshWednesday(nextYear),
  easter: computus(year),
  nextEaster: computus(nextYear),
  whitMonday: addDays(1, pentecost),
  nextWhitMonday: addDays(1, nextPentecost),
  season: getSeason(today)
}
