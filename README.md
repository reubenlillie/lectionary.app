<div align="center">

<header>

<a href="https://lectionary.app/"><img src="https://lectionary.app/img/logo.svg" alt="Lectionary.app logo" width="150"></a>

# [The Lectionary.app](https://lectionary.app/)

Daily Bible Readings

A [progressive web app](https://web.dev/progressive-web-apps/) for the Daily Office Lectionary

Built with [Eleventy](https://11ty.dev/)

Hosted on [Netlify](https://netlify.com/)

[![Netlify Status](https://api.netlify.com/api/v1/badges/22bc7c25-321d-4bb4-857e-d49a73029872/deploy-status)](https://app.netlify.com/sites/lectionary-app/deploys)

</header>

</div>

## Summary

Daily Bible readings from the _Book of Common Prayer_ updated at midnight UTC.

The [layouts](https://gitlab.com/reubenlillie/lectionary.app/-/tree/master/_includes/layouts) are written entirely in vanilla JavaScript (files with the [`*.11ty.js`](https://www.11ty.dev/docs/languages/javascript/) extension). Eleventy turns these templates into HTML and builds a prerendered copy of the app. 

Bible abbreviations have been adapted to follow the _SBL Handbook of Style_, 2nd ed., from the [Society of Biblical Literature](https://www.sbl-site.org/). See [`_data/bibleBooks/README.md`](https://gitlab.com/reubenlillie/lectionary.app/-/blob/master/_data/bibleBooks/README.md).

## Getting started

Follow these steps to run a local copy of the Lectionary.app on your computer:

### System requirements

The Lectionary.app needs Node.js to run [Eleventy](https://www.11ty.dev/docs/getting-started/) and Pa11y for [accessibility testing](https://lectionary.app/accessibility).

* Instructions for installing [Node.js](https://nodejs.org/)
* Instructions for installing [Pa11y](https://github.com/pa11y/pa11y) and [Pally-CI](https://github.com/pa11y/pa11y-ci) globally

Once you have Node.js and Pa11y installed, enter the following commands into your terminal:

### 1. Clone this repository and all its dependencies

```cli
git clone https://gitlab.com/reubenlillie/lectionary.app.git
```

### 2. Go to the working directory

```cli
cd lectionary.app
```

### 3. Install the project dependencies with [NPM](https://www.npmjs.com/)

```cli
npm install
```

### 4. Build a copy of the app

```cli
npm run build
```

Or serve the app for local development

```cli
npm run dev
```

Or test the build for accessibility and other errors

```cli
npm run test
```

## Publish your own copy

The command `npm run build` will generate a copy of the app files in a `_site` directory, which you can deploy with any hosting service.

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/reubenlillie/lectionary.app)

## Contributing

Please submit any corrections or feature requests as GitLab [issues](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html) or [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/).

&copy; 2021 by [Reuben L. Lillie](https://twitter.com/reubenlillie/)

The [_Book of Common Prayer_](https://www.episcopalchurch.org/files/book_of_common_prayer.pdf) is not and never has been under copyright. The Episcopal Church first ratified the BCP in 1789 and last certified it in 2007.
