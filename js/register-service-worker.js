/**
 * @file Register the app service worker
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @see {@link https://gomakethings.com/writing-your-first-service-worker-with-vanilla-js/ “Writing Your First Service Worker in Vanilla JS” by Chris Ferdinandi}
 */ 

// Immediately invoked function expression
(function () {
  // Client supports service workers
  if(navigator && navigator.serviceWorker) {
    // Register the service worker in the project root
    navigator.serviceWorker.register('/service-worker.min.js');
  }
  return
}())
