/**
 * @file Loads fuller versions of fonts loaded in the first stage
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @since 0.11.0 Add Lato
 * @since 0.11.1 Remove unused font variants from fallback
 * @see {@link https://www.zachleat.com/web/css-tricks-web-fonts/ “Developing a Robust Font Loading Strategy for CSS-Tricks” by Zach Leatherman}
 */

// Immediately invoked function expression
(function () {
  // Client supports CSS Font Loading API
  if('fonts' in document) {
    /*
     * Create a new FontFace object
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/FontFace/FontFace FontFace.FontFace() Constructor on MDN}
     */
    var regular = new FontFace('Lato', 'url(/fonts/lato/Lato-Regular-hint-all.woff2) format("woff2")')
    
    /*
     * Load font faces into the DOM 
     */
    Promise.all([regular.load()]).then(fonts =>
      fonts.forEach(font => document.fonts.add(font)))
  }
  
  /*
   * Fallback for browsers that don’t support the CSS Font Loading API by
   * injecting @font-face CSS at-rules
   * @see {@link https://caniuse.com/#feat=font-loading CSS Font Loading on “Can I Use…”}
   */
  if(!('fonts' in document) && 'head' in document) {
    var style = document.createElement('style')
    style.innerHTML = '@font-face {font-family: /fonts/lato/Lato; src: url(/fonts/lato/Lato-Regular-hint-all.woff2);}'
    document.head.appendChild(style)
  }

  return
}())
