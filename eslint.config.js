import pluginJs from '@eslint/js'


export default [
  pluginJs.configs.recommended,
 {
    rules: {
      eqeqeq: "off",
      "no-unused-vars": "warn",
      "no-undef": "warn"
    }
  }
]
