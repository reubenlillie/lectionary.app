/**
 * @file Defines a shortcode the for displaying a formatted date
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * HTML `<time>` markup
 * @module _includes/shortcodes/page-date
 * @since 0.5.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered shortcode
 * @example `${this.pageDate(data)}`
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 */
export default ({page: {date}, app: {locale}}, options) => {
  /** @type {Object} Merge options */
  options = {...locale.dateOptions, ...options}

  // Set UTC date
  date = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getTimezoneOffset() / 60, 0, 0, 0))

  return `<!-- ./_includes/shortcodes/page-date.js -->
<time>${date.toLocaleDateString(locale, options)}</time>`
}
