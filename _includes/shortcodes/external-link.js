/**
 * @file Defines a shortcode to format external links
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.2.0
 */

/**
 * Define markup for links to external content
 * @module _includes/shortcodes/external-link
 * @param {string} link The `<a>` element to format
 * @return {string} HTML An external link
 * @example <caption>Example shortcode in an Eleventy template</caption>
 * `${this.externalLink(link)}`
 */
export default link => `<!--_includes/shortcodes/external-link.js--><span data-link="external">${link}<sup data-emoji="link"></sup></span>`
