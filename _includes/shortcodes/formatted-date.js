/**
 * @file Defines a filter for formatting a date.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Define markup
 * @module _includes/shortcodes/formatted-date
 * @since 0.11.0
 * @param {Object<Date>} day Unfromatted date string
 * @param {Object} end Localization data, including a localde code and formatting options
 * @return {string} HTML
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString “Date.prototype.toISOString” on MDN}
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat “Intl.DateTimeFormat” on MDN}
 * @example 
 * // In an Eleventy template
 * `${this.dateRange(startDate, endDate)}`
 */
export default (day, locale) => `<!--_includes/shortcodes/formatted-date.js--><time datetime="${day.toISOString()}">${new Intl.DateTimeFormat(locale.code, locale.dateOptions)
  .format(new Date(day))}</time>`
