/**
 * @file Defines a template for lectionary entries
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.5.0
 * @since 2.1.0 Add `collectsTemplate()`
 * @since 2.2.0 Add external link to BibleGateway
 */

// Import local data
import bcp from '../../_data/bcp.js'

/*
 * Import local filters
 * @since 2.2.0
 */
import emojiById from '../filters/emoji-by-id.js'
import toBibleReferenceString from '../filters/to-bible-reference-string.js'

/*
 * Import local shortcodes
 * @since 2.2.0
 */
import externalLink from './external-link.js'
import searchBibleGateway from './search-bible-gateway.js'

/**
 * Defines a template for collects
 * @since 2.1.0
 * @param {Object[Array]} collects An array of collects
 * @param {string|null} [show=contemporary] The default collects to show
 * @return {string} HTML defining the daily collect or collects
 */
function collectsTemplate(collects, show = 'contemporary') {
  /**
   * Defines markup for a single collect
   * @since 2.1.0
   * @param {Object} collect A destructured collect object
   * @param {number} [index=0] Index in collects array, defaults to 0
   * @return {string} HTML for the collect text
   */
  var item = ({text, bcpPage}, index = 0) => `<!--item(text, ${bcpPage})-->
    <p data-collect-index="${index}"
       data-bcp="${bcpPage}">
      <span>${text}</span>
      <span><em>Amen.</em></span></p>`

  /**
   * Defines markup for citing the BCP
   * @since 2.1.0
   * @param {number} page BCP page citation
   * @return {string} HTML citing a page of the BCP
   * @see _data/bcp
   */
  var cite = page => `<p class="x-small text-inline-end text-gray">From the ${externalLink(`<a href="${bcp.pdf}#page=${page}"><cite>Book of Common Prayer</cite></a>`)}</p>`

  return `<!--collectsTemplate()-->
<details class="prayer">
  <summary>Prayer</summary>
  ${collects[show].length > 1
    ? `<!--More than one collect to show--><ul class="no-list-style">
<li data-collect="${show}">${collects[show].map((collect, index, arr) =>
  `${item(collect)}
  <!--Check if this is the last collect in the array-->
  ${index === arr.length -1
    ? `<!--Cite BCP after last collect-->${cite(collect.bcpPage)}`
    : '<!--Not last collect-->'}`
  ).join('<span class="smaller text-gray"><em>or this</em></span>')}
</li>
</ul>`
    : `<!--Only one collect to show-->
${item(collects[show][0])}
${cite(collects[show][0].bcpPage)}`
  }
</details>`
}

/**
 * Defines a template for psalm references
 * @since 0.11.0
 * @param {Object[Array]} psalms An array of psalm references
 * @param {string|null} hour The time of day for the psalms
 * @return {string} HTML defining list items for the psalms
 */
function psalmsTemplate(psalms, hour = null) {
  return `<!--psalmsTemplate(${psalms})-->
<li data-hour="${hour}"
  data-reading="psalm">Ps ${psalms.map((psalm, index) =>
  `<span data-psalm-index="${index}">${psalm}</span>`).join(', ')}
</li>`
}

/**
 * Defines a template for lesson references
 * @since 0.11.0
 * @param {Object[Array]} lessons An array of lectionary readings
 * @param {string|null} hour The time of day for the lessons
 * @return {string} HTML defining list items for the lessons
 */
function lessonsTemplate(lessons, hour = null) {
  return `<!--lessonsTemplate(${lessons})-->
${lessons.map((lesson, index) => `<!--${lesson}-->
<li ${hour !== null ? `data-hour="${hour}"` : ''}
  data-reading="${lesson[0]}"
  data-reading-index="${index}"><span>${lesson[1]}</span>
</li>`)
  .join('\n')}`
}

/**
 * The rendered template
 * @module _includes/shortcodes/entry
 * @since 0.5.0
 * @since 0.11.0 Refactored to a single unordered list with `data-hour` and `data-reading` attributes
 * @since 2.1.0 Add collects before readings
 * @since 2.2.0 Add external link to BibleGateway
 * @param {Object} Eleventy’s `data` object
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @return {string} HTML
 * @example <caption>Example shortcode in an Eleventy template</caption>
 * `${this.entry(data)}`
 */
export default ({entry: {collects, psalms, lessons}}) => {
  /** @type {string} List references to pass as a search query */
  var searchString = toBibleReferenceString({psalms, lessons})

  return `<!--_includes/shortcodes/entry.js-->
${collects
  ? `<!--Collects-->${collectsTemplate(collects)}`
  : '<!--No collects-->'}
<ul class="emoji-list hanging-indent">
  ${psalms.morning
    ? `<!--Morning psalm-->${psalmsTemplate(psalms.morning, 'morning')}`
    : '<!--No morning psalm-->'}
  ${lessons.morning
    ? `<!--Morning lessons-->${lessonsTemplate(Object.entries(lessons.morning), 'morning')}`
    : `<!--Lessons-->${lessonsTemplate(Object.entries(lessons))}`}
  ${lessons.evening
    ? `<!--Evening lessons-->${lessonsTemplate(Object.entries(lessons.evening), 'evening')}`
    : `<!--No distinction between morning and evening lessons-->`}
  ${psalms.evening
    ? `<!--Evening psalm-->${psalmsTemplate(psalms.evening, 'evening')}`
    : '<!--No evening psalm-->'}
</ul>
<p class="small">${emojiById('open-book')}&nbsp;${searchBibleGateway(searchString, {}, 'Read on BibleGateway')}</p>`
}
