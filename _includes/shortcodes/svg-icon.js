/**
 * @file Defines a shortcode for SVG icons (like the app logo)
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import fileToString from '../filters/file-to-string.js'

/**
 * Define markup for SVG icons accompanying navigation items
 * @module _includes/shortcodes/nav-item
 * @since 1.0.0 Renamed from `iconTemplate()` in `../shortcodes/nav-item.js`
 * @param {string} icon Subset of Eleventy `data` passed from `default`
 * @return {string} HTML
 */
export default icon => icon
  // Otherwise, use the SVG from the icons sprite
  ? `<!--./_includes/shortcodes/svg-icon.js-->
    <svg class="icon icon-${icon}" width="16" height="16">
      <use href="/icons.svg#${icon}"></use>
    </svg>`
  // Fall back comment for when there is no icon
  : '<!--No icon-->'
