/**
 * @file Defines a shortcode for the app contact information
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * HTML `<address>` markup
 * @module _includes/shortcodes/internal-css
 * @since 0.5.0
 * @since 1.0.0 Remove email address
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered shortcode
 * @example `${this.contactInfo(data)}`
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 */
export default ({app, copyright, contact: {address}}) => `<!--./_includes/shortcodes/contact.js-->
<address>
  ${copyright.holder}<br>
  Attn: ${app.name}<br>
  ${address.street}<br>
  ${address.city}, ${address.state} ${address.zip}
</address>`
