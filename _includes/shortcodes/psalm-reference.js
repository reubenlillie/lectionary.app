/**
 * @file Defines a template for psalms references from a lectionary entry.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

// Local filters
import wbrify from './../filters/wbrify.js'

/**
 * Defines markup for psalms references, say, to include in a list or table
 * @module _includes/shortcodes/lesson-reference
 * @since 0.11.0
 * @param {Object[]} psalms References from a lectionary entry
 * @param {string} type Object key the type of psalms (morning or evening)
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @return {string} HTML
 * @example 
 * // As a list item
 * `<li>${psalmReference(psalms.morning, 'morning')}</li>`
 */
export default (psalms, type) => Array.isArray(psalms) 
  ? `<!--_includes/shortcodes/psalm-reference.js--><span data-psalms-type="${type}">${psalms.map((ps, index, arr) => `${wbrify(ps)}`).join(', ')}</span>`
  : `<!--Double check there actually is no ${type} psalm-->`
