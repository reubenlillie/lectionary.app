/**
 * @file Defines a template for previous/next reading navigation links
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

// Import local modules
import isSameDate from '../functions/is-same-date.js'

/**
 * Defines the default template for previous/next links
 * @param {Object} [options={}] Override template default settings
 * @since 0.5.0
 */
function template(options = {}) {
  /** @type {Object} Default link options */
  var defaults = {
    url: '/', 
    textContent: 'Today', 
    before: '', 
    after: '',
    column: 1
  }

  /** @type {Object} Merge options with defaults */
  var settings = {...defaults, ...options}
  
  /** @type {Object} Destructure the merged settings object */
  var {url, textContent, before, after, column} = settings

  // Define a link with the proper settings
  return `<a ${column === 2 
    ? `style=grid-column:2;justify-self:end;` 
    : ''} 
    href="${url}">
    <span>${before ? before : ''}&nbsp;${textContent}&nbsp;${after ? after : ''}</span>
  </a>`
}

/**
 * Defines the link for the previous reading
 * @since 0.5.0
 * @param {Object} Eleventy’s `data` object 
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 */
function prevLink({today: pageDate, days: {today, tomorrow}}) {
  var icon = '⏪'
  return isSameDate(pageDate, today.date)
    ? template({url: '/yesterday/', textContent: 'Yesterday', before: icon})
    : isSameDate(pageDate, tomorrow.date)
      ? template({before: icon})
      : ''
}

/**
 * Defines the link for the next reading
 * @since 0.5.0
 * @param {Object} Eleventy’s `data` object 
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 */
function nextLink({today: pageDate, days: {today, yesterday}}) {
  var icon = '⏩'
  return isSameDate(pageDate, today.date)
    ? template({url: '/tomorrow/', textContent: 'Tomorrow', after: icon, column: 2})
    : isSameDate(pageDate, yesterday.date)
      ? template({after: icon, column: 2})
      : ''
}

/**
 * The rendered template
 * @module _includes/shortcodes/prev-next-nav
 * @since 0.5.0
 * @param {Object} Eleventy’s `data` object 
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @return {string} HTML
 * @example <caption>Example shortcode in an Eleventy template</caption>
 * `${this.prevNextNav(data)}`
 */
export default data => `<!-- ./_includes/shortcodes/prev-next-nav.js -->
<nav id="prev_next_nav" 
     style="grid-template-columns:1fr 1fr;" 
     class="grid space-between margin-block-start">
  ${prevLink(data)}
  ${nextLink(data)}
</nav>`
