/**
 * @file Defines a template for a row within a table of lectionary entries.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 * @since 1.1.0 Add `entryTitle`
 * @since 2.2.0 Add external links to BibleGateway
 */

// Local filters
import toBibleReferenceString from '../filters/to-bible-reference-string.js'

// Local template functions
import addDays from '../functions/add-days.js'
import getColor from '../functions/get-color.js'
import getWeekday from '../functions/get-weekday.js'
import getPentecost from '../functions/get-pentecost.js'
import isSameDate from '../functions/is-same-date.js'

// Local shortcodes
import formattedDate from './formatted-date.js'
import lessonReference from './lesson-reference.js'
import psalmReference from './psalm-reference.js'
import searchBibleGateway from './search-bible-gateway.js'

/**
 * The rendered template
 * @module _includes/shortcodes/entry-row
 * @since 0.11.0
 * @param {Object} day Contains a `Date` object and a lectionary entry
 * @param {string} locale Localization data
 * @param {Object} dateOptions Formatting options for localized date strings
 * @return {string} HTML
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @example <caption>Example shortcode in an Eleventy template</caption>
 * `${this.entryRow(day.locale)}`
 */
export default ({
  today,
  entry: {
    season,
    week,
    bcpPage,
    day,
    title,
    color,
    psalms,
    lessons
  }
}, locale) => {
  /** @type {string} Liturgical color for today */
  var liturgicalColor = getColor(color, season)

  /** @type {number} Four-digit year for today */
  var year = today.getFullYear()

  /** @type {string} Weekday name for today */
  var weekday = getWeekday(today)

  /** @type {Object.<Date>} Day of Pentecost for the current year */
  var pentecost = getPentecost(year)

  /** @type {string} Is the title property an object? */
  var entryTitle = typeof(title) === 'object'
    // Then format use the shortened version for tables
    ? title.main
    // Otherwise it’s a basic string
    : title

  /**
   * Determine whether or not to display the number of a proper
   * @param {Object.<Date>} today Date to check
   * @return {boolean}
   */
  function showProperNumber(today) {
    // Is today in after Pentecost
    return season === 'The Season after Pentecost'
      // And is today either Whit Monday?
      && (isSameDate(today, addDays(1, pentecost))
        // Or the Monday after Trinity?
        || isSameDate(today, addDays(8, pentecost))
        // Or a Sunday?
        || weekday === 'Sunday')
      // And is there a proper number to display?
      && week
        ? true
        : false
  }

  return `<!--_/includes/shortcodes/entry-row.js-->
<tr data-weekday="${weekday}" data-bcp="${bcpPage}">
  <th scope="row" class="slim-column">
    <span>${title
      ? entryTitle
      : day.includes('Dec') || day.includes('Jan')
        ? weekday
        : day }</span><br>
    ${showProperNumber(today)
        ? `<!--Proper number--><span class="x-small">(${week})</span><br>`
        : title
          ? !/\b((Mon|Tues|Wed(nes)?|Thur(s)?|Fri|Sat(ur)?|Sun)(day)?)\b/.test(title)
            ? `<!--Weekday--><span class="x-small">(${weekday})</span><br>`
            : '<!--Neither weekday nor proper shown-->'
          : '<!--Proper number not shown-->'}
    <span data-color="${liturgicalColor}" class="x-small">${formattedDate(today, locale)}</span><br>
    <span data-emoji="open-book"
          class="xx-small">${searchBibleGateway(toBibleReferenceString({psalms, lessons}), {}, 'BibleGateway')}</span>
  </th>
  <td>${psalmReference(psalms.morning, 'morning')}</td>
  <td>${lessonReference(lessons, 'first')}</td>
  <td>${lessonReference(lessons, 'second')}</td>
  <td>${lessonReference(lessons, 'gospel')}</td>
  <td>${psalmReference(psalms.evening, 'evening')}</td>
</tr>`
}
