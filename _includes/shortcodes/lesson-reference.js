/**
 * @file Defines a template for a lesson reference from a lectionary entry.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

// Local filters
import wbrify from './../filters/wbrify.js'

/**
 * Defines markup for a lesson reference, say, to include in a list or table
 * @module _includes/shortcodes/lesson-reference
 * @since 0.11.0
 * @param {Object} lessons References from a lectionary entry
 * @param {string} type Object key for a type of lesson
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @return {string} HTML
 * @example 
 * // As a list item
 * `<li>${lessonReference(lessons, 'first')}</li>`
 */
export default (lessons, type) => {
  /** @type {string} Object key for an alternate lesson (e.g., altFirst) */
  var alt = `alt${type.charAt(0).toUpperCase() + type.slice(1)}`

  return `<!--lessonReference(${JSON.stringify(lessons)}, ${type})-->
${lessons[type]
  ? `<span data-lesson-type="${type}">${wbrify(lessons[type])}</span>
    ${lessons[alt]
      ? `<br><span data-lesson-type="${alt}">${wbrify(lessons[alt])}</span>`
      : '<!--No ${alt} lesson-->'}`
  : lessons.morning || lessons.evening
    ? `${lessons.morning[type]
      ? `<span data-lesson-type="morning-${type}">${wbrify(lessons.morning[type])}</span>
        ${lessons.morning[alt]
          ? `<br><span data-lesson-type="morning-${alt}">${wbrify(lessons.morning[alt])}</span>`
          : '<!--No morning ${alt}-->'}` 
      : '<!--No morning ${type}-->'} 
      ${lessons.evening[type]
        ? `${lessons.morning[type] ? '<br>' : ''}<span data-lesson-type="evening-${type}">${wbrify(lessons.evening[type])}</span>
          ${lessons.evening[alt]
            ? `<br><span data-lesson-type="evening-${alt}">${wbrify(lessons.evening[alt])}</span>`
            : '<!--No evening ${alt}-->'}`
        : '<!--No evening ${type}-->'}`
    : '<!--Double check there actually is no ${type} lesson-->'
  }`
}
