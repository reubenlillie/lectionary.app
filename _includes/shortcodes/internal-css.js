/**
 * @file Defines a shortcode for loading internal stylesheets
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import fileToString from '../filters/file-to-string.js'

/**
 * HTML `<style>` markup
 * @module _includes/shortcodes/internal-css
 * @since 0.5.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered shortcode
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties Using CSS custom properties (variables) on MDN}
 * @example 
 * // In an Eleventy template
 * `${this.internalCSS(data)}`
 */
export default ({
  colors: {red, green, pink, purple, black, darkGray, darkerGray, mediumGray, lighterGray, lightGray, white},
  emoji: {symbols},
  hasTable
}) => {
  /** @type {string} CSS custom properties */
  var root = `/* ./_includes/shortcodes/internal-css.js */
    :root {
      /* Color Codes */
      --red: ${red.rgb};
      --green: ${green.rgb};
      --pink: ${pink.rgb};
      --purple: ${purple.rgb};
      --white: ${white.rgb};
      --lightGray: ${lightGray.rgb};
      --lighterGray: ${lighterGray.rgb};
      --mediumGray: ${mediumGray.rgb};
      --darkerGray: ${darkGray.rgb};
      --darkGray: ${darkGray.rgb};
      --black: ${black.rgb};
      /* Named Properties */
      --background-color: rgb(var(--white));
      --text-color: rgb(var(--black));
      --background-gray: rgb(var(--lighterGray));
      --text-gray: rgb(var(--darkerGray));
      --border: 1px solid var(--background-gray);
      --table-row-gray: rgb(var(--lightGray));
      /* Emoji */
      ${symbols.map(({id, emoji}) => `--${id}-emoji: "${emoji}";`)
        .join('\n')}
    }
    @media (prefers-color-scheme: dark) {
      :root {
        --background-color: rgb(var(--black));
        --text-color: rgb(var(--white));
        --background-gray: rgb(var(--darkerGray));
        --text-gray: rgb(var(--lighterGray));
        --table-row-gray: rgb(var(--darkGray));
      }
    }`

  /** @type {string} file path for fonts stylesheet */
  var fonts = 'css/fonts.css'

  /** @type {string} file path for layout stylesheet */
  var layout = 'css/layout.css'

  /** @type {string} file path for typography stylesheet */
  var typography = 'css/typography.css'

  /** @type {string} file path for colors stylesheet */
  var colors = 'css/colors.css'

  /** @type {string} file path for tables stylesheet */
  var tables = 'css/tables.css'

  /** @type {string} file path for forms stylesheet */
  var forms = 'css/forms.css'

  /** @type {string} Placeholder for CSS to be added internally */
  var css = ''

  // Order the file contents for internal CSS
  css += `/* ./${fonts} */ ${fileToString(fonts)}`
  css += `/* ./${layout} */ ${fileToString(layout)}`
  css += `/* ./${typography} */ ${fileToString(typography)}`
  css += `/* ./${colors} */ ${fileToString(colors)}`
  css += `/* ./${forms} */ ${fileToString(forms)}`

  css+= hasTable
    ? `/* ./tables */ ${fileToString(tables)}`
    : '/* This page has no table */'

  return root + css
}
