/**
 * @file Defines a shortcode for navigation links
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.6.0
 * @since 1.0.0 Move private `iconTemplate()` method to `svgIcon()`
 */

import svgIcon from '../shortcodes/svg-icon.js'

/**
 * Define markup for a navigation item
 * @module _includes/shortcodes/nav-item
 * @since 0.6.0
 * @since 0.9.0 Add `use` elements for SVG icons
 * @since 0.11.0 Add `iconTemplate()`
 * @param {Object} item Subset of Eleventy `data`
 * @return {string} HTML
 */
export default ({data: {attr, element, emojiId, icon, navTitle, url}, ...rest}) => `<!--./_includes/shortcodes/nav-item.js-->
<li ${emojiId ? `data-emoji="${emojiId}"` : ''}>
  <${element ? element : 'a'} class="flex flex-column no-padding" ${attr
    ? attr
    : `href="${url ? url : rest.url}"`}>
    ${!emojiId ? svgIcon(icon) : ''}
    <span>${navTitle}</span>
  </${element ? element : 'a'}>
</li>`
