/**
 * @file Defines a shortcode for linking to BibleGateway’s search page
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.2.0
 */

// Import local modules
import bibleGateway from '../../_data/bible-gateway.js'
import externalLink from './external-link.js'

/**
 * Define markup for linking to BibleGateway’s search page
 * @module _includes/shortcodes/search-bible-gateway
 * @since 2.2.0
 * @param {string} reference Biblical reference to search on BibleGateway.com
 * @param {object} [options={}] Query parameters
 * @param {string} [linkText=null] Link text in place of reference
 * @return {string} HTML an external link to search BibleGateway.com
 * @example <caption>Example shortcode in an Eleventy template</caption>
 * `${this.searchBibleGateway(reference, {}, 'Read on BibleGateway')}`
 */
export default (reference, options = {}, linkText = null) => {
  // Merge options with global defaults
  options = Object.assign(bibleGateway.queryParameters, options)

  /** @type {string} Does the reference does not begin with a letter (i.e., a Bible book name)? */
  var searchString = /[^a-zA-Z]/.test(reference.charAt(0))
    // Then search Psalms
    ? `Ps ${reference}`
    // Otherwise it’s a string
    : reference

  /** @type {Object} Does the options array have at least one property? */
  var parameters = Object.keys(options)
    // Chain the parameters
    ? Object.entries(options).map(param =>
        // Create a string from the key and value (e.g., '&version=NRSVUE')
        `&${param[0]}=${param[1]}`
      ).join('')
    // No query parameters to attach to the string
    : ''

  /* @type {string} HTML for the search link */
  var link = `<a target="_blank"
    rel="noopener"
    href="${bibleGateway.queryStringPefix}${encodeURIComponent(searchString)}${parameters}">${linkText
      ? linkText
      : reference}</a>`

  return `<!--_includes/shortcodes/search-bible-gateway.js--><span data-search="BibleGateway">${externalLink(link)}<span>`
}
