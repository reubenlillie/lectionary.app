/**
 * @file The chained layout for the liturgical season pages.
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.11.0
 * @since 2.2.0 Simplify information about the season table
 */

// Local modules
import addDays from '../functions/add-days.js'

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  layout: 'layouts/page'
}

/**
 * The content of the template
 * @since 0.11.0
 * @since 1.0.0 Use `addDays` to make `getDateRange` inclusive of `endDate`
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
export function render(data) {
  /** @type {Object} Destructure a portion of `data` */
  var {
    app: {locale},
    emoji: {symbols},
    title,
    content,
    description,
    lectionary,
    startDate,
    endDate,
    today,
  } = data

  /** @type {Object[]} Dates in the season */
  var dates = this.getDateRange(startDate, addDays(1, endDate))

  /** @type {Object[]} Lectionary entries for the season */
  var season = dates.map(day => {
    return {
      today: day,
      entry: this.getEntry(lectionary, day)
    }
  }, this)

  /** @type {string} Name of the liturgical season */
  var seasonName = season[0]?.entry?.season

  /** @type {string} Name of the liturgical year */
  var lectionaryYear = season[0]?.entry?.year

  var tableName = `${seasonName}${lectionaryYear}`

  // Make “The” lowercase for “the Season after Pentecost”
  seasonName = seasonName?.charAt(0) === 'T'
    ? seasonName.charAt(0).toLowerCase() + seasonName.substring(1)
    : seasonName

  return `<!--_/includes/layouts/season.11ty.js-->
<section id="content">
  ${content ? content : `<p>${description}</p>`}
  <blockquote>
    <p>${this.emojiById('information')} On smaller screens, you can scroll both horizontally and vertically to read across rows and columns.</p>
  </blockquote>
</section>
<div role="region" aria-labelledby="seasonTable" tabindex="0">
  <table id="seasonTable" class="hanging-indent small row-borders">
    <caption>Daily Office Lectionary Readings for ${seasonName} (${lectionaryYear})</caption>
    <thead>
      <tr>
        <th scope="col" class="slim-column">${this.emojiById('calendar')} Day</th>
        <th scope="col" class="slim-column">${this.emojiById('morning')} Morning<br>${this.emojiById('psalm')} Psalm</th>  
        <th scope="col" class="slim-column">${this.emojiById('first')} First</th>
        <th scope="col" class="slim-column">${this.emojiById('second')} Second</th>
        <th scope="col" class="slim-column">${this.emojiById('gospel')} Gospel</th>
        <th scope="col" class="slim-column">${this.emojiById('evening')} Evening<br>${this.emojiById('psalm')} Psalm</th>  
      </tr>
    </thead>
    <tbody>
      ${season.map(day => this.entryRow(day, locale), this).join('\n')}
    </tbody>
    <tfoot class="x-small">
      <tr>
        <td colspan="6" class="padding-block-start"><span id="${tableName}TableNote"><i>Note:</i> References follow <a href="/abbreviations/">abbreviations</a> from the Society of Biblical Literature (<abbr>SBL</abbr>).</span></td>
      </tr>
    <tfoot>
  </table>
<div>`
}
