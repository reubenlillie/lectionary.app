/**
 * @file The chained layout for the lectionary entry
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.5.0
 * @since 0.11.0 Destructure app data for better date formatting
 * @since 1.1.0 Add `titleTemplate()`, `titleH1()`, and `entryTitle`
 * @since 2.2.0 Move emoji outside anchor tags
 * @see {@link https://www.11ty.dev/docs/layout-chaining/ Layout chaining in Eleventy}
 */

 /**
  * Acts as front matter data in JavaScript template files
  * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
  */
export var data = {
  layout: 'layouts/main'
}

/**
 * Defines markup for the entry title as an first-level heading
 * @since 1.1.0
 * @param title {Object|string} Entry title string
 * @return {string} HTML
 */
var titleH1 = title => `<!--titleH1() in ./_includes/layouts/entry.11ty.js-->
<h1 class="no-margin flex flex-column justify-end">${title}</h1>`

/**
 * Defines markup for the entry title based on if it’s an object or string
 * @since 1.1.0
 * @param title {Object|string} Entry title data
 * @return {string} HTML
 * @see Steve Faulkner (2022) {@link https://www.tpgi.com/subheadings-subtitles-alternative-titles-and-taglines-in-html/ “Subheadings, subtitles, alternative titles and taglines in HTML”}, TPGi (July 19, 2022).
 */
function titleTemplate(title) {
  /** @type {string} Placeholder for HTML */
  var html

  // Is the title an object?
  html = typeof(title) === 'object'
    // Does the object has a sub property?
    ? title.sub
      // Use an accessible <hgroup> for the object properties
      ? `<!--Entry title has sub property-->
<hgroup role="group" aria-roledescription="Heading group">
  ${titleH1(title.main)}
  <p aria-roledescription="subtitle" class="no-margin smaller">${title.sub}</p>
</hgroup>`
      // Use the main property as the title without an hgroup
      : `<!--Entry title has no sub property-->${titleH1(title.main)}`
    // Otherwise the title is a string
    : `<!--Entry title is a string-->${titleH1(title)}`

  return `<!--titleTemplate() in ./_includes/layouts/entry.11ty.js-->${html}`
}

/**
 * Defines markup for lectionary entries
 * @param {Object} data 11ty’s `data` object
 * @return {string} HTML
 */
export function render(data) {
  /** @type {Object} Destructure a portion of `data` */
  var {
    app: {locale},
    content,
    entry: {bcpPage, color, season, title, week, year},
    today
  } = data

  /** @type {string} Is there a title property (like some Sundays or Holy Days)? */
  var entryTitle = title
    // Format the title markup
    ? titleTemplate(title)
    // Then, is there a week property? (Hint: there should be.)
    : week
      ? titleTemplate(week)
      // Otherwise, something is wrong with the entry data
      : '<!--No title or week proper for entry-->'

  return `<!--_includes/layouts/entry.11ty.js-->
<article class="entry">
  <header class="padding text-center">
    ${entryTitle}
    <p data-color="${this.getColor(color, season)}"
      class="no-margin text-center smaller">${this.getWeekday(today)}, ${this.formattedDate(today, locale)}</p>
  </header>
  <div class="flex flex-column">
    ${this.prevNextNav(data)}
    ${this.entry(data)}
  </div>
  ${content}
  <p class="small">${this.emojiById('key')}&nbsp;<a href="/abbreviations/">Abbreviations</a></p>
  ${bcpPage
    ? `<footer class="text-center smaller">
    <p>${this.emojiById('calendar')}&nbsp;<a href="/what-is-the-lectionary/">Daily Office Lectionary ${year}</a></p>
  </footer>
</article>`
    : '</article>'
  }`
}
