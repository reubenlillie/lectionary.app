/**
 * @file The chained layout for the `<main>` element
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.5.0
 * @see {@link https://www.11ty.dev/docs/layout-chaining/ Layout chaining in Eleventy}
 */

/**
 * Acts as front matter data in JavaScript template files
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  layout: 'layouts/base',
  eleventyComputed: {
    today: data => data.days.today.date,
    entry: data => data.days.today.entry
  }
}

/**
 * Defines markup for the `<main>`
 * @since 0.5.0
 * @param {Object} data 11ty’s `data` object
 * @return {string} HTML
 */
export function render({content, entry: {season}}) {
  return `<!--./layouts/main.11ty.js-->
<main id="main" class="flex flex-column"
  ${season
    ? `data-season="${season.split(' ').pop().toLowerCase()}"`
    : ''}>
  ${content}
</main>`
}
