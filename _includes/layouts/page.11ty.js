/**
 * @file The chained layout for pages
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/layout-chaining/ Layout chaining in Eleventy}
 */

 /**
  * Acts as data in JavaScript template files
  * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
  */
export var data = {
  layout: 'layouts/main'
}

/**
 * Defines markup for pages
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render({content, emojiId, title}) {
  return `<!--_includes/layout/page.11ty.js-->
<article class="page">
  ${title
    ? `<header class="padding text-center">
      <h1 class="no-margin flex flex-column justify-end">${emojiId
        ? this.emojiById(emojiId)
        : '<!--No emoji-->'}
        ${title}</h1>
    </header>`
    : ''
  }
  ${content}
</article>`
}
