/**
 * @file The base layout
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.1.0
 * @see {@link https://www.11ty.dev/docs/layouts/ Layouts in Eleventy}
 */

 /**
  * Acts as front matter data in JavaScript template files
  * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
  */
export var data = {
  description: ""
}

/**
 * Defines markup for the base template
 * @param {Object} data Elevent’s `data` object
 * @return {string} HTML
 */
export async function render(data) {
  /** @type {Object} Destructure a portion of `data` */
  var {
    app: {baseUrl, locale, name},
    collections: {footerNav, nav},
    content,
    description,
    donateButton,
    page: {fileSlug},
    pkg,
    tags,
    title
  } = data

  // Use the description from `package.json` as a fallback
  description = description ? description : pkg.description

  return `<!--./_includes/layout/base.11ty.js-->
<!DOCTYPE html>
<html lang="${locale.code}">
  <head>
    <title>${title ? title : name}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="${description}">
    <meta property="og:title" content="${title ? title : name}">
    <meta property="og:description" content="${description}">
    <meta property="og:url" content="${baseUrl}/index.html">
    <meta property="og:type" content="website">
    <meta property="og:image" content='${baseUrl}/img/og.png'>
    <meta property="og:image:alt" content="Lectionary.app">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@LectionaryApp">
    <meta name="twitter:description" content="${description}">
    <meta name="twitter:image" content='${baseUrl}/img/og.png'>
    ${this.fileToString('favicons/html_code.html')}
    <link rel="preload" href="/fonts/lato/Lato-Regular-kern-latin.woff2" as="font" type="font/woff2" crossorigin>
    <script>${await this.minifyJs(this.fileToString('js/font-loading-second-stage.js'))}</script>
    <style>${this.minifyCss(this.internalCss(data))}</style>
  </head>
  <body ${tags ? `data-tags="${tags.map(tag => `${tag}`).join(' ')}"` : ''}
    data-page="${fileSlug ? fileSlug : 'home' }"
    class="flex flex-column no-margin">
    <a class="screen-reader" href="#main">Skip to content.</a>
    <header id="app_header" class="text-center">
      <nav id="app_nav" aria-label="primary-navigation">
        <ul class="flex no-padding smaller">
          ${nav
            .sort((a, b) => a.data.weight - b.data.weight)
            .map(item => this.navItem(item), this).join('\n')}
        </ul>
      </nav>
    </header>
    ${content}
    <footer id="app_footer" class="margin">
      <nav id="footer_nav" aria-label="footer-navigation">
        <ul class="emoji-list flex no-padding small">
          <li>
            <a href="/"><svg width="10" height="10"><use href="/icons.svg#logo"></use></svg>&nbsp;${name}</a>
          </li>
          ${footerNav.sort((a, b) => a.data.weight - b.data.weight)
            .map(item => this.navItem(item), this).join('\n')}
        </ul>
      </nav>
    </footer>
    <script src="https://twemoji.maxcdn.com/v/latest/twemoji.min.js" crossorigin="anonymous"></script>
    ${donateButton
      ? `<!--donateButton--><script src="https://zeffy-scripts.s3.ca-central-1.amazonaws.com/embed-form-script.min.js" async></script>`
      : '<!--No donate Button on this page-->'}
    <script>${await this.minifyJs(this.fileToString('js/register-service-worker.js'))}</script>
  </body>
</html>`
}
