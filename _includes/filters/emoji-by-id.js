/**
 * @file Defines a filter for selecting emoji from global data
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

// Global data
import emoji from '../../_data/emoji.js'

/**
 * Select an emoji by it’s id in `./_data/emojis.js`
 * @module _includes/filters/emoji-by-id
 * @since 0.11.0
 * @param {string} id The id for the emoji
 * @return {string} The first matching emoji
 * @see {@link https://www.11ty.dev/docs/filters/ Filters in Eleventy}
 * @example 
 * // In an Eleventy template
 * `${this.emojiById('gospel')} // 🗞️`
 */
export default id => {
  /** @type {Object[Array]} Emoji matching the specified id */
  var hasEmojiId = emoji.symbols
    .filter(({id: test}) => test === id)

  return `<!--_includes/filter/emoji-by-id.js-->${hasEmojiId[0].emoji
    ? hasEmojiId[0].emoji
    : `<!--No emoji matching “${id}” id in _data/emoji.js-->`}`
}
