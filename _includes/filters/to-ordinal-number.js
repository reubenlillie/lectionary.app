/**
 * @file Defines a filter for displaying ordinal numbers
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Appends an ordinal suffix to a specified number
 * @module _includes/filters/ordinal-number
 * @since 0.5.0
 * @param {number} num The real number to convert
 * @return {string} An ordinal number
 * @see {@link https://stackoverflow.com/questions/13627308/add-st-nd-rd-and-th-ordinal-suffix-to-a-number#39466341 Stack Overflow discussion about adding ordinal suffixes with JavaScript}
 * @example 
 * // In an Eleventy template
 * `${this.toOrdinalNumber(28)}`
 */
export default num => {
  var suffix = ['st','nd','rd'][((num + 90) % 100 - 10) % 10 - 1] || 'th' 
  return num + suffix
}
