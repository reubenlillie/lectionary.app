/**
 * @file Defines a filter to convert an SVG to a data URI
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/*
 * Import Taylor Hunt’s Mini SVG `data:` URI module
 * @see {@link https://github.com/tigt/mini-svg-data-uri Mini SVG `data:` URI module on GitHub}
 */
import svgToMiniDataURI from 'mini-svg-data-uri'

/**
 * Convert an SVG file to a `data:` URI
 * @module _includes/filters/svgDataUri
 * @since 0.1.0
 * @param {string} path Filepath for an SVG
 * @return {string} The `data:` URI for the SVG
 * @see {@link https://css-tricks.com/using-svg/ “Using SVG” by Chris Coyier on CSS-Tricks}
 * @see {@link https://css-tricks.com/probably-dont-base64-svg/ “Probably Don’t Base64 SVG” by Chris Coyier on CSS-Tricks}
 * @see {@link https://codepen.io/tigt/post/optimizing-svgs-in-data-uris “Optimizing SVGs in data URIs” by Taylor Hunt on CodePen}
 * @example 
 * // In an Eleventy Template
 * `${this.svgToDataUri('img/logo.svg')}`
 */
export default path => {
  /** @type {string} The contents of the specified file */
  var svg = this.fileToString(path)

  return svgToMiniDataURI(svg)
}
