/**
 * @file Defines a filter for adding line break opportunities to a string
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Add line break opportunities to a string
 * @module _includes/filters/wbrify
 * @since 0.11.0
 * @param {string} str Unformatted string
 * @return {string} HTML Formatted string
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/HTML/Element/wbr
 “<wbr>: The Line Break Opportunity element” on MDN}
 * @example 
 * // In an Eleventy template
 * `${this.wbrify(lessons.first)}</li>`
 */
export default (str) => {
  // Insert a line break opportunity after a comma, semicolon, colon, endash, opening paranthesis, or opening bracket
  var formatted = str.replace(/(?<after>[,;:(–[])/giu, '$1<wbr>')
    // Before a single period, closing parenthesis, or closing bracket
    .replace(/(?<before>[.(\]])/giu, '<wbr>$1')

  return formatted
}
