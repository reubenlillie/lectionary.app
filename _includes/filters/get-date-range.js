/**
 * @file Defines a filter to get a range of dates (liturgical season)
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Create an array of dates within a range
 * @module _includes/filters/get-date-range
 * @since 0.11.0
 * @param {Object<Date>} start Starting date
 * @param {Object<Date>} end Ending date
 * @return {Object[]} Dates
 * @see {@link https://github.com/jakubpawlowicz/clean-css#minify-method clean-css on GitHub}
 * @see {@link https://www.11ty.dev/docs/data-js/#example-exposing-environment-variables Environment variables in Eleventy}
 * @example 
 * // In an Eleventy template
 * `${this.dateRange(startDate, endDate)}`
 */
export default (start, end) => {
  var arr = []
  var day = new Date(start)
  while (day < end) {
    arr.push(new Date(day))
    day.setDate(day.getDate() + 1);
  }
  return arr
}
