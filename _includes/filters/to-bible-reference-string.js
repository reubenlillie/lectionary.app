/**
 * @file Defines a filter for converting arrays of Bible references to a string
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.2.0
 */

/**
 * Format a list of Bible references, separated by semicolons
 * @param {string[]} An array of references
 * @return {string} Formatted references (for searching BibleGateway.com)
 */
function mapToString(arr) {
  // If the first character is not a letter, then the reference is to a psalm
  return `${arr.map(item =>
    /[^a-zA-Z]/.test(item.charAt(0))
      ? `Ps ${item}`
      : item).join('; ')}`
}

/**
 * Convert arrays of psalms and lessons references into a string,
 * following the order of readings
 * @module _includes/filters/to-bible-search-string
 * @param {Object[]} readings Psalms and/or lessons arrays to format
 * @return {string} Formatted string to pass to `searchBibleGateway()`
 * @see {@link https://www.11ty.dev/docs/filters/ Filters in Eleventy}
 * @see _includes/shortcodes/searchBibleGateway
 * @example
 * // In an Eleventy template
 * `${this.toBibleReferenceString({entry.psalms, entry.lessons})}`
 */
export default ({psalms, lessons}) => // Follow the order of service
  `${psalms?.morning
    ? mapToString(psalms.morning)
    : ''}; ${lessons.morning
      ? mapToString(Object.values(lessons?.morning))
      : mapToString(Object.values(lessons))}; ${lessons?.evening
        ? `${mapToString(Object.values(lessons?.evening))}; `
        : ''}${psalms?.evening
          ? mapToString(psalms.evening)
          : ''}`
