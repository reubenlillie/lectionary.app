/**
 * @file Finds the lectionary entries matching a calendar date
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Find the lectionary entry for one matching a specified calendar date
 * @module _includes/functions/by-calendar-date
 * @since 0.5.0
 * @param {Object[]} entries Lectionary entries to filter (a year, an entire lectionary)
 * @param {string} calendarDate Date to search
 * @return {Object[]} Filtered lectionary entries
 * @example 
 * // In an Eleventy template
 * `this.byWeekday(data.dol, 'Monday')}`
 */
export default (entries, calendarDate) =>
  entries.reduce((filteredEntries, currentEntry) => 
    [
      ...filteredEntries, 
      ...Object.values(currentEntry.days).filter(entry => 
        entry.day === calendarDate)
    ], 
    []
  )
