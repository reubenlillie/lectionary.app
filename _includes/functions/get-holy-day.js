/**
 * @file Determine if today is a Major Holy Day
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import byCalendarDate from './by-calendar-date.js'
import byHolyDay from './by-holy-day.js'
import byYear from './by-year.js'
import getCalendarDate from './get-calendar-date.js'
import getYear from './get-year.js'

/**
 * Check if there is any lectionary entry matching today’s calendar date
 * @module _includes/functions/get-holy-day
 * @since 0.5.0
 * @param {Object[]} entries
 * @param {Object.<Date>} [today=Current Date] Date to check
 * @return {boolean}
 * @see {@link https://episcopalchurch.org/files/book_of_common_prayer.pdf#page=15 “The Calendar of the Church Year” in the BCP}
 * @see {@link https://episcopalchurch.org/files/book_of_common_prayer.pdf#page=996 Holy Days in the BCP}
 * @example
 * // In an Eleventy template
 * `${this.getHolyDay(data.dol, today)}`
 */
export default (entries, today = new Date()) => {
  /** @type {string} Formatted calendar date for `today` e.g., Jan 1*/
  var calendarDate = getCalendarDate(today)
  
  /** @type {Object} Entry matching Holy Day */
  var holyDay = byHolyDay(entries.holyDays, calendarDate)
  
  /** @type {Object} Current year’s entry matching `calendarDate` */
  var onCalendarDate = byCalendarDate(
    byYear(entries, getYear(today)), 
    calendarDate
  )[0]

  // Does today’s date appear in the lectionary?
  return holyDay
    ? holyDay
    : onCalendarDate
      ? onCalendarDate
      : null
}
