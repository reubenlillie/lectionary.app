/**
 * @file Adds (or subtracts) days from a specified date
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Add (or subtract) days from a JavaScript Date() object
 * @module _includes/functions/add-days
 * @since 0.5.0
 * @param {number} daysToAdd Days to add (positive number) or subtract (negative number) from the start date
 * @param {Object.<Date>} [startDate=Current Date] Date from which to calculate
 * @return {Object.<Date>} Date (in local time)
 * @example 
 * // In an Eleventy template
 * `Pentecost 2025 is ${this.addDays(49, this.computus(2025))}`
 */
export default (daysToAdd, startDate = new Date()) => {
  startDate = startDate
    ? new Date(startDate)
    : new Date
  return new Date(startDate.setDate(startDate.getDate() + daysToAdd))
}
