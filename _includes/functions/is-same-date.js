/**
 * @file Checks if two dates are the same
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Compare two dates years, months, and days, not anything more granular
 * @module _includes/functions/is-same-date
 * @since 0.5.0
 * @param {Object.<Date>} firstDate First date to compare
 * @param {Object.<Date>} secondDate Second date to compare
 * @return {boolean} Whether the dates are the same
 * @example 
 * // In an Eleventy template
 * `Is today Febuary 2? `{this.isSameDate(today, new Date(year, 1, 2))}`
 */
export default (firstDate, secondDate) => 
  firstDate.getFullYear() === secondDate.getFullYear() &&
  firstDate.getMonth() === secondDate.getMonth() &&
  firstDate.getDate() === secondDate.getDate()
