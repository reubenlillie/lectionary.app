/**
 * @file Gets the lectionary entry matching a holy day
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Filter an array of holy days by 
 * @module _includes/functions/by-holy-day
 * @since 0.5.0
 * @param {Object[]} entries Lectionary entries to filter (a year, an entire lectionary)
 * @param {string} calendarDate Date to search
 * @return {Object} Lectionary entry for matching holy day
 * @example 
 * // In an Eleventy template
 * `St. Stephen’s Day is ${this.byHolyDay(data.dol.holyDays, 'Dec 26')}`
 */
export default (entries, calendarDate) => {
  entries = Array.isArray(entries) 
    ? entries 
    : Object.values(entries)
  return entries.find(entry => entry.day === calendarDate)
}
