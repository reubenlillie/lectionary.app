/**
 * @file Get the week of the liturgical season
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import computus from './computus.js'
import daysBetween from './days-between.js'
import getAdvent from './get-advent.js'
import getAfterEpiphany from './get-after-epiphany.js'
import getAshWednesday from './get-ash-wednesday.js'
import getChristmas from './get-christmas.js'
import getPentecost from './get-pentecost.js'
import getSeason from './get-season.js'

/**
 * Determine the week of the liturgical season for a specified date
 * @module _includes/functions/get-week
 * @since 0.5.0
 * @param {Object.<Date>} [today=Current Date] Date (in local time)
 * @return {number} The week number of the liturgical season
 * @example 
 * // In an Eleventy template
 * `${this.getWeek()}`
 */
export default (today = new Date()) => {
  /** @type {Object} Placeholder object to be returned */
  var startDate = {}

  /** @type {number} Four-digit year for `today` */
  var year = today.getFullYear()
  
  /** @type {string} The current liturgial season */
  var season = getSeason(today)

  // Is today in Advent?
  if(season === 'Advent') {
    startDate = getAdvent(year) 
  }

  // Is today in Christmastide?
  if(season === 'Christmas') {
    startDate = getChristmas(year)
  }

  // Is today in Epiphanytide?
  if(season === 'Epiphany') {
    startDate = getAfterEpiphany(year)
  }

  // Is today in Lent?
  if(season === 'Lent') {
    startDate = getAshWednesday(year)
  }
  
  // Is today in Eastertide?
  if(season === 'Easter') {
    startDate = computus(year)
  }
  
  // Is today in the Season after Pentecost?
  if(season === 'The Season after Pentecost') {
    startDate = getPentecost(year)
  }
  
  return  Math.floor(daysBetween(startDate, today) / 7)
}
