/**
 * @file Gets the lectionary entries matching a weekday
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Filter an array of lectionary entries for those on a specified weekday
 * @module _includes/functions/by-weekday
 * @since 0.5.0
 * @param {Object[]} entries Lectionary entries to filter (a year, an entire lectionary)
 * @param {string} weekday Name of the liturgical season
 * @return {Object[]} Filtered lectionary entries
 * @example 
 * // In an Eleventy template
 * `this.byWeekday(data.dailyOffice, 'Monday')}`
 */
export default (entries, weekday) => 
  entries.reduce((filteredEntries, currentEntry) => 
    [
      ...filteredEntries, 
      ...Object.values(currentEntry.days).filter(entry => 
        entry.day === weekday)
    ], 
    []
  )
