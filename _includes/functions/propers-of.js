/**
 * @file Get the propers for the current year’s season after Pentecost
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import daysBetween from  './days-between.js'
import getPentecost from  './get-pentecost.js'

/**
 * Get the propers for the current year’s season after Pentecost
 * @module _includes/functions/propers-of
 * @since 0.5.0
 * @since 1.0.0 Add placeholder Sunday for Propers 1 and 2
 * @param {Object[]} Propers to filter
 * @param {Object.<Date>} [pentecost=Pentecost for the current year] Date
 * @return {Object[]} Filtered propers
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#sorting_with_map Sorting with map on MDN}
 * @example `Propers for the current year: ${this.propersOf(propers)}`
 */
export default (propers,
  pentecost = getPentecost(new Date().getFullYear())) => {
  /** @type {Object[]} Remove Trinity weekend entries */
  propers = [...propers.filter(entry => entry.props.week)]

  /** @type {number} Four-digit year for `pentecost` */
  var year = pentecost.getFullYear()

  /**
   * Define the starting proper for the current year
   * @function
   * @name fromStartingProper
   * @param {Object[]} propers Propers to filter
   * @see {@link https://episcopalchurch.org/files/book_of_common_prayer.pdf#page=158 “Concerning the Proper of the Church Year” from the BCP}
   * @see {@link https://episcopalchurch.org/files/book_of_common_prayer.pdf#page=967 The Season after Pentecost from the BCP}
   */
  var fromStartingProper = propers => {
    // Proper 2: Week of the Sunday closest to May 18
    if(daysBetween(pentecost, new Date(year, 4, 18)) <= 3) {
      return propers.slice(1)
    }

    // Proper 3: Week of the Sunday closest to May 25
    if(daysBetween(pentecost, new Date(year, 4, 25)) <= 3) {
      return propers.slice(2)
    } 

    // Proper 4: Week of the Sunday closest to June 1
    if(daysBetween(pentecost, new Date(year, 5, 1)) <= 3) {
      return propers.slice(3)
    }

    // Proper 5: Week of the Sunday closest to June 8
    if(daysBetween(pentecost, new Date(year, 5, 8)) <= 3) {
      return propers.slice(4)
    }

    // Proper 6: Week of the Sunday closest to June 15
    if(daysBetween(pentecost, new Date(year, 5, 15)) <= 3) {
      return propers.slice(5)
    }
  }

  // Create a temporary array to hold each proper’s index and number
  var indexed = propers.map((entry, index) => {
    return {
      index: index,
      proper: parseInt(entry.props.week.split(' ').pop())
    }
  })

  // Sort the temporary array
  indexed.sort((a, b) =>
    a.proper > b.proper
      ? 1
      : a.proper < b.proper
        ? -1
        : 0
  )

  // Reorder the original array based on the proper number
  propers = [...indexed.map(item => propers[item.index])]

  // Remove unused propers from the beginning of the sorted array
  propers = [...fromStartingProper(propers)]

  // If a proper has fewer than seven days (i.e., no Sunday, cf. Propers 1 and 2)
  propers.forEach((proper, index, arr) => arr[index].days.length < 7
    // Add a placeholder Sunday
    ? proper.days.unshift({day: 'Sunday'})
    : null)

  return propers
}
