/**
 * @file Calculates the date of Ash Wednesday on the Gregorian Calendar
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import addDays from './add-days.js'
import computus from './computus.js'

/**
 * Calculate Ash Wednesday of a specified year
 * @module _includes/functions/get-ash-wednesday
 * @since 0.5.0
 * @param {number} [year=Current Year] Four-digit year
 * @return {Object.<Date>} Date (in local time)
 * @example 
 * // In an Eleventy template
 * `Ash Wednesday Day 2023 is ${this.ashWednesday(2023)}`
 */
export default (year = new Date().getFullYear()) =>
  // 40 days and 6 Sundays before Easter
  addDays(-46, computus(year))
