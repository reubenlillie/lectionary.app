/**
 * @file Determine if Sunday takes precedence over a Holy Day
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import addDays from './add-days.js'
import getAdvent from './get-advent.js'
import getAfterEpiphany from './get-after-epiphany.js'
import getAshWednesday from './get-ash-wednesday.js'
import getPentecost from './get-pentecost.js'
import isSameDate from './is-same-date.js'

/**
 * Check if a specified Sunday takes precedence over a Holy Day
 * @module _includes/functions/sunday-or-holy-day
 * @since 0.2.0
 * @param {Object[]} entries 
 * @param {Object.<Date>} [today=Current Date] Sunday to check
 * @return {boolean} true Sunday precedes Holy Day
 * @return {boolean} false Holy Day precedes Sunday
 * @see {@link https://episcopalchurch.org/files/book_of_common_prayer.pdf#page=15 “The Calendar of the Church Year” in the BCP}
 * @see {@link https://episcopalchurch.org/files/book_of_common_prayer.pdf#page=996 Holy Days in the BCP}
 * @example
 * // In an Eleventy template
 * `${this.SundayOrHolyDay(today)}`
 */
export default (today = new Date()) => {
  /** @type {number} Four-digit year for `today` */
  var year = today.getFullYear() 

  // Is today Sunday
  if(today.getDay() === 0) {
    // Or is today Holy Name (Jan 1)?
    if(isSameDate(today, new Date(year, 0, 1))
      // Is today the Presentation (Feb 2)?
      || isSameDate(today, new Date(year, 1, 2))
      // Or is today the Transfiguration (Aug 6)?
      || isSameDate(today, new Date(year, 7, 6))) {
      return false
    }

    /*
     * Lessons appointed for the feast may be substituted
     * for those of the Sunday outside these date ranges
     */

    // Not between the Last Sunday ater Pentecost . . . 
    if((addDays(-7, getAdvent(year)) < today
      // . . . and the First Sunday after the Epiphany (OR for year change)
      || today < getAfterEpiphany(year))
      // Not between the Last Sunday after the Epiphany . . . 
      || (addDays(-3, getAshWednesday(year)) < today
      // . . . and Trinity Sunday
      && today < addDays(7, getPentecost(year)))) {
        return true
    }

    // Otherwise, the Holy Day takes precedence
    return false
  } 

  // Otherwise, the Holy Day takes precedence
  return false
}
