/**
 * @file Gets the date of the Epiphany
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Get the JavaScript `Date` object for the Epiphany (January 6)
 * @module _includes/functions/get-epiphany
 * @since 0.5.0
 * @param {Object} [year=Current Year] Date (in local time)
 * @return {Object} Date of the Epiphany
 * @example 
 * // In an Eleventy template
 * `this.getEpiphany(2023)`
 */
export default (year = new Date().getFullYear()) =>
  new Date(year, 0, 6) 
