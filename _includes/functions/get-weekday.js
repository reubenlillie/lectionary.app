/**
 * @file Gets the day of the week
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Get the day of the week for a specified date
 * @module _includes/functions/get-weekday
 * @since 0.5.0
 * @param {Object} [today=Current Date)
 * @return {string} The English name of the day of the week 
 * @example `Today is ${this.getWeekday()}`
 */
export default (today = new Date()) =>
  new Intl.DateTimeFormat('en', {weekday: 'long'}).format(new Date(today))
