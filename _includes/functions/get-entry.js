/**
 * @file Get the lectionary entry and its properties for a specified date
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 0.5.0
 */

import addDays from './add-days.js'
import bySeason from './by-season.js'
import byWeekday from './by-weekday.js'
import byYear from './by-year.js'
import daysBetween from './days-between.js'
import getAdvent from './get-advent.js'
import getAfterEpiphany from './get-after-epiphany.js'
import getAshWednesday from './get-ash-wednesday.js'
import getEpiphany from './get-epiphany.js'
import getEve from './get-eve.js'
import getHolyDay from './get-holy-day.js'
import getPentecost from './get-pentecost.js'
import getSeason from './get-season.js'
import getWeekIndex from './get-week-index.js'
import getWeekday from './get-weekday.js'
import getYear from './get-year.js'
import isEve from './is-eve.js'
import isSameDate from './is-same-date.js'
import isWeekOf1Epiphany from './is-week-of-1-epiphany.js'
import propersOf from './propers-of.js'
import sundayOrHolyDay from './sunday-or-holy-day.js'
import toOrdinalNumber from '../filters/to-ordinal-number.js'

/**
 * Get the lectionary entry for a specified date from `_data/dailyOffice`
 * @since 0.5.0
 * @since 0.9.0 Depracates `getEntryDay()` and `getEntryProperties()`
 * @since 0.11.0 Set props for Holy Days _after_ `propersOf()` runs
 * @param {Object} data Eleventy’s `data` object
 * @param {Object.<Date>} today Date for which to find an entry
 * @return {Object} Lectionary entry
 * @example
 * // In an Eleventy template
 * `Entry for Christmas 2021: ${this.getEntry(dailyOffice, new Date(2021, 11, 25))}`
 */
export default (entries, today) => {
  /** @type {Object} Placeholder for a lectionary entry */
  var entry = {}

  /** @type {Object} Placeholder for special evening entry, if applicable */
  var eve

  /** @type {Object} Placeholder for a lectionary entry’s `props` object */
  var props

  /** @type {Object} Placeholder for a lectionary entry’s `day` object */
  var day

  /** @type {Object} Placeholder for a single entry on the same weekday */
  var weekday

  /** @type {number} Four-digit year for today */
  var year = today.getFullYear()

  /** @type {Object.<Date>} 1 Epiphany for the current year */
  var afterEpiphany = getAfterEpiphany(year)

  /** @type {Object.<Date>} Day of Pentecost for the current year */
  var pentecost = getPentecost(year)

  /** @type {boolean} Is today the Eve of Trinity? */
  var isTrinityEve = isSameDate(today, addDays(6, pentecost))

  /** @type {boolean} Is today Trinity Sunday? */
  var isTrinity = isSameDate(today, addDays(7, pentecost))

  /** @type {Object[]} Lectionary entries for the current liturgical season */
  var season = bySeason(byYear(entries, getYear(today)), getSeason(today))

  /** @type {string} Weekday name for today */
  var dayOfWeek = getWeekday(today)

  /** {Object[]} Entries on the same weekday within the current season */
  var sameWeekday = byWeekday(season, dayOfWeek)

  /** @type {number} Position of the current week in `entries` array */
  var index = getWeekIndex(entries, today)

  /** @type {Object} Lectionary entry matching today by calendar date */
  var holyDay = getHolyDay(entries, today)

  /*
   * Check which `props` and `day` data to use
   * based on the season, week, and whether today is also a Holy Day.
   */

  // Is today a Sunday after Christmas?
  if(dayOfWeek === 'Sunday' && season[0].props.season === 'Christmas') {
    // Is today still in December?
    weekday = today.getMonth() === 11
      // First Sunday after Christmas
      ? sameWeekday[0]
      // Second Sunday after Christmas
      : sameWeekday[1]
  }

  /*
   * Must run AFTER determining if today is a Sunday after Christmas,
   * because `weekday` would be overridden
   */

  // Is today Christmas Day or Holy Name and also a Sunday?
  if(dayOfWeek === 'Sunday'
    && (isSameDate(today, new Date(year, 11, 25))
      || isSameDate(today, new Date(year, 0, 1)))) {
    weekday = getHolyDay(entries, today)
  }

  // Is today in Epiphany?
  if(season[0].props.season === 'Epiphany') {
    // Is today in the week of the Epiphany and Following?
    if(getEpiphany(year) <= today
      && today < afterEpiphany) {
        // There are no weekday entries, so use the dated entries
        day = holyDay
        // Use the properties from the first item in the Epiphany array
        props = season[0].props
    }

    // Is today the Eve of the First Sunday after the Epiphany?
    if(isSameDate(today, addDays(-1, getAfterEpiphany(year)))) {
      // Entry from the last day in the Epiphany and Following array
      eve = season[0].days.pop()
    }

    // Is today in the Week of the First Sunday after the Epiphany?
    if(isWeekOf1Epiphany(today)) {
      // Ignore conflicting dated entries from the Epiphany and Following
      holyDay = null
    }

    // Is today in the Week of the First Sunday after the Epiphany?
    if(isWeekOf1Epiphany(today)
      // OR in the Week of Last Epiphany?
      || daysBetween(today, getAshWednesday(year)) <= 3) {
        // Account for the entries for the Epiphany and Following
        props = season[index + 1].props

    }

    // Offset props by 1 for the Epiphany and Following by default
    props = props ||
      season.filter(() => sameWeekday[index + 1])[index + 1].props
  }

  // Is today a Monday or Tuesday in Lent?
  if(season[0].props.season === 'Lent'
    && (dayOfWeek === 'Monday' || dayOfWeek === 'Tuesday')) {
    // Account for the fact that Lent begins on a Wednesday
    props = season.filter(() => sameWeekday[index])[index + 1].props
  }

  /*
   * Must run BEFORE determining if today is in the season after Pentecost,
   * because unused propers are removed from the start of the `season` array
   * including the entry for Trinity.
   */

  // Is today the Eve of Trinity?
  if(isTrinityEve) {
    /*
     * The entry in `_data/dailyOffice` for Trinity weekend 
     * does not contain a `props.week` key.
     */
    eve = season.find(entry => !entry.props.week).days[0]
  }

  // Is today Trinity Sunday?
  if(isTrinity) {
    day = season.find(entry => !entry.props.week).days[1]
  }

  // Is today the Eve of Trinity OR Trinity Sunday?
  if(isTrinity || isTrinityEve) {
    props = season.find(entry => !entry.props.week).props
  }

  // Is today in the season after Pentecost?
  if(season[0].props.season === 'The Season after Pentecost') {
    // Sort and filter the propers for the current year
    season = propersOf(season, pentecost)
  }

  /*
   * Must run AFTER determining if today is in the season after Pentecost,
   * because unused propers are removed from the `season` array.
   */

  // Set properties for Holy Days outside of Epiphany
  if(holyDay) {
    // Check if any properties have already been assigned
    props = props
      // Keep them
      ? props
      // Use this week’s properties
      : season[index].props
  }

  /*
   * Check if any weekday data has already been assigned
   */
  weekday = weekday
    ? weekday
    : byWeekday(season, dayOfWeek).length === index
      ? byWeekday(season, dayOfWeek)[index -1]
      : byWeekday(season, dayOfWeek)[index]

  // Check if any properties have already been assigned
  props = props
    ? props
    // Otherwise, use the standard week index by default.
    : season.filter(() => sameWeekday[index]).length === index
      ? season.filter(() => sameWeekday[index])[index - 1].props
      : season.filter(() => sameWeekday[index])[index].props

  /*
   * Check if any day data has already been assigned
   */
  day = day
    // Use that day, period.
    ? day
    // Is today a Holy Day
    : holyDay
      // Does it take precedence of a Sunday?
      ? !sundayOrHolyDay(today)
        // Is today the eve of a Holy Day and that has a weekday entry?
        ? isEve(holyDay) && weekday
          // Is tomorrow an observed Holy Day?
          ? !sundayOrHolyDay(addDays(1, today)) || isTrinityEve
            // Incorporate the eve data
            ? getEve(weekday, holyDay)
            // Ignore the eve data
            : weekday
          // Use the Holy Day data
          : holyDay
        // Use the entry for Sunday
        : weekday
    // Use the weekday entry by default
    : weekday

  // Merge the appropriate properties and readings data
  entry = Object.assign({}, props, day)

  /*
   * Modify specific `entry` properties under the following circumstances:
   */

  // Is today also the 'Eve' of a day?
  if(eve) {
    // Replace the title, if any
    entry.title = eve.title
    // Replace the evening psalms with the ones for the eve
    entry.psalms = Object.assign({}, entry.psalms, eve.psalms)
    // Use both the original set of readings and the ones for the eve
    entry.lessons = entry.lessons.evening
      // If the original already has evening lessons, replace them
      ? Object.assign({}, entry.lessons, eve.lessons)
      // Otherwise, move the original readings to the morning
      : Object.assign({morning: entry.lessons}, eve.lessons)
  }

  // Is today also a Sunday?
  if(dayOfWeek === 'Sunday') {
    // Is today between Trinity Sunday and the Last Sunday after Pentecost?
    if(addDays(7, pentecost) < today &&
      today < addDays(-7, getAdvent(year))) {
      // Does today’s entry already have a title property?
      entry.title
        // Keep that title
        ? entry.title
        // Count the Sunday’s since Pentecost 
        : entry.title = `${toOrdinalNumber(index)} Sunday after Pentecost`
    }

    /*
     * Is today the Last Sunday after Pentecost?
     * @since 1.1.0 Assign `entry.title` as an object instead of string
     */
    if(isSameDate(today, addDays(-7, getAdvent(year)))) {
      entry.title = {
        full: 'Last Sunday after Pentecost: The Reign of Our Lord Jesus Christ',
        main: 'Reign of Christ',
        sub: 'Last Sunday after Pentecost'
      }
    }
  }

  return entry
}
