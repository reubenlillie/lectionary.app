/**
 * @file Get the date of the First Sunday after the Epiphany
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import addDays from './add-days.js'

/**
 * Get the First Sunday after Epiphany of a specified year
 * @module _includes/functions/get-after-epiphany
 * @since 0.5.0
 * @param {number} [year=Current Year] Four-digit year
 * @return {Object.<Date>} (in local time)
 * @example 
 * // In an Eleventy template
 * `First Sunday after the Epiphany 2022 is ${this.getAfterEpiphany(2022))}`
 */
export default (year = new Date().getFullYear()) => {
  // The earliest the First Sunday after the Epiphany can be is January 7
  var earliest = new Date(year, 0, 7)

  return addDays(7 - earliest.getDay(), earliest)
}
