/**
 * @file Calculates the date of Easter Sunday on the Gregorian Calendar
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Calculate Easter Sunday using an adaptation of Jean Meeus’s algorithm
 * @module _includes/functions/computus
 * @since 0.5.0
 * @param {number} [year=Current Year] Four-digit year
 * @return {Object.<Date>} (in local time)
 * @example 
 * // In an Eleventy template
 * `Easter Day 2023 is ${this.computus(2023)}`
 * @see {@link https://en.wikipedia.org/wiki/Computus Jean Meeus’s algorithm}
 */
export default (year = (new Date).getFullYear()) => {
  // Begin calculating from January 1 of the specified year
  var day = new Date(year, 0, 1)

  // Find the “golden number”
  var a = year % 19

  // Choose which version of the algorithm to use based on the given year
  var b = (2200 <= year && year <= 2299)
    ? ((11 * a) + 4) % 30
    : ((11 * a) + 5) % 30

  // Determine whether or not to compensate for the previous step
  var c = ((b === 0) || (b === 1 && a > 10 ))
    ? (b + 1)
    : b

  // Use c first to find the month: April or March
  var m = (1 <= c && c <= 19)
    ? 3
    : 2

  // Then use c to find the full moon after the northward equinox.
  var d = (50 - c) % 31

  // Mark the date of that full moon—the “Paschal” full moon
  day.setMonth(m, d)

  // Count forward the number of days until the following Sunday (Easter).
  day.setMonth(m, d + (7 - day.getDay()))

  // Gregorian Western Easter Sunday (in local time)
  return day
}
