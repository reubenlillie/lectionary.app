/**
 * @file Get the `Array` index for the liturgical week
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import bySeason from './by-season.js'
import byYear from './by-year.js'
import daysBetween from './days-between.js'
import getAshWednesday from './get-ash-wednesday.js'
import getSeason from './get-season.js'
import getWeek from './get-week.js'
import getYear from './get-year.js'

/**
 * Check if there is any lectionary entry matching today’s calendar date
 * @module _includes/functions/get-week-index
 * @since 0.5.0
 * @param {Object[]} Lectionary entries from `_data/dailyOffice`
 * @param {Object.<Date>} [today=Current Date] Date to check
 * @return {number}
 * @example 
 * // In an Eleventy template
 * `Today’s week index: ${this.getWeekIndex(data.dailyOffice)}`
 */
export default (entries, today = new Date()) => {
  /** @type {number} Four-digit year for `today` */
  var year = today.getFullYear()
  
  /** @type {string} Current liturgical season */
  var season = getSeason(today)
  
  /** @type {number} Current week of the liturgical season */
  var week = getWeek(today)

  // Is today in Christmastide? 
  if(season === 'Christmas') {
    week = 0
  }

  // Is today in Epiphany?
  if(season === 'Epiphany') {
    // Is today before the Week of Last Epiphany?
    return daysBetween(today, getAshWednesday(year)) > 3
      ? week // Yes? The index is the same as the week number
      : /*
         * No? Compensate for the additional entries for
         * (1) the Epiphany and Following and 
         * (2) the Week of Last Epiphany
         * Is today before Thursday?
         */
        today.getDay() < 4
          // Then there are fewer entries to subtract
          ? bySeason(byYear(entries, getYear(today)), season).length - 2 
          // Then there’s one more set of entries to subtract
          : bySeason(byYear(entries, getYear(today)), season).length - 3 
  }
  
  // If not Epiphany, the index and liturgical week number are the same
  return week
}
