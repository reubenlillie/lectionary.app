/**
 * @file Merge properties for the eve of a holy day into a weekday entry
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Merge properties for the eve of a holy day into a weekday entry
 * @module _includes/functions/get-eve
 * @since 0.7.0
 * @since 0.9.0 Restructure psalms ojbect
 * @since 0.11.0 Account for when a Holy Day has morning and evening psalms
 * @param {weekday} weekday Lectionary entry for a weekday
 * @param {weekday} holyDay Lectionary entry that is the eve of a holy day
 * @return {Object} Merged lectionary entry
 * @example 
 * // In an Eleventy template
 * `${this.getEve(weekday, holyDay)}`
 */
 export default (
  {psalms: amPs, lessons: morning, ...restOfWeekday},
  {psalms: pmPs, lessons: evening, ...restOfHolyDay}
) => Object.assign(
    {}, 
    restOfWeekday, 
    restOfHolyDay,
    {
      psalms: {
        morning: pmPs.morning !== undefined ? pmPs.morning : amPs.morning,
        evening: pmPs.evening !== undefined ? pmPs.evening : amPs.evening,
      },
      lessons: {
        morning,
        ...evening
      }
    }
  )
