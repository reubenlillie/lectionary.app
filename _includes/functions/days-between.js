/**
 * @file Calculate the number of days between two dates
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Calculate the number of days between two JavaScript `Date` objects
 * @module _includes/functions/days-between
 * @since 0.5.0
 * @param {Object.<Date>} startDate Date 
 * @param {Object.<Date>} endDate Date 
 * @return {number} Days between `startDate` and `endDate`
 * @example 
 * // In an Eleventy template
 * `Days till Christmas: ${this.daysBetween(today, this.getChristmas(2021))}`
 */
export default (startDate, endDate) => 
  Math.round(Math.abs((new Date(startDate) - new Date(endDate)) / (24 * 60 * 60 * 1000)))
