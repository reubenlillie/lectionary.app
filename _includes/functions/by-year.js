/**
 * @file Gets the lectionary entries matching a year
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Filter an array of lectionary entries for those in a specified year
 * @module _includes/functions/by-year
 * @since 0.5.0
 * @since 2.0.0 Adjust logic for restructured DOL data
 * @param {Object[]} entries Lectionary entries to filter (e.g., an entire lectionary)
 * @param {string} year Name of the lectionary year
 * @return {Object[]} Filtered lectionary entries
 * @example
 * // In an Eleventy template
 * `this.byYear(data.dailyOffice, this.getYear(today))}`
 */
export default (entries, year) => {
  /** @type {Object[]} Placeholder array for entries in a given year */
  var thisYear = []

  // If entries is an array
  entries = Array.isArray(entries)
    // It’s ready to be filtered
    ? entries
    // Otherwise, create an array from the entries’ values
    : Object.values(entries)

  // Filter only entries with a `years` property
  entries = entries.filter(entry => entry.years)
    .forEach(entry => entry.years.forEach(y =>
    // If the entry year matches the `year` paramter
    y.props.year === year
      // Add that entry to this year’s array with the following shape
      ? thisYear.push(
        {
          // Merge the entry props with the year.props
          props: Object.assign({}, entry.props, y.props),
          days: y.days
        }
      )
      // Move on to the next entry
      : '')
    )

  return thisYear
}
