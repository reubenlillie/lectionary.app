/**
 * @file Gets or sets a liturgical color for a lectionary entry
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Gets or sets a liturgical color for a lectionary entry
 * @module _includes/functions/get-color
 * @since 0.11.0
 * @param {string} color A lectionary entry’s color (if defined)
 * @param {string} season A lectionary entry’s season
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @return {string} Color name
 * @example 
 * // As a data attribute
 * `<span data-color="${getColor(color, season)}"></span>`
 */
export default (color, season) =>
  // Does today’s entry include a hard-coded `color` property by default?
  color 
    // Use it
    ? color
    // Otherwise, use the appropriate color for the season
    : season === 'Advent' || season === 'Lent'
      ? 'purple'
      : season === 'Christmas' || season === 'Easter'
        ? 'white'
        : season === 'Epiphany' || season === 'The Season after Pentecost'
          ? 'green'
          : null
