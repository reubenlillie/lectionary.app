/**
 * @file Determine the Year of the Daily Office Lectionary
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import getAdvent from './get-advent.js'

/**
 * Determine which lectionary applies to a specified date
 * @module _includes/functions/get-year
 * @since 0.1.0
 * @param {Object.<Date>} [today=Current Date] (in local time)
 * @return {string} Daily Office Lectionary Year
 * @example 
 * // In an Eleventy template
 * `${this.getYear()}`
 */
export default (today = new Date()) => {
  /** @type {number} Four-digit year for `today` */
  var year = today.getFullYear()

  // Is today on or after the First Sunday of Advent Advent?
  return today >= getAdvent(year)
    ? year % 2 === 0 // Yes; is year even?
      ? 'Year One' // After Advent has begun; year is even
      : 'Year Two' // After Advent has begun; year is odd
    : year % 2 === 0 // No; is year even?
      ? 'Year Two' // Before Advent; year is even
      : 'Year One' // Before Advent; year is odd
}
