/**
 * @file Get the liturgical season
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import computus from './computus.js'
import getAdvent from './get-advent.js'
import getAshWednesday from './get-ash-wednesday.js'
import getChristmas from './get-christmas.js'
import getEpiphany from './get-epiphany.js'
import getPentecost from './get-pentecost.js'

/**
 * Determine the liturgical season of a specified date
 * @module _includes/functions/get-season
 * @since 0.5.0
 * @param {Object.<Date>} [today=Current Date] Date (in local time)
 * @return {string} The liturgical season
 * @example 
 * // In an Eleventy template
 * `${this.getSeason()}`
 */
export default (today = new Date()) => {
  /** @type {string} Placeholder string to be returned */
  var season = ''

  /** @type {number} Four-digit year for `today` */
  var year = today.getFullYear()

  // Is today between the First Sunday of Advent (inclusive) and Christmas Day?
  if(getAdvent(year) <= today && today < getChristmas(year)) {
    season = 'Advent'
  }

  // Is today between Christmas Day (inclusive) and the Epiphany?
  if(getChristmas(year) <= today ||  today < getEpiphany(year)) {
    season = 'Christmas'
  }

  // Is today between the Epiphany (inclusive) and Ash Wednesday?
  if(getEpiphany(year) <= today && today < getAshWednesday(year)) {
    season = 'Epiphany'
  }

  // Is today between Ash Wednesday (inclusive) and Easter?
  if(getAshWednesday(year) <= today && today < computus(year)) {
    season = 'Lent'
  }

  // Is today between Easter (inclusive) and the Day of Pentecost (inclusive)?
  if(computus(year) <= today && today <= getPentecost(year)) {
    season = 'Easter'
  }

  // Is today between Pentecost the First Sunday of Advent?
  if(getPentecost(year) < today && today < getAdvent(year)) {
    season = 'The Season after Pentecost'
  }

  return season
}
