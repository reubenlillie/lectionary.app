/**
 * @file Gets the date of Christmas
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Get the JavaScript `Date` object for Christmas (Decemer 25)
 * @module _includes/functions/get-christmas
 * @since 0.5.0
 * @param {number} [year=Current Year] Date (in local time)
 * @return {Object.<Date>} Date of Christmas
 * @example 
 * // In an Eleventy template
 * `this.getChristmas(2023)`
 */
export default (year = new Date().getFullYear()) => new Date(year, 11, 25) 
