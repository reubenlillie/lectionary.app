/**
 * @file Get the First Sunday of Advent
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import addDays from './add-days.js'

/**
 * Get the First Sunday of Advent of a specified year
 * @module _includes/functions/get-advent
 * @since 0.5.0
 * @since 0.11.0 Default parameter for year
 * @param {number} [year=Current Year] Four-digit year
 * @return {Object.<Date>} (in local time)
 * @example 
 * // In an Eleventy template
 * `First Sunday of Advent 2021 is ${this.getAdvent(2021))}`
 */
export default (year = new Date().getFullYear()) => {
  // The earliest the First Sunday of Advent can be is November 26
  var earliest = new Date(year, 10, 26)

  return addDays(7 - earliest.getDay(), earliest)
}
