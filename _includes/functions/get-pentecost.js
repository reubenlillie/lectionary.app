/**
 * @file Calculates the date of Pentecost on the Gregorian Calendar
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import addDays from './add-days.js'
import computus from './computus.js'

/**
 * Calculate the Day of Pentecost of a specified year
 * @module _includes/functions/get-pentecost
 * @since 0.5.0
 * @param {number} [year=Current Year] Four-digit year
 * @return {Object} Date (in local time)
 * @example 
 * // In an Eleventy template
 * `Day of Pentecost 2023 is ${this.pentecost(2023)}`
 */
export default (year = new Date().getFullYear()) =>
  // The Sunday which is 50 days (inclusive) after Easter
  addDays(49, computus(year))
