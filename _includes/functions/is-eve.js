/**
 * @file Checks if an entry is for the eve of another holy day
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Checks if an entry is the eve of another holy day
 * @module _includes/functions/is-eve
 * @since 0.7.0
 * @since 1.1.0 Test that title is a string
 * @param {string} title Lectionary `entry.title` property
 * @return {boolean} Whether the title contains 'Eve'
 * @example
 * // In an Eleventy template
 * `${this.isEve(entry)}`
 */
export default ({title}) => typeof(title) === 'string' && title.includes('Eve') ? true : false
