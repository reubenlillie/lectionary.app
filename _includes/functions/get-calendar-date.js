/**
 * @file Gets a day’s formatted calendar date
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Get the calendar date in a format that matches those in `_data/dailyOffice/`
 * @module _includes/functions/get-calendar-date
 * @since 0.5.0
 * @param {Object.<Date>} The JavaScript `Date` object to format
 * @return {string} The formatted calendar day
 * @example 
 * // In an Eleventy template
 * `Today is ${this.getCalendarDate()}`
 */
export default today =>
  new Intl.DateTimeFormat('en', {month: 'short', day: 'numeric'})
    .format(new Date(today))
