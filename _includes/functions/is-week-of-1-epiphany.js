/**
 * @file Checks if a date is in the Week of the First Sunday after the Epiphany
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

import daysBetween from './days-between.js'
import getAfterEpiphany from './get-after-epiphany.js'
import isSameDate from './is-same-date.js'

/**
 * Check is a specified date is in the Week of the First Sunday after the Epiphany
 * @module _includes/functions/is-week-of-1-epiphany
 * @since 0.5.0
 * @param {Object.<Date>} [today=Current Date] Date for which to find an entry
 * @return {boolean} 
 * @example 
 * // In an Eleventy template
 * `Is today in the Week of the First Sunday after the Epiphany? ${this.isWeekOf1Epiphany()}`
 */
export default (today = new Date()) => {
  /** @type {number} Four-digit year for `today` */
  var year = today.getFullYear()

  /** @type {Object.<Date>} The First Sunday after the Epiphany */
  var afterEpiphany = getAfterEpiphany(year)

  // Is today the First Sunday after the Epiphany?
  return isSameDate(afterEpiphany, today) 
    // Or is today within that same week?
    || (afterEpiphany < today 
      && 1 <= daysBetween(afterEpiphany, today) 
      && daysBetween(afterEpiphany, today) <= 6)
        ? true
        : false
}
