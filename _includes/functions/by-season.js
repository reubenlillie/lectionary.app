/**
 * @file Gets the lectionary entries matching a season
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Filter an array of lectionary entries for those in a specified season
 * @module _includes/functions/by-season
* @since 0.5.0
 * @param {Object[]} entries Lectionary entries to filter (a year, an entire lectionary)
 * @param {string} season Name of the liturgical season
 * @return {Object[]} Filtered lectionary entries
 * @example 
 * // In an Eleventy template
 * `this.bySeason(yearOneEntries)}`
 */
export default (entries, season) =>
  entries.filter(entry => entry.props.season === season)
