/**
 * @file Defines a filter to minify HTML template files
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/*
 * Import Juriy Zaytsev’s HTMLMinifier module
 * @see {@link https://github.com/kangax/html-minifier HTMLMinifier on GitHub}
 */
import htmlmin from 'html-minifier'

/**
 * Eleventy minifies HTML automatically in production
 * @module _includes/transforms/minify-html
 * @since 0.5.0
 * @param {string} content An HTML document
 * @param {string} outputPath Where Eleventy should output the content
 * @return {string} The minified content
 * @see {@link https://www.11ty.dev/docs/data-js/#example-exposing-environment-variables Environment variables in Eleventy}
 */
export default (content, outputPath) => {
  // Only minify HTML for production
  if(process.env.ELEVENTY_ENV === 'production' &&
    (outputPath !== false && outputPath.endsWith('.html'))) {
    var minified = htmlmin.minify(content, {
      useShortDoctype: true,
      removeComments: true,
      collapseWhitespace: true
    })
    return minified
  }

  return content
}

