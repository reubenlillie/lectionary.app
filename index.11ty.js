/**
 * @file The app index page
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Acts as data in JavaScript template files
 * @since 0.5.0
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in 11ty JavaScript templates}
 */
export var data = {
  icon: 'logo-color',
  layout: 'layouts/entry',
  navTitle: 'Home',
  tags: 'nav',
  weight: 1,
  eleventyComputed: {
    title: data => data.app.name,
    today: data => data.days.today.date,
    entry: data => data.days.today.entry
  },
  description: 'Helping you follow a proven system for daily Bible readings used worldwide for generations. Readings are updated daily at midnight UTC.'
}
